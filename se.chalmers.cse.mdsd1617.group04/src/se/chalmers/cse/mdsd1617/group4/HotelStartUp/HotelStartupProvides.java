/**
 */
package se.chalmers.cse.mdsd1617.group4.HotelStartUp;

import se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartupProvides#getIbookingstartup <em>Ibookingstartup</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartupProvides#getIroomstartup <em>Iroomstartup</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartupProvides#getIroomtypestartup <em>Iroomtypestartup</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage#getHotelStartupProvides()
 * @model
 * @generated
 */
public interface HotelStartupProvides extends IHotelStartupProvides {

	/**
	 * Returns the value of the '<em><b>Ibookingstartup</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ibookingstartup</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ibookingstartup</em>' reference.
	 * @see #setIbookingstartup(IBookingStartUp)
	 * @see se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage#getHotelStartupProvides_Ibookingstartup()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookingStartUp getIbookingstartup();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartupProvides#getIbookingstartup <em>Ibookingstartup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ibookingstartup</em>' reference.
	 * @see #getIbookingstartup()
	 * @generated
	 */
	void setIbookingstartup(IBookingStartUp value);

	/**
	 * Returns the value of the '<em><b>Iroomstartup</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomstartup</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomstartup</em>' reference.
	 * @see #setIroomstartup(IRoomStartUp)
	 * @see se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage#getHotelStartupProvides_Iroomstartup()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomStartUp getIroomstartup();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartupProvides#getIroomstartup <em>Iroomstartup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomstartup</em>' reference.
	 * @see #getIroomstartup()
	 * @generated
	 */
	void setIroomstartup(IRoomStartUp value);

	/**
	 * Returns the value of the '<em><b>Iroomtypestartup</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtypestartup</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtypestartup</em>' reference.
	 * @see #setIroomtypestartup(IRoomTypeStartUp)
	 * @see se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage#getHotelStartupProvides_Iroomtypestartup()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeStartUp getIroomtypestartup();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartupProvides#getIroomtypestartup <em>Iroomtypestartup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomtypestartup</em>' reference.
	 * @see #getIroomtypestartup()
	 * @generated
	 */
	void setIroomtypestartup(IRoomTypeStartUp value);
} // HotelStartupProvides
