/**
 */
package se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HotelStartUpFactoryImpl extends EFactoryImpl implements HotelStartUpFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static HotelStartUpFactory init() {
		try {
			HotelStartUpFactory theHotelStartUpFactory = (HotelStartUpFactory)EPackage.Registry.INSTANCE.getEFactory(HotelStartUpPackage.eNS_URI);
			if (theHotelStartUpFactory != null) {
				return theHotelStartUpFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new HotelStartUpFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartUpFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES: return createHotelStartupProvides();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartupProvides createHotelStartupProvides() {
		HotelStartupProvidesImpl hotelStartupProvides = new HotelStartupProvidesImpl();
		return hotelStartupProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartUpPackage getHotelStartUpPackage() {
		return (HotelStartUpPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static HotelStartUpPackage getPackage() {
		return HotelStartUpPackage.eINSTANCE;
	}

} //HotelStartUpFactoryImpl
