/**
 */
package se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group4.Booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp;
import se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp;
import se.chalmers.cse.mdsd1617.group4.Room.RoomFactory;
import se.chalmers.cse.mdsd1617.group4.Room.impl.RoomFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeFactory;
import se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Startup Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl.HotelStartupProvidesImpl#getIbookingstartup <em>Ibookingstartup</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl.HotelStartupProvidesImpl#getIroomstartup <em>Iroomstartup</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl.HotelStartupProvidesImpl#getIroomtypestartup <em>Iroomtypestartup</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelStartupProvidesImpl extends MinimalEObjectImpl.Container implements HotelStartupProvides {
	
	/**
	 * The cached value of the '{@link #getIbookingstartup() <em>Ibookingstartup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIbookingstartup()
	 * @generated
	 * @ordered
	 */
	protected IBookingStartUp ibookingstartup;
	/**
	 * The cached value of the '{@link #getIroomstartup() <em>Iroomstartup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomstartup()
	 * @generated
	 * @ordered
	 */
	protected IRoomStartUp iroomstartup;
	/**
	 * The cached value of the '{@link #getIroomtypestartup() <em>Iroomtypestartup</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtypestartup()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeStartUp iroomtypestartup;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected HotelStartupProvidesImpl() {
		super();
		BookingFactory  bf  = BookingFactoryImpl.init();
		RoomFactory  rf		= RoomFactoryImpl.init();
		RoomTypeFactory rtf = RoomTypeFactoryImpl.init();
		
		ibookingstartup = bf.createBookingHandler();
		iroomstartup = rf.createRoomHandler();
		iroomtypestartup = rtf.createRoomTypeHandler();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return HotelStartUpPackage.Literals.HOTEL_STARTUP_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingStartUp getIbookingstartup() {
		if (ibookingstartup != null && ibookingstartup.eIsProxy()) {
			InternalEObject oldIbookingstartup = (InternalEObject)ibookingstartup;
			ibookingstartup = (IBookingStartUp)eResolveProxy(oldIbookingstartup);
			if (ibookingstartup != oldIbookingstartup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGSTARTUP, oldIbookingstartup, ibookingstartup));
			}
		}
		return ibookingstartup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IBookingStartUp basicGetIbookingstartup() {
		return ibookingstartup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIbookingstartup(IBookingStartUp newIbookingstartup) {
		IBookingStartUp oldIbookingstartup = ibookingstartup;
		ibookingstartup = newIbookingstartup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGSTARTUP, oldIbookingstartup, ibookingstartup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomStartUp getIroomstartup() {
		if (iroomstartup != null && iroomstartup.eIsProxy()) {
			InternalEObject oldIroomstartup = (InternalEObject)iroomstartup;
			iroomstartup = (IRoomStartUp)eResolveProxy(oldIroomstartup);
			if (iroomstartup != oldIroomstartup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMSTARTUP, oldIroomstartup, iroomstartup));
			}
		}
		return iroomstartup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomStartUp basicGetIroomstartup() {
		return iroomstartup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomstartup(IRoomStartUp newIroomstartup) {
		IRoomStartUp oldIroomstartup = iroomstartup;
		iroomstartup = newIroomstartup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMSTARTUP, oldIroomstartup, iroomstartup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeStartUp getIroomtypestartup() {
		if (iroomtypestartup != null && iroomtypestartup.eIsProxy()) {
			InternalEObject oldIroomtypestartup = (InternalEObject)iroomtypestartup;
			iroomtypestartup = (IRoomTypeStartUp)eResolveProxy(oldIroomtypestartup);
			if (iroomtypestartup != oldIroomtypestartup) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPESTARTUP, oldIroomtypestartup, iroomtypestartup));
			}
		}
		return iroomtypestartup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeStartUp basicGetIroomtypestartup() {
		return iroomtypestartup;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomtypestartup(IRoomTypeStartUp newIroomtypestartup) {
		IRoomTypeStartUp oldIroomtypestartup = iroomtypestartup;
		iroomtypestartup = newIroomtypestartup;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPESTARTUP, oldIroomtypestartup, iroomtypestartup));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		ibookingstartup.clearBookings();
		iroomtypestartup.resetRoomTypes();
		iroomstartup.startUpRooms(numRooms);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGSTARTUP:
				if (resolve) return getIbookingstartup();
				return basicGetIbookingstartup();
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMSTARTUP:
				if (resolve) return getIroomstartup();
				return basicGetIroomstartup();
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPESTARTUP:
				if (resolve) return getIroomtypestartup();
				return basicGetIroomtypestartup();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGSTARTUP:
				setIbookingstartup((IBookingStartUp)newValue);
				return;
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMSTARTUP:
				setIroomstartup((IRoomStartUp)newValue);
				return;
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPESTARTUP:
				setIroomtypestartup((IRoomTypeStartUp)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGSTARTUP:
				setIbookingstartup((IBookingStartUp)null);
				return;
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMSTARTUP:
				setIroomstartup((IRoomStartUp)null);
				return;
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPESTARTUP:
				setIroomtypestartup((IRoomTypeStartUp)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IBOOKINGSTARTUP:
				return ibookingstartup != null;
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMSTARTUP:
				return iroomstartup != null;
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES__IROOMTYPESTARTUP:
				return iroomtypestartup != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case HotelStartUpPackage.HOTEL_STARTUP_PROVIDES___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelStartupProvidesImpl
