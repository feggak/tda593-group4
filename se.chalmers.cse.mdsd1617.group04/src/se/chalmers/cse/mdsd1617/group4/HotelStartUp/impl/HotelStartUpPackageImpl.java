/**
 */
package se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage;

import se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl;

import se.chalmers.cse.mdsd1617.group4.Group4Package;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpFactory;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartupProvides;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.IHotelStartupProvides;

import se.chalmers.cse.mdsd1617.group4.Room.RoomPackage;

import se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl;

import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage;

import se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl;

import se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class HotelStartUpPackageImpl extends EPackageImpl implements HotelStartUpPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iHotelStartupProvidesEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hotelStartupProvidesEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private HotelStartUpPackageImpl() {
		super(eNS_URI, HotelStartUpFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link HotelStartUpPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static HotelStartUpPackage init() {
		if (isInited) return (HotelStartUpPackage)EPackage.Registry.INSTANCE.getEPackage(HotelStartUpPackage.eNS_URI);

		// Obtain or create and register package
		HotelStartUpPackageImpl theHotelStartUpPackage = (HotelStartUpPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof HotelStartUpPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new HotelStartUpPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group4PackageImpl theGroup4Package = (Group4PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group4Package.eNS_URI) instanceof Group4PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group4Package.eNS_URI) : Group4Package.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) : RoomTypePackage.eINSTANCE);

		// Create package meta-data objects
		theHotelStartUpPackage.createPackageContents();
		theGroup4Package.createPackageContents();
		theBookingPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theRoomTypePackage.createPackageContents();

		// Initialize created meta-data
		theHotelStartUpPackage.initializePackageContents();
		theGroup4Package.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theRoomTypePackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theHotelStartUpPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(HotelStartUpPackage.eNS_URI, theHotelStartUpPackage);
		return theHotelStartUpPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIHotelStartupProvides() {
		return iHotelStartupProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIHotelStartupProvides__Startup__int() {
		return iHotelStartupProvidesEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHotelStartupProvides() {
		return hotelStartupProvidesEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHotelStartupProvides_Ibookingstartup() {
		return (EReference)hotelStartupProvidesEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHotelStartupProvides_Iroomstartup() {
		return (EReference)hotelStartupProvidesEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHotelStartupProvides_Iroomtypestartup() {
		return (EReference)hotelStartupProvidesEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelStartUpFactory getHotelStartUpFactory() {
		return (HotelStartUpFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		iHotelStartupProvidesEClass = createEClass(IHOTEL_STARTUP_PROVIDES);
		createEOperation(iHotelStartupProvidesEClass, IHOTEL_STARTUP_PROVIDES___STARTUP__INT);

		hotelStartupProvidesEClass = createEClass(HOTEL_STARTUP_PROVIDES);
		createEReference(hotelStartupProvidesEClass, HOTEL_STARTUP_PROVIDES__IBOOKINGSTARTUP);
		createEReference(hotelStartupProvidesEClass, HOTEL_STARTUP_PROVIDES__IROOMSTARTUP);
		createEReference(hotelStartupProvidesEClass, HOTEL_STARTUP_PROVIDES__IROOMTYPESTARTUP);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		BookingPackage theBookingPackage = (BookingPackage)EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI);
		RoomPackage theRoomPackage = (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);
		RoomTypePackage theRoomTypePackage = (RoomTypePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		hotelStartupProvidesEClass.getESuperTypes().add(this.getIHotelStartupProvides());

		// Initialize classes, features, and operations; add parameters
		initEClass(iHotelStartupProvidesEClass, IHotelStartupProvides.class, "IHotelStartupProvides", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIHotelStartupProvides__Startup__int(), null, "startup", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "numRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(hotelStartupProvidesEClass, HotelStartupProvides.class, "HotelStartupProvides", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHotelStartupProvides_Ibookingstartup(), theBookingPackage.getIBookingStartUp(), null, "ibookingstartup", null, 1, 1, HotelStartupProvides.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getHotelStartupProvides_Iroomstartup(), theRoomPackage.getIRoomStartUp(), null, "iroomstartup", null, 1, 1, HotelStartupProvides.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getHotelStartupProvides_Iroomtypestartup(), theRoomTypePackage.getIRoomTypeStartUp(), null, "iroomtypestartup", null, 1, 1, HotelStartupProvides.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
	}

} //HotelStartUpPackageImpl
