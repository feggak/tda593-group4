/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.BasicEMap;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;
import se.chalmers.cse.mdsd1617.group4.Booking.Booking;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group4.Booking.Extra;
import se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO;
import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getIroom <em>Iroom</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getIroomtype <em>Iroomtype</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getDateFrom <em>Date From</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getDateTo <em>Date To</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getFirstName <em>First Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getLastName <em>Last Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getBookingstatus <em>Bookingstatus</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl#getRoomidtoextrasmap <em>Roomidtoextrasmap</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * The cached value of the '{@link #getIroom() <em>Iroom</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroom()
	 * @generated
	 * @ordered
	 */
	protected EList<IRoom> iroom;
	/**
	 * The cached value of the '{@link #getIroomtype() <em>Iroomtype</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtype()
	 * @generated
	 * @ordered
	 */
	protected EList<IRoomType> iroomtype;

	/**
	 * The default value of the '{@link #getDateFrom() <em>Date From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateFrom()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_FROM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDateFrom() <em>Date From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateFrom()
	 * @generated
	 * @ordered
	 */
	protected Date dateFrom = DATE_FROM_EDEFAULT;

	/**
	 * The default value of the '{@link #getDateTo() <em>Date To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateTo()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_TO_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDateTo() <em>Date To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateTo()
	 * @generated
	 * @ordered
	 */
	protected Date dateTo = DATE_TO_EDEFAULT;

	/**
	 * The default value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRST_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected String firstName = FIRST_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getLastName() <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastName()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getLastName() <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastName()
	 * @generated
	 * @ordered
	 */
	protected String lastName = LAST_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getBookingstatus() <em>Bookingstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingstatus()
	 * @generated
	 * @ordered
	 */
	protected static final BookingStatus BOOKINGSTATUS_EDEFAULT = BookingStatus.INITIATED;

	/**
	 * The cached value of the '{@link #getBookingstatus() <em>Bookingstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingstatus()
	 * @generated
	 * @ordered
	 */
	protected BookingStatus bookingstatus = BOOKINGSTATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected int bookingID = BOOKING_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoomidtoextrasmap() <em>Roomidtoextrasmap</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomidtoextrasmap()
	 * @generated
	 * @ordered
	 */
	protected EMap<Integer, EList<Extra>> roomidtoextrasmap;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingImpl() {
		super();
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */	
	protected BookingImpl(String firstName, String lastName, Date startDate, Date endDate, int bookingID) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateFrom = startDate;
		this.dateTo = endDate;
		this.bookingID = bookingID;
		iroom = new BasicEList<IRoom>();
		iroomtype = new BasicEList<IRoomType>();
		this.roomidtoextrasmap = new BasicEMap<Integer, EList<Extra>>();
	}

	public BookingImpl(BookingDTO bookingData){
		
		setDateFrom(bookingData.getDateFrom());
		setDateTo(bookingData.getDateTo());
		setFirstName(bookingData.getFirstName());
		setLastName(bookingData.getLastName());
		
		BookingStatus status = null;
		
		switch (bookingData.getBookingstatus()) {
		case "confirmed":
			status= BookingStatus.CONFIRMED;
			break;
		case "initiated":
			status= BookingStatus.INITIATED;
			break;
		case "checkedin":
			status= BookingStatus.CHECKEDOUT;
			break;
		case "checkedout":
			status= BookingStatus.CHECKEDOUT;
			break;
		default:
			break;
		}
		
		setBookingstatus(status);
		
		EList<ExtraDTO> listDto = bookingData.getExtradto();
		
		for (int i = 0; i < listDto.size(); i++) {
			ExtraDTO room = listDto.get(i);
			Integer roomID = room.getRoomNr();
			ExtraImpl e = new ExtraImpl();
			e.setDescription(room.getDescription());
			e.setPrice(room.getPrice());
			
			if(this.getRoomidtoextrasmap().get(room.getRoomNr())==null){
				EList<Extra> eList = new BasicEList<Extra>();
				eList.add(e);
				this.roomidtoextrasmap.put(roomID, eList);
			}else{
				EList<Extra> eList = this.getRoomidtoextrasmap().get(roomID);
				eList.add(e);
				this.roomidtoextrasmap.put(listDto.get(i).getRoomNr(), eList);
			}
		}
		
		
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IRoom> getIroom() {
		if (iroom == null) {
			iroom = new EObjectResolvingEList<IRoom>(IRoom.class, this, BookingPackage.BOOKING__IROOM);
		}
		return iroom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IRoomType> getIroomtype() {
		if (iroomtype == null) {
			iroomtype = new EObjectResolvingEList<IRoomType>(IRoomType.class, this, BookingPackage.BOOKING__IROOMTYPE);
		}
		return iroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateFrom(Date newDateFrom) {
		Date oldDateFrom = dateFrom;
		dateFrom = newDateFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__DATE_FROM, oldDateFrom, dateFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDateTo() {
		return dateTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateTo(Date newDateTo) {
		Date oldDateTo = dateTo;
		dateTo = newDateTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__DATE_TO, oldDateTo, dateTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstName(String newFirstName) {
		String oldFirstName = firstName;
		firstName = newFirstName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__FIRST_NAME, oldFirstName, firstName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastName(String newLastName) {
		String oldLastName = lastName;
		lastName = newLastName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__LAST_NAME, oldLastName, lastName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingStatus getBookingstatus() {
		return bookingstatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingstatus(BookingStatus newBookingstatus) {
		BookingStatus oldBookingstatus = bookingstatus;
		bookingstatus = newBookingstatus == null ? BOOKINGSTATUS_EDEFAULT : newBookingstatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__BOOKINGSTATUS, oldBookingstatus, bookingstatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingID() {
		return bookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingID(int newBookingID) {
		int oldBookingID = bookingID;
		bookingID = newBookingID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING__BOOKING_ID, oldBookingID, bookingID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<Integer, EList<Extra>> getRoomidtoextrasmap() {
		if (roomidtoextrasmap == null) {
			roomidtoextrasmap = new EcoreEMap<Integer,EList<Extra>>(BookingPackage.Literals.ROOM_ID_TO_EXTRAS_MAP, RoomIDToExtrasMapImpl.class, this, BookingPackage.BOOKING__ROOMIDTOEXTRASMAP);
		}
		return roomidtoextrasmap;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setIRoomList(EList<IRoom> roomList) {
		this.iroom = roomList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIRoomTypeList(EList<IRoomType> roomTypes) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addExtra(Extra extra) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BookingPackage.BOOKING__ROOMIDTOEXTRASMAP:
				return ((InternalEList<?>)getRoomidtoextrasmap()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING__IROOM:
				return getIroom();
			case BookingPackage.BOOKING__IROOMTYPE:
				return getIroomtype();
			case BookingPackage.BOOKING__DATE_FROM:
				return getDateFrom();
			case BookingPackage.BOOKING__DATE_TO:
				return getDateTo();
			case BookingPackage.BOOKING__FIRST_NAME:
				return getFirstName();
			case BookingPackage.BOOKING__LAST_NAME:
				return getLastName();
			case BookingPackage.BOOKING__BOOKINGSTATUS:
				return getBookingstatus();
			case BookingPackage.BOOKING__BOOKING_ID:
				return getBookingID();
			case BookingPackage.BOOKING__ROOMIDTOEXTRASMAP:
				if (coreType) return getRoomidtoextrasmap();
				else return getRoomidtoextrasmap().map();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING__IROOM:
				getIroom().clear();
				getIroom().addAll((Collection<? extends IRoom>)newValue);
				return;
			case BookingPackage.BOOKING__IROOMTYPE:
				getIroomtype().clear();
				getIroomtype().addAll((Collection<? extends IRoomType>)newValue);
				return;
			case BookingPackage.BOOKING__DATE_FROM:
				setDateFrom((Date)newValue);
				return;
			case BookingPackage.BOOKING__DATE_TO:
				setDateTo((Date)newValue);
				return;
			case BookingPackage.BOOKING__FIRST_NAME:
				setFirstName((String)newValue);
				return;
			case BookingPackage.BOOKING__LAST_NAME:
				setLastName((String)newValue);
				return;
			case BookingPackage.BOOKING__BOOKINGSTATUS:
				setBookingstatus((BookingStatus)newValue);
				return;
			case BookingPackage.BOOKING__BOOKING_ID:
				setBookingID((Integer)newValue);
				return;
			case BookingPackage.BOOKING__ROOMIDTOEXTRASMAP:
				((EStructuralFeature.Setting)getRoomidtoextrasmap()).set(newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__IROOM:
				getIroom().clear();
				return;
			case BookingPackage.BOOKING__IROOMTYPE:
				getIroomtype().clear();
				return;
			case BookingPackage.BOOKING__DATE_FROM:
				setDateFrom(DATE_FROM_EDEFAULT);
				return;
			case BookingPackage.BOOKING__DATE_TO:
				setDateTo(DATE_TO_EDEFAULT);
				return;
			case BookingPackage.BOOKING__FIRST_NAME:
				setFirstName(FIRST_NAME_EDEFAULT);
				return;
			case BookingPackage.BOOKING__LAST_NAME:
				setLastName(LAST_NAME_EDEFAULT);
				return;
			case BookingPackage.BOOKING__BOOKINGSTATUS:
				setBookingstatus(BOOKINGSTATUS_EDEFAULT);
				return;
			case BookingPackage.BOOKING__BOOKING_ID:
				setBookingID(BOOKING_ID_EDEFAULT);
				return;
			case BookingPackage.BOOKING__ROOMIDTOEXTRASMAP:
				getRoomidtoextrasmap().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING__IROOM:
				return iroom != null && !iroom.isEmpty();
			case BookingPackage.BOOKING__IROOMTYPE:
				return iroomtype != null && !iroomtype.isEmpty();
			case BookingPackage.BOOKING__DATE_FROM:
				return DATE_FROM_EDEFAULT == null ? dateFrom != null : !DATE_FROM_EDEFAULT.equals(dateFrom);
			case BookingPackage.BOOKING__DATE_TO:
				return DATE_TO_EDEFAULT == null ? dateTo != null : !DATE_TO_EDEFAULT.equals(dateTo);
			case BookingPackage.BOOKING__FIRST_NAME:
				return FIRST_NAME_EDEFAULT == null ? firstName != null : !FIRST_NAME_EDEFAULT.equals(firstName);
			case BookingPackage.BOOKING__LAST_NAME:
				return LAST_NAME_EDEFAULT == null ? lastName != null : !LAST_NAME_EDEFAULT.equals(lastName);
			case BookingPackage.BOOKING__BOOKINGSTATUS:
				return bookingstatus != BOOKINGSTATUS_EDEFAULT;
			case BookingPackage.BOOKING__BOOKING_ID:
				return bookingID != BOOKING_ID_EDEFAULT;
			case BookingPackage.BOOKING__ROOMIDTOEXTRASMAP:
				return roomidtoextrasmap != null && !roomidtoextrasmap.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	@SuppressWarnings("unchecked")
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING___SET_IROOM_LIST__ELIST:
				setIRoomList((EList<IRoom>)arguments.get(0));
				return null;
			case BookingPackage.BOOKING___SET_IROOM_TYPE_LIST__ELIST:
				setIRoomTypeList((EList<IRoomType>)arguments.get(0));
				return null;
			case BookingPackage.BOOKING___ADD_EXTRA__EXTRA:
				addExtra((Extra)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dateFrom: ");
		result.append(dateFrom);
		result.append(", dateTo: ");
		result.append(dateTo);
		result.append(", firstName: ");
		result.append(firstName);
		result.append(", lastName: ");
		result.append(lastName);
		result.append(", bookingstatus: ");
		result.append(bookingstatus);
		result.append(", bookingID: ");
		result.append(bookingID);
		result.append(')');
		return result.toString();
	}


} //BookingImpl
