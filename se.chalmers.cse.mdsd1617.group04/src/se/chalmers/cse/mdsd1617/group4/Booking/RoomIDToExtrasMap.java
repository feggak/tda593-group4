/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room ID To Extras Map</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.RoomIDToExtrasMap#getKey <em>Key</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.RoomIDToExtrasMap#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getRoomIDToExtrasMap()
 * @model
 * @generated
 */
public interface RoomIDToExtrasMap extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' attribute.
	 * @see #setKey(Integer)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getRoomIDToExtrasMap_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Integer getKey();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.RoomIDToExtrasMap#getKey <em>Key</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' attribute.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(Integer value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group4.Booking.Extra}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getRoomIDToExtrasMap_Value()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Extra> getValue();

} // RoomIDToExtrasMap
