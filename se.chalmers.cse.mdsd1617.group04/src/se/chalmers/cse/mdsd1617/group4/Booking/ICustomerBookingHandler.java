/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO;

import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>ICustomer Booking Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getICustomerBookingHandler()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface ICustomerBookingHandler extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" numBedsRequired="true" numBedsOrdered="false" startDateRequired="true" startDateOrdered="false" endDaetRequired="true" endDaetOrdered="false"
	 * @generated
	 */
	EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDaet);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false" inDateRequired="true" inDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	int initiateBooking(String firstName, String lastName, String inDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean confirmBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean addRoomToBooking(String roomTypeDescription, int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	int checkInRoom(IRoomType roomType, int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" roomNrRequired="true" roomNrOrdered="false"
	 * @generated
	 */
	void checkInPhysicalRoom(int bookingID, int roomNr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	double initiateCheckout(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNrRequired="true" roomNrOrdered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	double initiateRoomCheckout(int roomNr, int bookingID);

} // ICustomerBookingHandler
