/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking;

import java.util.Date;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EObject;
import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getIroom <em>Iroom</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getIroomtype <em>Iroomtype</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getDateFrom <em>Date From</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getDateTo <em>Date To</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getFirstName <em>First Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getLastName <em>Last Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getBookingstatus <em>Bookingstatus</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getRoomidtoextrasmap <em>Roomidtoextrasmap</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking()
 * @model
 * @generated
 */
public interface Booking extends EObject {
	/**
	 * Returns the value of the '<em><b>Iroom</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group4.Room.IRoom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroom</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroom</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_Iroom()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IRoom> getIroom();

	/**
	 * Returns the value of the '<em><b>Iroomtype</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtype</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtype</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_Iroomtype()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IRoomType> getIroomtype();

	/**
	 * Returns the value of the '<em><b>Date From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date From</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date From</em>' attribute.
	 * @see #setDateFrom(Date)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_DateFrom()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getDateFrom();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getDateFrom <em>Date From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date From</em>' attribute.
	 * @see #getDateFrom()
	 * @generated
	 */
	void setDateFrom(Date value);

	/**
	 * Returns the value of the '<em><b>Date To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date To</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date To</em>' attribute.
	 * @see #setDateTo(Date)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_DateTo()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getDateTo();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getDateTo <em>Date To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date To</em>' attribute.
	 * @see #getDateTo()
	 * @generated
	 */
	void setDateTo(Date value);

	/**
	 * Returns the value of the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Name</em>' attribute.
	 * @see #setFirstName(String)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_FirstName()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getFirstName();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getFirstName <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Name</em>' attribute.
	 * @see #getFirstName()
	 * @generated
	 */
	void setFirstName(String value);

	/**
	 * Returns the value of the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Name</em>' attribute.
	 * @see #setLastName(String)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_LastName()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getLastName();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getLastName <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Name</em>' attribute.
	 * @see #getLastName()
	 * @generated
	 */
	void setLastName(String value);

	/**
	 * Returns the value of the '<em><b>Bookingstatus</b></em>' attribute.
	 * The literals are from the enumeration {@link se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookingstatus</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookingstatus</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus
	 * @see #setBookingstatus(BookingStatus)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_Bookingstatus()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	BookingStatus getBookingstatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getBookingstatus <em>Bookingstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bookingstatus</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus
	 * @see #getBookingstatus()
	 * @generated
	 */
	void setBookingstatus(BookingStatus value);

	/**
	 * Returns the value of the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking ID</em>' attribute.
	 * @see #setBookingID(int)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_BookingID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getBookingID();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getBookingID <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking ID</em>' attribute.
	 * @see #getBookingID()
	 * @generated
	 */
	void setBookingID(int value);

	/**
	 * Returns the value of the '<em><b>Roomidtoextrasmap</b></em>' map.
	 * The key is of type {@link java.lang.Integer},
	 * and the value is of type list of {@link se.chalmers.cse.mdsd1617.group4.Booking.Extra},
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roomidtoextrasmap</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roomidtoextrasmap</em>' map.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBooking_Roomidtoextrasmap()
	 * @model mapType="se.chalmers.cse.mdsd1617.group4.Booking.RoomIDToExtrasMap<org.eclipse.emf.ecore.EIntegerObject, se.chalmers.cse.mdsd1617.group4.Booking.Extra>" ordered="false"
	 * @generated
	 */
	EMap<Integer, EList<Extra>> getRoomidtoextrasmap();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomListMany="true" roomListOrdered="false"
	 * @generated
	 */
	void setIRoomList(EList<IRoom> roomList);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomTypesMany="true" roomTypesOrdered="false"
	 * @generated
	 */
	void setIRoomTypeList(EList<IRoomType> roomTypes);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model extraRequired="true" extraOrdered="false"
	 * @generated
	 */
	void addExtra(Extra extra);

} // Booking
