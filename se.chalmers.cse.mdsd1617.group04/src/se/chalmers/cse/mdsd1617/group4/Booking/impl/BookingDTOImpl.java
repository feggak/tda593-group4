/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking.impl;

import java.util.Collection;
import java.util.Date;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import se.chalmers.cse.mdsd1617.group4.Booking.Booking;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage;
import se.chalmers.cse.mdsd1617.group4.Booking.Extra;
import se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO;
import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DTO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getDateTo <em>Date To</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getDateFrom <em>Date From</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getFirstName <em>First Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getLastName <em>Last Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getExtradto <em>Extradto</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getIroom <em>Iroom</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getIroomtype <em>Iroomtype</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getBookingstatus <em>Bookingstatus</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl#getPaidstatus <em>Paidstatus</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingDTOImpl extends MinimalEObjectImpl.Container implements BookingDTO {
	/**
	 * The default value of the '{@link #getDateTo() <em>Date To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateTo()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_TO_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getDateTo() <em>Date To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateTo()
	 * @generated
	 * @ordered
	 */
	protected Date dateTo = DATE_TO_EDEFAULT;
	/**
	 * The default value of the '{@link #getDateFrom() <em>Date From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateFrom()
	 * @generated
	 * @ordered
	 */
	protected static final Date DATE_FROM_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getDateFrom() <em>Date From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDateFrom()
	 * @generated
	 * @ordered
	 */
	protected Date dateFrom = DATE_FROM_EDEFAULT;

	/**
	 * The default value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected static final String FIRST_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getFirstName() <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstName()
	 * @generated
	 * @ordered
	 */
	protected String firstName = FIRST_NAME_EDEFAULT;
	/**
	 * The default value of the '{@link #getLastName() <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastName()
	 * @generated
	 * @ordered
	 */
	protected static final String LAST_NAME_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getLastName() <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLastName()
	 * @generated
	 * @ordered
	 */
	protected String lastName = LAST_NAME_EDEFAULT;
	/**
	 * The default value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected int bookingID = BOOKING_ID_EDEFAULT;
	/**
	 * The cached value of the '{@link #getExtradto() <em>Extradto</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtradto()
	 * @generated
	 * @ordered
	 */
	protected EList<ExtraDTO> extradto;
	/**
	 * The cached value of the '{@link #getIroom() <em>Iroom</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroom()
	 * @generated
	 * @ordered
	 */
	protected EList<IRoom> iroom;
	/**
	 * The cached value of the '{@link #getIroomtype() <em>Iroomtype</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtype()
	 * @generated
	 * @ordered
	 */
	protected IRoomType iroomtype;
	/**
	 * The default value of the '{@link #getBookingstatus() <em>Bookingstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingstatus()
	 * @generated
	 * @ordered
	 */
	protected static final String BOOKINGSTATUS_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getBookingstatus() <em>Bookingstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingstatus()
	 * @generated
	 * @ordered
	 */
	protected String bookingstatus = BOOKINGSTATUS_EDEFAULT;
	/**
	 * The default value of the '{@link #getPaidstatus() <em>Paidstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPaidstatus()
	 * @generated
	 * @ordered
	 */
	protected static final String PAIDSTATUS_EDEFAULT = null;
	/**
	 * The cached value of the '{@link #getPaidstatus() <em>Paidstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPaidstatus()
	 * @generated
	 * @ordered
	 */
	protected String paidstatus = PAIDSTATUS_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingDTOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING_DTO;
	}

	public BookingDTOImpl(Booking b){
		
		firstName = b.getFirstName();
		lastName = b.getLastName();
		dateFrom = b.getDateFrom();
		dateTo = b.getDateTo();
		bookingID = b.getBookingID();
		
		bookingstatus = "bookStatusFail";
		
		switch(b.getBookingstatus()){
		case CHECKEDIN:
			bookingstatus = "checkedin";
			break;
		case CHECKEDOUT:
			bookingstatus = "checkedout";
			break;
		case CONFIRMED:
			bookingstatus = "confirmed";
			break;
		case INITIATED:
			bookingstatus = "initiated";
			break;
		default:
			break;
		}
		
		//for all rooms
		for (int i = 0; i < b.getRoomidtoextrasmap().size(); i++) {
			Integer roomNr = b.getRoomidtoextrasmap().get(i).getKey();
			EList<Extra> eList = b.getRoomidtoextrasmap().get(roomNr);
			
			if(eList==null){
				return;
			}

			//for all extras for that room
			for(int j=0; j < eList.size() ;j++){
								
				ExtraDTO e = new ExtraDTOImpl();
				e.setDescription(eList.get(j).getDescription());
				e.setPrice(eList.get(j).getPrice());
				this.extradto.add(e);
			}
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDateTo() {
		return dateTo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateTo(Date newDateTo) {
		Date oldDateTo = dateTo;
		dateTo = newDateTo;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_DTO__DATE_TO, oldDateTo, dateTo));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDateFrom(Date newDateFrom) {
		Date oldDateFrom = dateFrom;
		dateFrom = newDateFrom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_DTO__DATE_FROM, oldDateFrom, dateFrom));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstName(String newFirstName) {
		String oldFirstName = firstName;
		firstName = newFirstName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_DTO__FIRST_NAME, oldFirstName, firstName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastName(String newLastName) {
		String oldLastName = lastName;
		lastName = newLastName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_DTO__LAST_NAME, oldLastName, lastName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingID() {
		return bookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingID(int newBookingID) {
		int oldBookingID = bookingID;
		bookingID = newBookingID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_DTO__BOOKING_ID, oldBookingID, bookingID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<ExtraDTO> getExtradto() {
		if (extradto == null) {
			extradto = new EObjectContainmentEList<ExtraDTO>(ExtraDTO.class, this, BookingPackage.BOOKING_DTO__EXTRADTO);
		}
		return extradto;
	}
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IRoom> getIroom() {
		if (iroom == null) {
			iroom = new EObjectResolvingEList<IRoom>(IRoom.class, this, BookingPackage.BOOKING_DTO__IROOM);
		}
		return iroom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomType getIroomtype() {
		if (iroomtype != null && iroomtype.eIsProxy()) {
			InternalEObject oldIroomtype = (InternalEObject)iroomtype;
			iroomtype = (IRoomType)eResolveProxy(oldIroomtype);
			if (iroomtype != oldIroomtype) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKING_DTO__IROOMTYPE, oldIroomtype, iroomtype));
			}
		}
		return iroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomType basicGetIroomtype() {
		return iroomtype;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomtype(IRoomType newIroomtype) {
		IRoomType oldIroomtype = iroomtype;
		iroomtype = newIroomtype;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_DTO__IROOMTYPE, oldIroomtype, iroomtype));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBookingstatus() {
		return bookingstatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingstatus(String newBookingstatus) {
		String oldBookingstatus = bookingstatus;
		bookingstatus = newBookingstatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_DTO__BOOKINGSTATUS, oldBookingstatus, bookingstatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPaidstatus() {
		return paidstatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPaidstatus(String newPaidstatus) {
		String oldPaidstatus = paidstatus;
		paidstatus = newPaidstatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_DTO__PAIDSTATUS, oldPaidstatus, paidstatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case BookingPackage.BOOKING_DTO__EXTRADTO:
				return ((InternalEList<?>)getExtradto()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING_DTO__DATE_TO:
				return getDateTo();
			case BookingPackage.BOOKING_DTO__DATE_FROM:
				return getDateFrom();
			case BookingPackage.BOOKING_DTO__FIRST_NAME:
				return getFirstName();
			case BookingPackage.BOOKING_DTO__LAST_NAME:
				return getLastName();
			case BookingPackage.BOOKING_DTO__BOOKING_ID:
				return getBookingID();
			case BookingPackage.BOOKING_DTO__EXTRADTO:
				return getExtradto();
			case BookingPackage.BOOKING_DTO__IROOM:
				return getIroom();
			case BookingPackage.BOOKING_DTO__IROOMTYPE:
				if (resolve) return getIroomtype();
				return basicGetIroomtype();
			case BookingPackage.BOOKING_DTO__BOOKINGSTATUS:
				return getBookingstatus();
			case BookingPackage.BOOKING_DTO__PAIDSTATUS:
				return getPaidstatus();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING_DTO__DATE_TO:
				setDateTo((Date)newValue);
				return;
			case BookingPackage.BOOKING_DTO__DATE_FROM:
				setDateFrom((Date)newValue);
				return;
			case BookingPackage.BOOKING_DTO__FIRST_NAME:
				setFirstName((String)newValue);
				return;
			case BookingPackage.BOOKING_DTO__LAST_NAME:
				setLastName((String)newValue);
				return;
			case BookingPackage.BOOKING_DTO__BOOKING_ID:
				setBookingID((Integer)newValue);
				return;
			case BookingPackage.BOOKING_DTO__EXTRADTO:
				getExtradto().clear();
				getExtradto().addAll((Collection<? extends ExtraDTO>)newValue);
				return;
			case BookingPackage.BOOKING_DTO__IROOM:
				getIroom().clear();
				getIroom().addAll((Collection<? extends IRoom>)newValue);
				return;
			case BookingPackage.BOOKING_DTO__IROOMTYPE:
				setIroomtype((IRoomType)newValue);
				return;
			case BookingPackage.BOOKING_DTO__BOOKINGSTATUS:
				setBookingstatus((String)newValue);
				return;
			case BookingPackage.BOOKING_DTO__PAIDSTATUS:
				setPaidstatus((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_DTO__DATE_TO:
				setDateTo(DATE_TO_EDEFAULT);
				return;
			case BookingPackage.BOOKING_DTO__DATE_FROM:
				setDateFrom(DATE_FROM_EDEFAULT);
				return;
			case BookingPackage.BOOKING_DTO__FIRST_NAME:
				setFirstName(FIRST_NAME_EDEFAULT);
				return;
			case BookingPackage.BOOKING_DTO__LAST_NAME:
				setLastName(LAST_NAME_EDEFAULT);
				return;
			case BookingPackage.BOOKING_DTO__BOOKING_ID:
				setBookingID(BOOKING_ID_EDEFAULT);
				return;
			case BookingPackage.BOOKING_DTO__EXTRADTO:
				getExtradto().clear();
				return;
			case BookingPackage.BOOKING_DTO__IROOM:
				getIroom().clear();
				return;
			case BookingPackage.BOOKING_DTO__IROOMTYPE:
				setIroomtype((IRoomType)null);
				return;
			case BookingPackage.BOOKING_DTO__BOOKINGSTATUS:
				setBookingstatus(BOOKINGSTATUS_EDEFAULT);
				return;
			case BookingPackage.BOOKING_DTO__PAIDSTATUS:
				setPaidstatus(PAIDSTATUS_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_DTO__DATE_TO:
				return DATE_TO_EDEFAULT == null ? dateTo != null : !DATE_TO_EDEFAULT.equals(dateTo);
			case BookingPackage.BOOKING_DTO__DATE_FROM:
				return DATE_FROM_EDEFAULT == null ? dateFrom != null : !DATE_FROM_EDEFAULT.equals(dateFrom);
			case BookingPackage.BOOKING_DTO__FIRST_NAME:
				return FIRST_NAME_EDEFAULT == null ? firstName != null : !FIRST_NAME_EDEFAULT.equals(firstName);
			case BookingPackage.BOOKING_DTO__LAST_NAME:
				return LAST_NAME_EDEFAULT == null ? lastName != null : !LAST_NAME_EDEFAULT.equals(lastName);
			case BookingPackage.BOOKING_DTO__BOOKING_ID:
				return bookingID != BOOKING_ID_EDEFAULT;
			case BookingPackage.BOOKING_DTO__EXTRADTO:
				return extradto != null && !extradto.isEmpty();
			case BookingPackage.BOOKING_DTO__IROOM:
				return iroom != null && !iroom.isEmpty();
			case BookingPackage.BOOKING_DTO__IROOMTYPE:
				return iroomtype != null;
			case BookingPackage.BOOKING_DTO__BOOKINGSTATUS:
				return BOOKINGSTATUS_EDEFAULT == null ? bookingstatus != null : !BOOKINGSTATUS_EDEFAULT.equals(bookingstatus);
			case BookingPackage.BOOKING_DTO__PAIDSTATUS:
				return PAIDSTATUS_EDEFAULT == null ? paidstatus != null : !PAIDSTATUS_EDEFAULT.equals(paidstatus);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (dateTo: ");
		result.append(dateTo);
		result.append(", dateFrom: ");
		result.append(dateFrom);
		result.append(", firstName: ");
		result.append(firstName);
		result.append(", lastName: ");
		result.append(lastName);
		result.append(", bookingID: ");
		result.append(bookingID);
		result.append(", bookingstatus: ");
		result.append(bookingstatus);
		result.append(", paidstatus: ");
		result.append(paidstatus);
		result.append(')');
		return result.toString();
	}

} //BookingDTOImpl
