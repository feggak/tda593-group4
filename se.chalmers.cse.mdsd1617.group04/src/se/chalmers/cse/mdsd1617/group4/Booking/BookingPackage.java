/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingFactory
 * @model kind="package"
 * @generated
 */
public interface BookingPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Booking";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group4/Booking.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group4.Booking";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	BookingPackage eINSTANCE = se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 0;

	/**
	 * The feature id for the '<em><b>Iroom</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__IROOM = 0;

	/**
	 * The feature id for the '<em><b>Iroomtype</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__IROOMTYPE = 1;

	/**
	 * The feature id for the '<em><b>Date From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__DATE_FROM = 2;

	/**
	 * The feature id for the '<em><b>Date To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__DATE_TO = 3;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__FIRST_NAME = 4;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__LAST_NAME = 5;

	/**
	 * The feature id for the '<em><b>Bookingstatus</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOKINGSTATUS = 6;

	/**
	 * The feature id for the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOKING_ID = 7;

	/**
	 * The feature id for the '<em><b>Roomidtoextrasmap</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__ROOMIDTOEXTRASMAP = 8;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = 9;

	/**
	 * The operation id for the '<em>Set IRoom List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___SET_IROOM_LIST__ELIST = 0;

	/**
	 * The operation id for the '<em>Set IRoom Type List</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___SET_IROOM_TYPE_LIST__ELIST = 1;

	/**
	 * The operation id for the '<em>Add Extra</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING___ADD_EXTRA__EXTRA = 2;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.RoomIDToExtrasMapImpl <em>Room ID To Extras Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.RoomIDToExtrasMapImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getRoomIDToExtrasMap()
	 * @generated
	 */
	int ROOM_ID_TO_EXTRAS_MAP = 1;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_ID_TO_EXTRAS_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_ID_TO_EXTRAS_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>Room ID To Extras Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_ID_TO_EXTRAS_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room ID To Extras Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_ID_TO_EXTRAS_MAP_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.ExtraImpl <em>Extra</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.ExtraImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getExtra()
	 * @generated
	 */
	int EXTRA = 2;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA__PRICE = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA__DESCRIPTION = 1;

	/**
	 * The number of structural features of the '<em>Extra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Extra</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingHandlerImpl <em>Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingHandlerImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getBookingHandler()
	 * @generated
	 */
	int BOOKING_HANDLER = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler <em>IBooking Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getIBookingHandler()
	 * @generated
	 */
	int IBOOKING_HANDLER = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp <em>IBooking Start Up</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getIBookingStartUp()
	 * @generated
	 */
	int IBOOKING_START_UP = 7;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl <em>DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getBookingDTO()
	 * @generated
	 */
	int BOOKING_DTO = 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.ExtraDTOImpl <em>Extra DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.ExtraDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getExtraDTO()
	 * @generated
	 */
	int EXTRA_DTO = 6;

	/**
	 * The number of structural features of the '<em>IBooking Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___CONFIRM_BOOKING__INT = 2;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = 3;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE_INT = 4;

	/**
	 * The operation id for the '<em>Initiate Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___INITIATE_CHECK_IN__INT = 5;

	/**
	 * The operation id for the '<em>Check In Physical Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___CHECK_IN_PHYSICAL_ROOM__INT_INT = 6;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___EDIT_BOOKING__BOOKINGDTO = 7;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___CANCEL_BOOKING__INT = 8;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___LIST_BOOKINGS = 9;

	/**
	 * The operation id for the '<em>List Occupied</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___LIST_OCCUPIED__DATE = 10;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___LIST_CHECK_INS__DATE_DATE = 11;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___LIST_CHECK_OUTS__DATE_DATE = 12;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE = 13;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___INITIATE_CHECKOUT__INT = 14;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT = 15;

	/**
	 * The operation id for the '<em>Get Booking By ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER___GET_BOOKING_BY_ID__INT = 16;

	/**
	 * The number of operations of the '<em>IBooking Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_HANDLER_OPERATION_COUNT = 17;

	/**
	 * The feature id for the '<em><b>Booking</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER__BOOKING = IBOOKING_HANDLER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iroombookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER__IROOMBOOKINGHANDLER = IBOOKING_HANDLER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Current Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER__CURRENT_BOOKING_ID = IBOOKING_HANDLER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER_FEATURE_COUNT = IBOOKING_HANDLER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING = IBOOKING_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IBOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___CONFIRM_BOOKING__INT = IBOOKING_HANDLER___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = IBOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE_INT = IBOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE_INT;

	/**
	 * The operation id for the '<em>Initiate Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___INITIATE_CHECK_IN__INT = IBOOKING_HANDLER___INITIATE_CHECK_IN__INT;

	/**
	 * The operation id for the '<em>Check In Physical Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___CHECK_IN_PHYSICAL_ROOM__INT_INT = IBOOKING_HANDLER___CHECK_IN_PHYSICAL_ROOM__INT_INT;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___EDIT_BOOKING__BOOKINGDTO = IBOOKING_HANDLER___EDIT_BOOKING__BOOKINGDTO;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___CANCEL_BOOKING__INT = IBOOKING_HANDLER___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___LIST_BOOKINGS = IBOOKING_HANDLER___LIST_BOOKINGS;

	/**
	 * The operation id for the '<em>List Occupied</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___LIST_OCCUPIED__DATE = IBOOKING_HANDLER___LIST_OCCUPIED__DATE;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___LIST_CHECK_INS__DATE_DATE = IBOOKING_HANDLER___LIST_CHECK_INS__DATE_DATE;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___LIST_CHECK_OUTS__DATE_DATE = IBOOKING_HANDLER___LIST_CHECK_OUTS__DATE_DATE;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE = IBOOKING_HANDLER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___INITIATE_CHECKOUT__INT = IBOOKING_HANDLER___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT = IBOOKING_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Get Booking By ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___GET_BOOKING_BY_ID__INT = IBOOKING_HANDLER___GET_BOOKING_BY_ID__INT;

	/**
	 * The operation id for the '<em>Clear Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER___CLEAR_BOOKINGS = IBOOKING_HANDLER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_HANDLER_OPERATION_COUNT = IBOOKING_HANDLER_OPERATION_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Date To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__DATE_TO = 0;

	/**
	 * The feature id for the '<em><b>Date From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__DATE_FROM = 1;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__FIRST_NAME = 2;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__LAST_NAME = 3;

	/**
	 * The feature id for the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__BOOKING_ID = 4;

	/**
	 * The feature id for the '<em><b>Extradto</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__EXTRADTO = 5;

	/**
	 * The feature id for the '<em><b>Iroom</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__IROOM = 6;

	/**
	 * The feature id for the '<em><b>Iroomtype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__IROOMTYPE = 7;

	/**
	 * The feature id for the '<em><b>Bookingstatus</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__BOOKINGSTATUS = 8;

	/**
	 * The feature id for the '<em><b>Paidstatus</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO__PAIDSTATUS = 9;

	/**
	 * The number of structural features of the '<em>DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO_FEATURE_COUNT = 10;

	/**
	 * The number of operations of the '<em>DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_DTO_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_DTO__DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_DTO__PRICE = 1;

	/**
	 * The feature id for the '<em><b>Room Nr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_DTO__ROOM_NR = 2;

	/**
	 * The number of structural features of the '<em>Extra DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_DTO_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Extra DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTRA_DTO_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>IBooking Start Up</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_START_UP_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Clear Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_START_UP___CLEAR_BOOKINGS = 0;

	/**
	 * The number of operations of the '<em>IBooking Start Up</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_START_UP_OPERATION_COUNT = 1;


	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getBookingStatus()
	 * @generated
	 */
	int BOOKING_STATUS = 8;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getIroom <em>Iroom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Iroom</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getIroom()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Iroom();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getIroomtype <em>Iroomtype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Iroomtype</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getIroomtype()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Iroomtype();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getDateFrom <em>Date From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date From</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getDateFrom()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_DateFrom();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getDateTo <em>Date To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date To</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getDateTo()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_DateTo();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getFirstName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getLastName()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_LastName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getBookingstatus <em>Bookingstatus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bookingstatus</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getBookingstatus()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_Bookingstatus();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getBookingID <em>Booking ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking ID</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getBookingID()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_BookingID();

	/**
	 * Returns the meta object for the map '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#getRoomidtoextrasmap <em>Roomidtoextrasmap</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Roomidtoextrasmap</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#getRoomidtoextrasmap()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Roomidtoextrasmap();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#setIRoomList(org.eclipse.emf.common.util.EList) <em>Set IRoom List</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set IRoom List</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#setIRoomList(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getBooking__SetIRoomList__EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#setIRoomTypeList(org.eclipse.emf.common.util.EList) <em>Set IRoom Type List</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set IRoom Type List</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#setIRoomTypeList(org.eclipse.emf.common.util.EList)
	 * @generated
	 */
	EOperation getBooking__SetIRoomTypeList__EList();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.Booking#addExtra(se.chalmers.cse.mdsd1617.group4.Booking.Extra) <em>Add Extra</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Booking#addExtra(se.chalmers.cse.mdsd1617.group4.Booking.Extra)
	 * @generated
	 */
	EOperation getBooking__AddExtra__Extra();

	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>Room ID To Extras Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room ID To Extras Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EIntegerObject" keyRequired="true" keyOrdered="false"
	 *        valueType="se.chalmers.cse.mdsd1617.group4.Booking.Extra" valueMany="true" valueOrdered="false"
	 * @generated
	 */
	EClass getRoomIDToExtrasMap();

	/**
	 * Returns the meta object for the attribute '{@link java.util.Map.Entry <em>Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Key</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoomIDToExtrasMap()
	 * @generated
	 */
	EAttribute getRoomIDToExtrasMap_Key();

	/**
	 * Returns the meta object for the reference list '{@link java.util.Map.Entry <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Value</em>'.
	 * @see java.util.Map.Entry
	 * @see #getRoomIDToExtrasMap()
	 * @generated
	 */
	EReference getRoomIDToExtrasMap_Value();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Booking.Extra <em>Extra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extra</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Extra
	 * @generated
	 */
	EClass getExtra();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.Extra#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Extra#getPrice()
	 * @see #getExtra()
	 * @generated
	 */
	EAttribute getExtra_Price();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.Extra#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.Extra#getDescription()
	 * @see #getExtra()
	 * @generated
	 */
	EAttribute getExtra_Description();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler
	 * @generated
	 */
	EClass getBookingHandler();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getBooking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getBooking()
	 * @see #getBookingHandler()
	 * @generated
	 */
	EReference getBookingHandler_Booking();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getIroombookinghandler <em>Iroombookinghandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroombookinghandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getIroombookinghandler()
	 * @see #getBookingHandler()
	 * @generated
	 */
	EReference getBookingHandler_Iroombookinghandler();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getCurrentBookingID <em>Current Booking ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Booking ID</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getCurrentBookingID()
	 * @see #getBookingHandler()
	 * @generated
	 */
	EAttribute getBookingHandler_CurrentBookingID();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler <em>IBooking Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler
	 * @generated
	 */
	EClass getIBookingHandler();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingHandler__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIBookingHandler__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#confirmBooking(int)
	 * @generated
	 */
	EOperation getIBookingHandler__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIBookingHandler__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#checkInRoom(se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#checkInRoom(se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType, int)
	 * @generated
	 */
	EOperation getIBookingHandler__CheckInRoom__IRoomType_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#initiateCheckIn(int) <em>Initiate Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#initiateCheckIn(int)
	 * @generated
	 */
	EOperation getIBookingHandler__InitiateCheckIn__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#checkInPhysicalRoom(int, int) <em>Check In Physical Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Physical Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#checkInPhysicalRoom(int, int)
	 * @generated
	 */
	EOperation getIBookingHandler__CheckInPhysicalRoom__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#editBooking(se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#editBooking(se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO)
	 * @generated
	 */
	EOperation getIBookingHandler__EditBooking__BookingDTO();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#cancelBooking(int)
	 * @generated
	 */
	EOperation getIBookingHandler__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#listBookings()
	 * @generated
	 */
	EOperation getIBookingHandler__ListBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#listOccupied(java.util.Date) <em>List Occupied</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#listOccupied(java.util.Date)
	 * @generated
	 */
	EOperation getIBookingHandler__ListOccupied__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#listCheckIns(java.util.Date, java.util.Date) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#listCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIBookingHandler__ListCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#listCheckOuts(java.util.Date, java.util.Date) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#listCheckOuts(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIBookingHandler__ListCheckOuts__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#addExtraCostToRoom(int, int, java.lang.String, double) <em>Add Extra Cost To Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost To Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#addExtraCostToRoom(int, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getIBookingHandler__AddExtraCostToRoom__int_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIBookingHandler__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIBookingHandler__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#getBookingByID(int) <em>Get Booking By ID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Booking By ID</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler#getBookingByID(int)
	 * @generated
	 */
	EOperation getIBookingHandler__GetBookingByID__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp <em>IBooking Start Up</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Start Up</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp
	 * @generated
	 */
	EClass getIBookingStartUp();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp#clearBookings() <em>Clear Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp#clearBookings()
	 * @generated
	 */
	EOperation getIBookingStartUp__ClearBookings();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus
	 * @generated
	 */
	EEnum getBookingStatus();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO <em>DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO
	 * @generated
	 */
	EClass getBookingDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getDateTo <em>Date To</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date To</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getDateTo()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EAttribute getBookingDTO_DateTo();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getDateFrom <em>Date From</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Date From</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getDateFrom()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EAttribute getBookingDTO_DateFrom();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getFirstName()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EAttribute getBookingDTO_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getLastName()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EAttribute getBookingDTO_LastName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getBookingID <em>Booking ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking ID</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getBookingID()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EAttribute getBookingDTO_BookingID();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getExtradto <em>Extradto</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Extradto</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getExtradto()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EReference getBookingDTO_Extradto();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getIroom <em>Iroom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Iroom</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getIroom()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EReference getBookingDTO_Iroom();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getIroomtype <em>Iroomtype</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomtype</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getIroomtype()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EReference getBookingDTO_Iroomtype();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getBookingstatus <em>Bookingstatus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Bookingstatus</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getBookingstatus()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EAttribute getBookingDTO_Bookingstatus();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getPaidstatus <em>Paidstatus</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Paidstatus</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getPaidstatus()
	 * @see #getBookingDTO()
	 * @generated
	 */
	EAttribute getBookingDTO_Paidstatus();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO <em>Extra DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Extra DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO
	 * @generated
	 */
	EClass getExtraDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO#getDescription()
	 * @see #getExtraDTO()
	 * @generated
	 */
	EAttribute getExtraDTO_Description();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO#getPrice()
	 * @see #getExtraDTO()
	 * @generated
	 */
	EAttribute getExtraDTO_Price();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO#getRoomNr <em>Room Nr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Nr</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO#getRoomNr()
	 * @see #getExtraDTO()
	 * @generated
	 */
	EAttribute getExtraDTO_RoomNr();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	BookingFactory getBookingFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Iroom</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__IROOM = eINSTANCE.getBooking_Iroom();

		/**
		 * The meta object literal for the '<em><b>Iroomtype</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__IROOMTYPE = eINSTANCE.getBooking_Iroomtype();

		/**
		 * The meta object literal for the '<em><b>Date From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__DATE_FROM = eINSTANCE.getBooking_DateFrom();

		/**
		 * The meta object literal for the '<em><b>Date To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__DATE_TO = eINSTANCE.getBooking_DateTo();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__FIRST_NAME = eINSTANCE.getBooking_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__LAST_NAME = eINSTANCE.getBooking_LastName();

		/**
		 * The meta object literal for the '<em><b>Bookingstatus</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOKINGSTATUS = eINSTANCE.getBooking_Bookingstatus();

		/**
		 * The meta object literal for the '<em><b>Booking ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOKING_ID = eINSTANCE.getBooking_BookingID();

		/**
		 * The meta object literal for the '<em><b>Roomidtoextrasmap</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__ROOMIDTOEXTRASMAP = eINSTANCE.getBooking_Roomidtoextrasmap();

		/**
		 * The meta object literal for the '<em><b>Set IRoom List</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___SET_IROOM_LIST__ELIST = eINSTANCE.getBooking__SetIRoomList__EList();

		/**
		 * The meta object literal for the '<em><b>Set IRoom Type List</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___SET_IROOM_TYPE_LIST__ELIST = eINSTANCE.getBooking__SetIRoomTypeList__EList();

		/**
		 * The meta object literal for the '<em><b>Add Extra</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation BOOKING___ADD_EXTRA__EXTRA = eINSTANCE.getBooking__AddExtra__Extra();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.RoomIDToExtrasMapImpl <em>Room ID To Extras Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.RoomIDToExtrasMapImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getRoomIDToExtrasMap()
		 * @generated
		 */
		EClass ROOM_ID_TO_EXTRAS_MAP = eINSTANCE.getRoomIDToExtrasMap();

		/**
		 * The meta object literal for the '<em><b>Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_ID_TO_EXTRAS_MAP__KEY = eINSTANCE.getRoomIDToExtrasMap_Key();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_ID_TO_EXTRAS_MAP__VALUE = eINSTANCE.getRoomIDToExtrasMap_Value();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.ExtraImpl <em>Extra</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.ExtraImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getExtra()
		 * @generated
		 */
		EClass EXTRA = eINSTANCE.getExtra();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA__PRICE = eINSTANCE.getExtra_Price();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA__DESCRIPTION = eINSTANCE.getExtra_Description();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingHandlerImpl <em>Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingHandlerImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getBookingHandler()
		 * @generated
		 */
		EClass BOOKING_HANDLER = eINSTANCE.getBookingHandler();

		/**
		 * The meta object literal for the '<em><b>Booking</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_HANDLER__BOOKING = eINSTANCE.getBookingHandler_Booking();

		/**
		 * The meta object literal for the '<em><b>Iroombookinghandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_HANDLER__IROOMBOOKINGHANDLER = eINSTANCE.getBookingHandler_Iroombookinghandler();

		/**
		 * The meta object literal for the '<em><b>Current Booking ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_HANDLER__CURRENT_BOOKING_ID = eINSTANCE.getBookingHandler_CurrentBookingID();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler <em>IBooking Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getIBookingHandler()
		 * @generated
		 */
		EClass IBOOKING_HANDLER = eINSTANCE.getIBookingHandler();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIBookingHandler__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIBookingHandler__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___CONFIRM_BOOKING__INT = eINSTANCE.getIBookingHandler__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIBookingHandler__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE_INT = eINSTANCE.getIBookingHandler__CheckInRoom__IRoomType_int();

		/**
		 * The meta object literal for the '<em><b>Initiate Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___INITIATE_CHECK_IN__INT = eINSTANCE.getIBookingHandler__InitiateCheckIn__int();

		/**
		 * The meta object literal for the '<em><b>Check In Physical Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___CHECK_IN_PHYSICAL_ROOM__INT_INT = eINSTANCE.getIBookingHandler__CheckInPhysicalRoom__int_int();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___EDIT_BOOKING__BOOKINGDTO = eINSTANCE.getIBookingHandler__EditBooking__BookingDTO();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___CANCEL_BOOKING__INT = eINSTANCE.getIBookingHandler__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___LIST_BOOKINGS = eINSTANCE.getIBookingHandler__ListBookings();

		/**
		 * The meta object literal for the '<em><b>List Occupied</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___LIST_OCCUPIED__DATE = eINSTANCE.getIBookingHandler__ListOccupied__Date();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___LIST_CHECK_INS__DATE_DATE = eINSTANCE.getIBookingHandler__ListCheckIns__Date_Date();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___LIST_CHECK_OUTS__DATE_DATE = eINSTANCE.getIBookingHandler__ListCheckOuts__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost To Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE = eINSTANCE.getIBookingHandler__AddExtraCostToRoom__int_int_String_double();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___INITIATE_CHECKOUT__INT = eINSTANCE.getIBookingHandler__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIBookingHandler__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>Get Booking By ID</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_HANDLER___GET_BOOKING_BY_ID__INT = eINSTANCE.getIBookingHandler__GetBookingByID__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp <em>IBooking Start Up</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getIBookingStartUp()
		 * @generated
		 */
		EClass IBOOKING_START_UP = eINSTANCE.getIBookingStartUp();

		/**
		 * The meta object literal for the '<em><b>Clear Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_START_UP___CLEAR_BOOKINGS = eINSTANCE.getIBookingStartUp__ClearBookings();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getBookingStatus()
		 * @generated
		 */
		EEnum BOOKING_STATUS = eINSTANCE.getBookingStatus();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl <em>DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getBookingDTO()
		 * @generated
		 */
		EClass BOOKING_DTO = eINSTANCE.getBookingDTO();

		/**
		 * The meta object literal for the '<em><b>Date To</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_DTO__DATE_TO = eINSTANCE.getBookingDTO_DateTo();

		/**
		 * The meta object literal for the '<em><b>Date From</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_DTO__DATE_FROM = eINSTANCE.getBookingDTO_DateFrom();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_DTO__FIRST_NAME = eINSTANCE.getBookingDTO_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_DTO__LAST_NAME = eINSTANCE.getBookingDTO_LastName();

		/**
		 * The meta object literal for the '<em><b>Booking ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_DTO__BOOKING_ID = eINSTANCE.getBookingDTO_BookingID();

		/**
		 * The meta object literal for the '<em><b>Extradto</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_DTO__EXTRADTO = eINSTANCE.getBookingDTO_Extradto();

		/**
		 * The meta object literal for the '<em><b>Iroom</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_DTO__IROOM = eINSTANCE.getBookingDTO_Iroom();

		/**
		 * The meta object literal for the '<em><b>Iroomtype</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_DTO__IROOMTYPE = eINSTANCE.getBookingDTO_Iroomtype();

		/**
		 * The meta object literal for the '<em><b>Bookingstatus</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_DTO__BOOKINGSTATUS = eINSTANCE.getBookingDTO_Bookingstatus();

		/**
		 * The meta object literal for the '<em><b>Paidstatus</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING_DTO__PAIDSTATUS = eINSTANCE.getBookingDTO_Paidstatus();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.ExtraDTOImpl <em>Extra DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.ExtraDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl#getExtraDTO()
		 * @generated
		 */
		EClass EXTRA_DTO = eINSTANCE.getExtraDTO();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA_DTO__DESCRIPTION = eINSTANCE.getExtraDTO_Description();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA_DTO__PRICE = eINSTANCE.getExtraDTO_Price();

		/**
		 * The meta object literal for the '<em><b>Room Nr</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTRA_DTO__ROOM_NR = eINSTANCE.getExtraDTO_RoomNr();

	}

} //BookingPackage
