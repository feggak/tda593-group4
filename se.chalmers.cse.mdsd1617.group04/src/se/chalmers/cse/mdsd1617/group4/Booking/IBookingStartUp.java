/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IBooking Start Up</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getIBookingStartUp()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IBookingStartUp extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearBookings();
} // IBookingStartUp
