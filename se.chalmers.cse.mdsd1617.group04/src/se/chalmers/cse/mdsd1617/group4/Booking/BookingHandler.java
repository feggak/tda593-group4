/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking;

import org.eclipse.emf.common.util.EList;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getBooking <em>Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getIroombookinghandler <em>Iroombookinghandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getCurrentBookingID <em>Current Booking ID</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingHandler()
 * @model
 * @generated
 */
public interface BookingHandler extends IBookingHandler, IBookingStartUp {
	/**
	 * Returns the value of the '<em><b>Booking</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group4.Booking.Booking}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingHandler_Booking()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Booking> getBooking();

	/**
	 * Returns the value of the '<em><b>Iroombookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroombookinghandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroombookinghandler</em>' reference.
	 * @see #setIroombookinghandler(IRoomBookingHandler)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingHandler_Iroombookinghandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomBookingHandler getIroombookinghandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getIroombookinghandler <em>Iroombookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroombookinghandler</em>' reference.
	 * @see #getIroombookinghandler()
	 * @generated
	 */
	void setIroombookinghandler(IRoomBookingHandler value);

	/**
	 * Returns the value of the '<em><b>Current Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Booking ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Booking ID</em>' attribute.
	 * @see #setCurrentBookingID(int)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingHandler_CurrentBookingID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getCurrentBookingID();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler#getCurrentBookingID <em>Current Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Booking ID</em>' attribute.
	 * @see #getCurrentBookingID()
	 * @generated
	 */
	void setCurrentBookingID(int value);

} // BookingHandler
