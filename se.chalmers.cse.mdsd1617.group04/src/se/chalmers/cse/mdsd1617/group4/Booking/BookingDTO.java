/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking;

import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getDateTo <em>Date To</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getDateFrom <em>Date From</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getFirstName <em>First Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getLastName <em>Last Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getExtradto <em>Extradto</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getIroom <em>Iroom</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getIroomtype <em>Iroomtype</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getBookingstatus <em>Bookingstatus</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getPaidstatus <em>Paidstatus</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO()
 * @model
 * @generated
 */
public interface BookingDTO extends EObject {

	/**
	 * Returns the value of the '<em><b>Date To</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date To</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date To</em>' attribute.
	 * @see #setDateTo(Date)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_DateTo()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getDateTo();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getDateTo <em>Date To</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date To</em>' attribute.
	 * @see #getDateTo()
	 * @generated
	 */
	void setDateTo(Date value);

	/**
	 * Returns the value of the '<em><b>Date From</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Date From</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Date From</em>' attribute.
	 * @see #setDateFrom(Date)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_DateFrom()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Date getDateFrom();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getDateFrom <em>Date From</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Date From</em>' attribute.
	 * @see #getDateFrom()
	 * @generated
	 */
	void setDateFrom(Date value);

	/**
	 * Returns the value of the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Name</em>' attribute.
	 * @see #setFirstName(String)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_FirstName()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getFirstName();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getFirstName <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Name</em>' attribute.
	 * @see #getFirstName()
	 * @generated
	 */
	void setFirstName(String value);

	/**
	 * Returns the value of the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Name</em>' attribute.
	 * @see #setLastName(String)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_LastName()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getLastName();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getLastName <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Name</em>' attribute.
	 * @see #getLastName()
	 * @generated
	 */
	void setLastName(String value);

	/**
	 * Returns the value of the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking ID</em>' attribute.
	 * @see #setBookingID(int)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_BookingID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getBookingID();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getBookingID <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking ID</em>' attribute.
	 * @see #getBookingID()
	 * @generated
	 */
	void setBookingID(int value);

	/**
	 * Returns the value of the '<em><b>Extradto</b></em>' containment reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group4.Booking.ExtraDTO}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extradto</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extradto</em>' containment reference list.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_Extradto()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ExtraDTO> getExtradto();

	/**
	 * Returns the value of the '<em><b>Iroom</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group4.Room.IRoom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroom</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroom</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_Iroom()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IRoom> getIroom();

	/**
	 * Returns the value of the '<em><b>Iroomtype</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtype</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtype</em>' reference.
	 * @see #setIroomtype(IRoomType)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_Iroomtype()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomType getIroomtype();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getIroomtype <em>Iroomtype</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomtype</em>' reference.
	 * @see #getIroomtype()
	 * @generated
	 */
	void setIroomtype(IRoomType value);

	/**
	 * Returns the value of the '<em><b>Bookingstatus</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookingstatus</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookingstatus</em>' attribute.
	 * @see #setBookingstatus(String)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_Bookingstatus()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getBookingstatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getBookingstatus <em>Bookingstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Bookingstatus</em>' attribute.
	 * @see #getBookingstatus()
	 * @generated
	 */
	void setBookingstatus(String value);

	/**
	 * Returns the value of the '<em><b>Paidstatus</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paidstatus</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paidstatus</em>' attribute.
	 * @see #setPaidstatus(String)
	 * @see se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage#getBookingDTO_Paidstatus()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getPaidstatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO#getPaidstatus <em>Paidstatus</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Paidstatus</em>' attribute.
	 * @see #getPaidstatus()
	 * @generated
	 */
	void setPaidstatus(String value);
} // BookingDTO
