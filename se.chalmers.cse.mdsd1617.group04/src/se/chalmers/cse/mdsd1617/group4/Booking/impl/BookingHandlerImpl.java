/**
 */
package se.chalmers.cse.mdsd1617.group4.Booking.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import se.chalmers.cse.mdsd1617.group4.Booking.Booking;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingHandler;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group4.Booking.Extra;
import se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp;
import se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler;
import se.chalmers.cse.mdsd1617.group4.Room.RoomDTO;
import se.chalmers.cse.mdsd1617.group4.Room.RoomFactory;
import se.chalmers.cse.mdsd1617.group4.Room.RoomStatus;
import se.chalmers.cse.mdsd1617.group4.Room.impl.RoomFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;
import se.chalmers.cse.mdsd1617.group4.impl.FreeRoomTypesDTOImpl;
import se.chalmers.cse.mdsd1617.group4.impl.Group4FactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingHandlerImpl#getBooking <em>Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingHandlerImpl#getIroombookinghandler <em>Iroombookinghandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingHandlerImpl#getCurrentBookingID <em>Current Booking ID</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingHandlerImpl extends MinimalEObjectImpl.Container implements BookingHandler {
	/**
	 * The cached value of the '{@link #getBooking() <em>Booking</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBooking()
	 * @generated
	 * @ordered
	 */
	protected EList<Booking> booking;

	/**
	 * The cached value of the '{@link #getIroombookinghandler() <em>Iroombookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroombookinghandler()
	 * @generated
	 * @ordered
	 */
	protected IRoomBookingHandler iroombookinghandler;

	/**
	 * The default value of the '{@link #getCurrentBookingID() <em>Current Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentBookingID()
	 * @generated
	 * @ordered
	 */
	protected static final int CURRENT_BOOKING_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCurrentBookingID() <em>Current Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentBookingID()
	 * @generated
	 * @ordered
	 */
	protected int currentBookingID = CURRENT_BOOKING_ID_EDEFAULT;

	private static BookingHandlerImpl instance = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected BookingHandlerImpl() {
		super();
		booking = new BasicEList<Booking>();

		RoomFactory roomFactory = RoomFactoryImpl.init();
		iroombookinghandler = (IRoomBookingHandler) roomFactory.createRoomHandler();
	}
	
	public static synchronized BookingHandler getInstance(){
		if(instance == null){
			instance = new BookingHandlerImpl();
		}
		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return BookingPackage.Literals.BOOKING_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Booking> getBooking() {
		if (booking == null) {
			booking = new EObjectResolvingEList<Booking>(Booking.class, this, BookingPackage.BOOKING_HANDLER__BOOKING);
		}
		return booking;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomBookingHandler getIroombookinghandler() {
		if (iroombookinghandler != null && iroombookinghandler.eIsProxy()) {
			InternalEObject oldIroombookinghandler = (InternalEObject)iroombookinghandler;
			iroombookinghandler = (IRoomBookingHandler)eResolveProxy(oldIroombookinghandler);
			if (iroombookinghandler != oldIroombookinghandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, BookingPackage.BOOKING_HANDLER__IROOMBOOKINGHANDLER, oldIroombookinghandler, iroombookinghandler));
			}
		}
		return iroombookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomBookingHandler basicGetIroombookinghandler() {
		return iroombookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroombookinghandler(IRoomBookingHandler newIroombookinghandler) {
		IRoomBookingHandler oldIroombookinghandler = iroombookinghandler;
		iroombookinghandler = newIroombookinghandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_HANDLER__IROOMBOOKINGHANDLER, oldIroombookinghandler, iroombookinghandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCurrentBookingID() {
		return currentBookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentBookingID(int newCurrentBookingID) {
		int oldCurrentBookingID = currentBookingID;
		currentBookingID = newCurrentBookingID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, BookingPackage.BOOKING_HANDLER__CURRENT_BOOKING_ID, oldCurrentBookingID, currentBookingID));
	}

	private Date stringToDateConverter(String date){
		Date parsedDate = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		try {
			parsedDate = format.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return parsedDate;
	}
	
	private String dateToStringConverter(Date date){
		String parsedDate = null;
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		parsedDate = format.format(date);	
		return parsedDate;
	}
	
	public Booking getBookingByID(int bookingID){
		for (Booking b : booking){
			if (b.getBookingID() == bookingID) {
				return b;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		
		Date dateFrom = stringToDateConverter(startDate);
		Date dateTo = stringToDateConverter(endDate);
		
		EList<FreeRoomTypesDTO> freeRooms = new BasicEList<>();
		EList<IRoom> rooms = iroombookinghandler.getAllRooms(); // use get method instead?

		//if the dates are not correct, we should return an empty list
		if (dateFrom.after(dateTo)) {
			return freeRooms;
		}
		
		//create freeRooms out of all rooms  
		Group4FactoryImpl factory =  new Group4FactoryImpl();
		
		for(int i=0; i < rooms.size(); i++ ){
			
			IRoom r = rooms.get(i);
			
			if(r.getRoomType().getNumberOfBeds() < numBeds ){
				continue;
			}
			
			boolean flag = false; // does this free room type exist?
			
			for (int j = 0; j < freeRooms.size(); j++) {
				if(freeRooms.get(j).getRoomTypeDescription().equals(r.getRoomType().getDescription())){
					flag = true;
					freeRooms.get(j).setNumFreeRooms(freeRooms.get(j).getNumFreeRooms() + 1);
					break;
				}
			}
			
			if(!flag){
				FreeRoomTypesDTOImpl freeRoomType = (FreeRoomTypesDTOImpl) factory.createFreeRoomTypesDTO();
				
				freeRoomType.setNumBeds(r.getRoomType().getNumberOfBeds());
				freeRoomType.setNumFreeRooms(1);
				freeRoomType.setRoomTypeDescription(r.getRoomType().getDescription());
				freeRooms.add(freeRoomType);
			}
		}
		
		// filter out those rooms which are booked
		
		for(int i=0; i < booking.size(); i++ ){
			
			Booking b = booking.get(i); // go through all bookings
			
			if(b.getDateFrom().before(dateFrom) && b.getDateTo().after(dateFrom) || b.getDateFrom().before(dateTo) && b.getDateTo().after(dateTo)){
				continue; // if its in different period, it doesn't bother us
			}
			
			for(int j=0; j < b.getIroomtype().size(); j++ ){ // go through all room types in booking
				
				if(b.getIroomtype().get(j).getNumberOfBeds() != numBeds){ // rooms with different number of beds dont bother us
					continue;
				}
				
				// reduce this room in our result 
				
				String type = b.getIroomtype().get(j).getDescription();
				
				for(int k=0; k<freeRooms.size(); k++){
					if(type.equals(freeRooms.get(k).getRoomTypeDescription())){
						freeRooms.get(k).setNumFreeRooms(freeRooms.get(k).getNumFreeRooms() -1);
					}
					
				}
				
			}
			
		}
		
		//return the rest
		return freeRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		Date start = stringToDateConverter(startDate);
		Date end =stringToDateConverter(endDate);
		if(firstName == null || lastName == null || start.after(end) || firstName.isEmpty() || lastName.isEmpty()){
			return -1;
		}
		Booking b = new BookingImpl	(firstName,lastName, start, end, currentBookingID);
		booking.add(b);
		currentBookingID++;
		return b.getBookingID();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->.
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		Booking b = getBookingByID(bookingID);
		if(b!=null && b.getBookingstatus().equals(BookingStatus.INITIATED)){
			if(!b.getIroomtype().isEmpty()){
				b.setBookingstatus(BookingStatus.CONFIRMED);
				return true;
			}
		}
		return false;	
	}

	/**
	 * This method adds a fictional room (a RoomType) to a booking
	 * 
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		Booking booking = this.getBookingByID(bookingID);
		if(booking == null || !booking.getBookingstatus().equals(BookingStatus.INITIATED)){
			return false;
		}
		
		//IRoom room = this.getFreeRooms(numBeds, startDate, endDate)
		EList <IRoom> rooms = iroombookinghandler.getAllRooms();
		Set <IRoomType> roomTypes = new HashSet<IRoomType>();
		
		for (int i = 0; i < rooms.size(); i++) {
			roomTypes.add(rooms.get(i).getRoomType());
		}
		
		IRoomType roomtype=null;
		for (Iterator<IRoomType> it = roomTypes.iterator(); it.hasNext(); ) {
	        IRoomType f = it.next();
	        if (f.getName().equals(roomTypeDescription) || f.getDescription().equals(roomTypeDescription)){
	        	roomtype = f;
	        }
	    }
		
		if(roomtype == null){
			return false;
		}
		
		EList<FreeRoomTypesDTO> freeRooms = getFreeRooms(roomtype.getNumberOfBeds(),
				dateToStringConverter(booking.getDateFrom()),
				dateToStringConverter(booking.getDateTo()));
		
		if(freeRooms == null){
			return false;
		}

		for(FreeRoomTypesDTO free : freeRooms){
			
			if(free.getRoomTypeDescription().equals(roomtype.getDescription()) && free.getNumFreeRooms() > 0){
				booking.getIroomtype().add(roomtype);
				return true;
			}
			
		}
		
		return false;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(IRoomType roomType, int bookingID) {
		Booking booking = getBookingByID(bookingID);
		if(booking == null || !booking.getBookingstatus().equals(BookingStatus.CONFIRMED)){
			return -1;
		}
		if (!booking.getIroomtype().contains(roomType)){
			return -1;
		}
		
		int roomNr = iroombookinghandler.checkInRoom(roomType);
		
		if(roomNr == -1 ){
			return -1;
		}
		
		EList<IRoom> bookingRooms = booking.getIroom();
		boolean added = bookingRooms.add(iroombookinghandler.getRoomByNr(roomNr)); //is this really needed? is it ever false?
		
		if (added) {
			booking.setIRoomList(bookingRooms);
			booking.getIroomtype().remove(roomType);
			if (booking.getIroomtype().isEmpty()) {
				booking.setBookingstatus(BookingStatus.CHECKEDIN);
			}
			return roomNr;
		} else {
			return -1;
		}
	}

	/**
	 * Does not set the booking status to paid 
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNr, int bookingID) {

		Booking b = getBookingByID(bookingID);
		
		if(b == null) {
			return -1;
		}
		
		IRoom r = null;
		for (IRoom room : b.getIroom()) {
			if (room.getRoomID() == roomNr) {
				r = room;
				break;
			}
		}
		
		if (r == null) {
			return -1;
		}
		
		if (r.getStatus() != RoomStatus.OCCUPIED) {
			return -1;
		}
		
		iroombookinghandler.checkOutPsysicalRoom(roomNr);
		
		double price = r.getRoomType().getPrice();
		
		if(b.getRoomidtoextrasmap() == null || b.getRoomidtoextrasmap().size() == 0){
			//dont add extras to price since its null
		}else{
			EList<Extra> extraList = b.getRoomidtoextrasmap().get(roomNr).getValue();
			
			for(int i=0; i<extraList.size(); i++){
				price = price + extraList.get(i).getPrice();
			}
		}
		
		b.getIroom().remove(r);

		if(b.getIroom().size()==0){
			b.setBookingstatus(BookingStatus.CHECKEDOUT);
		}
        
		double days = getLengthOfBooking(b.getDateFrom(), b.getDateTo());
		
		return price * days;
	}

	// returning set instead of elist would be better
	/**
	 * @generated NOT
	 */
	public EList<RoomDTO> initiateCheckIn(int bookingID) {

		Booking b = getBookingByID(bookingID);
		
		EList<RoomDTO> allDTOs = iroombookinghandler.getAllRoomDTOs(); // maybe this should be get all free rooms dtos
		EList<RoomDTO> bookingDTOs = new BasicEList<RoomDTO>();
		
		if (b == null) {
			throw new IllegalArgumentException("No booking with that ID!");
		}
		
		if (b.getBookingstatus() != BookingStatus.CONFIRMED) {
			throw new IllegalArgumentException("Booking needs to be confirmed to be able to check in!");
		}
		
		for (RoomDTO roomDTO : allDTOs) {
			for (IRoomType roomType : b.getIroomtype()) {
				if (roomDTO.getRoomTypeName().equals(roomType.getName()) &&
						roomDTO.getRoomStatus() == RoomStatus.FREE &&
						!bookingDTOs.contains(roomDTO)) {
					bookingDTOs.add(roomDTO);
				}
			}
		}
		
		return bookingDTOs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkInPhysicalRoom(int bookingID, int roomNr) {

		// This function is likely unnecessary 

		Booking b = getBookingByID(bookingID);
		IRoom room = iroombookinghandler.getRoomByNr(roomNr);
		
		if (b == null) {
			throw new IllegalArgumentException("There is no booking with that ID!");
		}
		if (room == null) {
			throw new IllegalArgumentException("There is no room with that number!");
		}
		if (b.getBookingstatus() != BookingStatus.CONFIRMED) {
			throw new IllegalArgumentException("Booking needs to be in the confirmed state to check in rooms!");
		}
		
		for (IRoomType roomType : b.getIroomtype()) {
			if (room.getRoomType() == roomType) {
				EList<IRoom> bookingRooms = b.getIroom();
				boolean checkedIn = iroombookinghandler.checkInPsysicalRoom(roomNr);
				if (checkedIn) {
					bookingRooms.add(room);
					b.setIRoomList(bookingRooms);
					b.getIroomtype().remove(roomType);
					if (b.getIroomtype().isEmpty()) {
						b.setBookingstatus(BookingStatus.CHECKEDIN);
					}
					break;
				} else {
					throw new IllegalArgumentException("Can't check in that room!");
				}
			}
		}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void editBooking(BookingDTO bookingData) {

		
		for (int i = 0; i < booking.size(); i++) {
			if(booking.get(i).getBookingID() == bookingData.getBookingID()){
				
				booking.remove(i);
				booking.add(new BookingImpl(bookingData));
				return;
			}
		}
		
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void cancelBooking(int bookingID) {
		
		for (int i = 0; i < booking.size(); i++) {
			if(booking.get(i).getBookingID() == bookingID){
				
				//booking.remove(i); 
				
				booking.get(i).setBookingstatus(BookingStatus.CANCELLED);
				
				break;
			}
		}
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated not
	 */
	public EList<BookingDTO> listBookings() {

		EList<BookingDTO> list = new BasicEList<BookingDTO>();
		
		for (int i = 0; i < booking.size(); i++) {
			list.add(new BookingDTOImpl(booking.get(i)));
		}
		
		return list;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	/*Finds the bookings that where date is between bookings dateFrom and dateTo. 
	 * Also checks if the room connected to the booking i occupied.
	 * If both are true the rooms are added to a list.
	 * */
	public EList<IRoom> listOccupied(Date date) {
		EList<IRoom> occupiedRooms = new BasicEList<IRoom>();
		for(Booking b : booking){
			if((date.after(b.getDateFrom()) || date.compareTo(b.getDateFrom())==0) 
					&& (date.before(b.getDateTo()) || date.compareTo(b.getDateTo())==0)){
				for(IRoom room :  b.getIroom()){
					if(room.getStatus() == RoomStatus.OCCUPIED){
						occupiedRooms.add(room);
					}
						
				}
			}
		}
		return occupiedRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	//checks if the startdate of abooking is within startDate and endDate and have been checked in(other status then INITIATED)
	public EList<BookingDTO> listCheckIns(Date startDate, Date endDate) {
		EList<BookingDTO> checkedInList = new BasicEList<BookingDTO>();
		for(Booking b : booking){
			if((startDate.after(b.getDateFrom())|| startDate.compareTo(b.getDateFrom())==0)
					&&(endDate.before(b.getDateTo()) || endDate.compareTo(b.getDateTo())==0)
					&& (b.getBookingstatus() != BookingStatus.INITIATED)){
				checkedInList.add(new BookingDTOImpl(b));
			}
		}
		return checkedInList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	//checks if the startdate of abooking is within startDate and endDate and have been checked out
	public EList<BookingDTO> listCheckOuts(Date startDate, Date endDate) {
		EList<BookingDTO> checkedOutList = new BasicEList<BookingDTO>();
		for(Booking b : booking){
			if((startDate.after(b.getDateFrom())|| startDate.compareTo(b.getDateFrom())==0)
					&&(endDate.before(b.getDateTo()) || endDate.compareTo(b.getDateTo())==0)
					&& (b.getBookingstatus() == BookingStatus.CHECKEDOUT)){
				checkedOutList.add(new BookingDTOImpl(b));
			}	
		}
		return checkedOutList;
	}
		
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtraCostToRoom(int bookingID, int roomNr, String extraDescription, double price) {
	
		ExtraImpl extra = new ExtraImpl();
		extra.setDescription(extraDescription);
		extra.setPrice(price);
		
		for (int i = 0; i < booking.size(); i++) {
			if(booking.get(i).getBookingID() == bookingID){
				EList<Extra> extras = (EList<Extra>) booking.get(i).getRoomidtoextrasmap().get(roomNr);
				if(extras == null){
					booking.get(i).getRoomidtoextrasmap().put(roomNr, extras);
				}else{
					booking.get(i).getRoomidtoextrasmap().remove(roomNr);
					booking.get(i).getRoomidtoextrasmap().put(roomNr, extras);
				}
				break;
			}
		}
		
	}

	/**
	 * Checksout all rooms of a specific booking and
	 * returns the price for the hole booking
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		
		Booking b = getBookingByID(bookingID);
		
//		if (b == null || !b.getBookingstatus().equals(BookingStatus.CHECKEDIN)){
//			return -1;
//		}
		
		double price = 0;
		
		//add roomPrice for every room to price
		for (int i = 0; i < b.getIroom().size(); i++) {
			IRoom room = b.getIroom().get(i);
			int roomNr = room.getRoomID();
			double roomPrice;
		
			iroombookinghandler.checkOutPsysicalRoom(roomNr);
						
			roomPrice = room.getRoomType().getPrice(); 
			
			//add price for extras
			if(b.getRoomidtoextrasmap() == null || b.getRoomidtoextrasmap().size() == 0){
				//dont add extras to price since its null
			}else{
				EList<Extra> extraList = b.getRoomidtoextrasmap().get(roomNr).getValue();
				
				for(int k=0; k<extraList.size(); k++){
					roomPrice = roomPrice + extraList.get(k).getPrice();
				}	
			}
			price = price + roomPrice;
		}
		
		b.setBookingstatus(BookingStatus.CHECKEDOUT);
		b.getIroom().clear();
		
		double days = getLengthOfBooking(b.getDateFrom(), b.getDateTo());
		
		return price * days;
	}
	
	private double getLengthOfBooking (Date fromDate, Date toDate){
		if(toDate.before(fromDate)) return 1;
		
        long mil = toDate.getTime() - fromDate.getTime();
        mil = (((mil / 1000) / 3600) / 24);
        return (double) mil;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearBookings() {
		booking = new BasicEList<Booking>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case BookingPackage.BOOKING_HANDLER__BOOKING:
				return getBooking();
			case BookingPackage.BOOKING_HANDLER__IROOMBOOKINGHANDLER:
				if (resolve) return getIroombookinghandler();
				return basicGetIroombookinghandler();
			case BookingPackage.BOOKING_HANDLER__CURRENT_BOOKING_ID:
				return getCurrentBookingID();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case BookingPackage.BOOKING_HANDLER__BOOKING:
				getBooking().clear();
				getBooking().addAll((Collection<? extends Booking>)newValue);
				return;
			case BookingPackage.BOOKING_HANDLER__IROOMBOOKINGHANDLER:
				setIroombookinghandler((IRoomBookingHandler)newValue);
				return;
			case BookingPackage.BOOKING_HANDLER__CURRENT_BOOKING_ID:
				setCurrentBookingID((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_HANDLER__BOOKING:
				getBooking().clear();
				return;
			case BookingPackage.BOOKING_HANDLER__IROOMBOOKINGHANDLER:
				setIroombookinghandler((IRoomBookingHandler)null);
				return;
			case BookingPackage.BOOKING_HANDLER__CURRENT_BOOKING_ID:
				setCurrentBookingID(CURRENT_BOOKING_ID_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case BookingPackage.BOOKING_HANDLER__BOOKING:
				return booking != null && !booking.isEmpty();
			case BookingPackage.BOOKING_HANDLER__IROOMBOOKINGHANDLER:
				return iroombookinghandler != null;
			case BookingPackage.BOOKING_HANDLER__CURRENT_BOOKING_ID:
				return currentBookingID != CURRENT_BOOKING_ID_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IBookingStartUp.class) {
			switch (baseOperationID) {
				case BookingPackage.IBOOKING_START_UP___CLEAR_BOOKINGS: return BookingPackage.BOOKING_HANDLER___CLEAR_BOOKINGS;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case BookingPackage.BOOKING_HANDLER___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case BookingPackage.BOOKING_HANDLER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case BookingPackage.BOOKING_HANDLER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case BookingPackage.BOOKING_HANDLER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE_INT:
				return checkInRoom((IRoomType)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_HANDLER___INITIATE_CHECK_IN__INT:
				return initiateCheckIn((Integer)arguments.get(0));
			case BookingPackage.BOOKING_HANDLER___CHECK_IN_PHYSICAL_ROOM__INT_INT:
				checkInPhysicalRoom((Integer)arguments.get(0), (Integer)arguments.get(1));
				return null;
			case BookingPackage.BOOKING_HANDLER___EDIT_BOOKING__BOOKINGDTO:
				editBooking((BookingDTO)arguments.get(0));
				return null;
			case BookingPackage.BOOKING_HANDLER___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case BookingPackage.BOOKING_HANDLER___LIST_BOOKINGS:
				return listBookings();
			case BookingPackage.BOOKING_HANDLER___LIST_OCCUPIED__DATE:
				return listOccupied((Date)arguments.get(0));
			case BookingPackage.BOOKING_HANDLER___LIST_CHECK_INS__DATE_DATE:
				return listCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackage.BOOKING_HANDLER___LIST_CHECK_OUTS__DATE_DATE:
				return listCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case BookingPackage.BOOKING_HANDLER___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE:
				addExtraCostToRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
				return null;
			case BookingPackage.BOOKING_HANDLER___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case BookingPackage.BOOKING_HANDLER___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case BookingPackage.BOOKING_HANDLER___GET_BOOKING_BY_ID__INT:
				return getBookingByID((Integer)arguments.get(0));
			case BookingPackage.BOOKING_HANDLER___CLEAR_BOOKINGS:
				clearBookings();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (currentBookingID: ");
		result.append(currentBookingID);
		result.append(')');
		return result.toString();
	}

} //BookingHandlerImpl
