/**
 */
package se.chalmers.cse.mdsd1617.group4;

import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.IHotelStartupProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Admin Test</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelAdminTest#getIhoteladminprovides <em>Ihoteladminprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelAdminTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelAdminTest()
 * @model
 * @generated
 */
public interface HotelAdminTest extends EObject {
	/**
	 * Returns the value of the '<em><b>Ihoteladminprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ihoteladminprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ihoteladminprovides</em>' reference.
	 * @see #setIhoteladminprovides(IHotelAdminProvides)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelAdminTest_Ihoteladminprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IHotelAdminProvides getIhoteladminprovides();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminTest#getIhoteladminprovides <em>Ihoteladminprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ihoteladminprovides</em>' reference.
	 * @see #getIhoteladminprovides()
	 * @generated
	 */
	void setIhoteladminprovides(IHotelAdminProvides value);

	/**
	 * Returns the value of the '<em><b>Ihotelstartupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ihotelstartupprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ihotelstartupprovides</em>' reference.
	 * @see #setIhotelstartupprovides(IHotelStartupProvides)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelAdminTest_Ihotelstartupprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IHotelStartupProvides getIhotelstartupprovides();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ihotelstartupprovides</em>' reference.
	 * @see #getIhotelstartupprovides()
	 * @generated
	 */
	void setIhotelstartupprovides(IHotelStartupProvides value);

} // HotelAdminTest
