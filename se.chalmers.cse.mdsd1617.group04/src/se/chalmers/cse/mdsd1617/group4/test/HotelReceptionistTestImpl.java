/**
 */
package se.chalmers.cse.mdsd1617.group4.test;

import static org.junit.Assert.*;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.junit.Before;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group4.Group4Factory;
import se.chalmers.cse.mdsd1617.group4.Group4Package;
import se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpFactory;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl.HotelStartUpFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.Room.RoomDTO;
import se.chalmers.cse.mdsd1617.group4.impl.Group4FactoryImpl;
import se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Receptionist Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.test.HotelReceptionistTestImpl#getIhotelreceptionistprovides <em>Ihotelreceptionistprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.test.HotelReceptionistTestImpl#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelReceptionistTestImpl extends MinimalEObjectImpl.Container implements HotelReceptionistTest {
	
	private static int bookingID;

	@Before
	public void setUp() {
		if (this.ihotelstartupprovides == null) {
			HotelStartUpFactory factory = HotelStartUpFactoryImpl.init();
			this.ihotelstartupprovides = factory.createHotelStartupProvides();
		}

		this.ihotelstartupprovides.startup(10);

		if (this.ihotelreceptionistprovides == null) {
			Group4Factory factory = Group4FactoryImpl.init();
			this.ihotelreceptionistprovides = factory.createReceptionistProvides();
		}
		
		bookingID = this.ihotelreceptionistprovides.initiateBooking("Namn", "20161201", "20161220", "Namnssson");
		this.ihotelreceptionistprovides.addRoomToBooking("DEFAULT", bookingID);
		this.ihotelreceptionistprovides.addRoomToBooking("DEFAULT", bookingID);
		this.ihotelreceptionistprovides.confirmBooking(bookingID);
	}
	
	@Test
	public void checkInBooking_roomsAvailable() {
		EList<RoomDTO> rooms = ihotelreceptionistprovides.initiateCheckIn(bookingID);
		assertNotNull(rooms);
		assertNotEquals(0, rooms.size());
	}
	
	@Test
	public void checkInPsysicalRoom_isPossible() {
		EList<RoomDTO> rooms = ihotelreceptionistprovides.initiateCheckIn(bookingID);
		int roomNr = rooms.get(0).getRoomID();
		ihotelreceptionistprovides.checkInPhysicalRooms(bookingID, roomNr);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void checkInPsysicalRoom_sameRoomTwice() {
		EList<RoomDTO> rooms = ihotelreceptionistprovides.initiateCheckIn(bookingID);
		int roomNr = rooms.get(0).getRoomID();
		ihotelreceptionistprovides.checkInPhysicalRooms(bookingID, roomNr);
		ihotelreceptionistprovides.checkInPhysicalRooms(bookingID, roomNr);
	}
	
	@Test
	public void checkOutBooking_twoRoomsCorrectPrice() {
		EList<RoomDTO> rooms = ihotelreceptionistprovides.initiateCheckIn(bookingID);
		ihotelreceptionistprovides.checkInPhysicalRooms(bookingID, rooms.get(0).getRoomID());
		ihotelreceptionistprovides.checkInPhysicalRooms(bookingID, rooms.get(1).getRoomID());
		double priceToPay = ihotelreceptionistprovides.initiateCheckout(bookingID);
		assertTrue(priceToPay == 200.0);
	}
	
	@Test
	public void checkOutBooking_fourRoomsCorrectPrice() {
		int bID = this.ihotelreceptionistprovides.initiateBooking("Namn", "20161201", "20161220", "Namnssson");
		this.ihotelreceptionistprovides.addRoomToBooking("DEFAULT", bID);
		this.ihotelreceptionistprovides.addRoomToBooking("DEFAULT", bID);
		this.ihotelreceptionistprovides.addRoomToBooking("DEFAULT", bID);
		this.ihotelreceptionistprovides.addRoomToBooking("DEFAULT", bID);
		this.ihotelreceptionistprovides.confirmBooking(bID);
		EList<RoomDTO> rooms = ihotelreceptionistprovides.initiateCheckIn(bID);
		ihotelreceptionistprovides.checkInPhysicalRooms(bID, rooms.get(0).getRoomID());
		ihotelreceptionistprovides.checkInPhysicalRooms(bID, rooms.get(1).getRoomID());
		ihotelreceptionistprovides.checkInPhysicalRooms(bID, rooms.get(2).getRoomID());
		ihotelreceptionistprovides.checkInPhysicalRooms(bID, rooms.get(3).getRoomID());
		double priceToPay = ihotelreceptionistprovides.initiateCheckout(bID);
		assertTrue(priceToPay == 400.0);
	}
	
	@Test
	public void checkOutRoom_twoRooms() {
		EList<RoomDTO> rooms = ihotelreceptionistprovides.initiateCheckIn(bookingID);
		ihotelreceptionistprovides.checkInPhysicalRooms(bookingID, rooms.get(0).getRoomID());
		ihotelreceptionistprovides.checkInPhysicalRooms(bookingID, rooms.get(1).getRoomID());
		double priceToPay = ihotelreceptionistprovides.initiateRoomCheckout(rooms.get(0).getRoomID(), bookingID);
		assertEquals(priceToPay, 100.0, 0.000001);
		double priceToPay2 = ihotelreceptionistprovides.initiateRoomCheckout(rooms.get(1).getRoomID(), bookingID);
		assertEquals(priceToPay2, 100.0, 0.000001);
	}
	
	@Test
	public void checkOutAndPayRoom_oneRoom() {
		EList<RoomDTO> rooms = ihotelreceptionistprovides.initiateCheckIn(bookingID);
		int roomNr = rooms.get(0).getRoomID();
		ihotelreceptionistprovides.checkInPhysicalRooms(bookingID, roomNr);
		double price = ihotelreceptionistprovides.initiateRoomCheckout(roomNr, bookingID);
		assertTrue(price > 0);
		ihotelreceptionistprovides.payRoomDuringCheckout(roomNr, "4012345678901234", "200", 10, 18, "Namn", "Namnsson");
	}

	/**
	 * The cached value of the '{@link #getIhotelreceptionistprovides() <em>Ihotelreceptionistprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIhotelreceptionistprovides()
	 * @generated
	 * @ordered
	 */
	protected IHotelReceptionistProvides ihotelreceptionistprovides;

	/**
	 * The cached value of the '{@link #getIhotelstartupprovides() <em>Ihotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIhotelstartupprovides()
	 * @generated
	 * @ordered
	 */
	protected IHotelStartupProvides ihotelstartupprovides;

	public HotelReceptionistTestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group4Package.Literals.HOTEL_RECEPTIONIST_TEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelReceptionistProvides getIhotelreceptionistprovides() {
		if (ihotelreceptionistprovides != null && ihotelreceptionistprovides.eIsProxy()) {
			InternalEObject oldIhotelreceptionistprovides = (InternalEObject)ihotelreceptionistprovides;
			ihotelreceptionistprovides = (IHotelReceptionistProvides)eResolveProxy(oldIhotelreceptionistprovides);
			if (ihotelreceptionistprovides != oldIhotelreceptionistprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES, oldIhotelreceptionistprovides, ihotelreceptionistprovides));
			}
		}
		return ihotelreceptionistprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelReceptionistProvides basicGetIhotelreceptionistprovides() {
		return ihotelreceptionistprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIhotelreceptionistprovides(IHotelReceptionistProvides newIhotelreceptionistprovides) {
		IHotelReceptionistProvides oldIhotelreceptionistprovides = ihotelreceptionistprovides;
		ihotelreceptionistprovides = newIhotelreceptionistprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES, oldIhotelreceptionistprovides, ihotelreceptionistprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides getIhotelstartupprovides() {
		if (ihotelstartupprovides != null && ihotelstartupprovides.eIsProxy()) {
			InternalEObject oldIhotelstartupprovides = (InternalEObject)ihotelstartupprovides;
			ihotelstartupprovides = (IHotelStartupProvides)eResolveProxy(oldIhotelstartupprovides);
			if (ihotelstartupprovides != oldIhotelstartupprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES, oldIhotelstartupprovides, ihotelstartupprovides));
			}
		}
		return ihotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides basicGetIhotelstartupprovides() {
		return ihotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIhotelstartupprovides(IHotelStartupProvides newIhotelstartupprovides) {
		IHotelStartupProvides oldIhotelstartupprovides = ihotelstartupprovides;
		ihotelstartupprovides = newIhotelstartupprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES, oldIhotelstartupprovides, ihotelstartupprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES:
				if (resolve) return getIhotelreceptionistprovides();
				return basicGetIhotelreceptionistprovides();
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES:
				if (resolve) return getIhotelstartupprovides();
				return basicGetIhotelstartupprovides();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES:
				setIhotelreceptionistprovides((IHotelReceptionistProvides)newValue);
				return;
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES:
				setIhotelstartupprovides((IHotelStartupProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES:
				setIhotelreceptionistprovides((IHotelReceptionistProvides)null);
				return;
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES:
				setIhotelstartupprovides((IHotelStartupProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES:
				return ihotelreceptionistprovides != null;
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES:
				return ihotelstartupprovides != null;
		}
		return super.eIsSet(featureID);
	}

} //HotelReceptionistTestImpl
