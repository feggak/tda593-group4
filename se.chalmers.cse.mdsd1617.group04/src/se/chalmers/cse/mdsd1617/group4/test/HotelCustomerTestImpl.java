/**
 */
package se.chalmers.cse.mdsd1617.group4.test;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group4.Group4Factory;
import se.chalmers.cse.mdsd1617.group4.Group4Package;
import se.chalmers.cse.mdsd1617.group4.HotelCustomerTest;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpFactory;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.IHotelStartupProvides;
import se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl.HotelStartUpFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.impl.Group4FactoryImpl;
import se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Customer Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.test.HotelCustomerTestImpl#getIhotelcustomerprovides <em>Ihotelcustomerprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.test.HotelCustomerTestImpl#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelCustomerTestImpl extends MinimalEObjectImpl.Container implements HotelCustomerTest {

	@Before
	public void setUp() {
		if (this.ihotelstartupprovides == null) {
			HotelStartUpFactory factory = HotelStartUpFactoryImpl.init();
			this.ihotelstartupprovides = factory.createHotelStartupProvides();
		}

		this.ihotelstartupprovides.startup(10);

		if (this.ihotelcustomerprovides == null) {
			Group4Factory factory = Group4FactoryImpl.init();
			this.ihotelcustomerprovides = factory.createHotelCustomerProvides();
		}
	}

	@Test
	public void searchForFreeRoomsTest() {
		EList<FreeRoomTypesDTO> rooms = ihotelcustomerprovides.getFreeRooms(2, "20161201", "20161210");
		assertFalse(rooms.isEmpty());
	}
	
	@Test
	public void searchForFreeRooms_wrongDates() {
		EList<FreeRoomTypesDTO> rooms = ihotelcustomerprovides.getFreeRooms(2, "20161210", "20161201");
		assertTrue(rooms.isEmpty());
	}
	
	@Test
	public void makeABookingTest() {
		int bookingID = ihotelcustomerprovides.initiateBooking("Namn", "20161201", "20161210", "Namnsson");
		assertNotEquals(-1, bookingID);
		ihotelcustomerprovides.addRoomToBooking("DEFAULT", bookingID);
		ihotelcustomerprovides.addRoomToBooking("DEFAULT", bookingID);
		boolean result = ihotelcustomerprovides.confirmBooking(bookingID);
		assertTrue(result);
		int roomNr = ihotelcustomerprovides.checkInRoom("DEFAULT", bookingID);
		assertNotEquals(-1, roomNr);
		int roomNr2 = ihotelcustomerprovides.checkInRoom("DEFAULT", bookingID);
		assertNotEquals(-1, roomNr2);
	}

	/**
	 * The cached value of the '{@link #getIhotelcustomerprovides() <em>Ihotelcustomerprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIhotelcustomerprovides()
	 * @generated
	 * @ordered
	 */
	protected IHotelCustomerProvides ihotelcustomerprovides;

	/**
	 * The cached value of the '{@link #getIhotelstartupprovides() <em>Ihotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIhotelstartupprovides()
	 * @generated
	 * @ordered
	 */
	protected IHotelStartupProvides ihotelstartupprovides;

	public HotelCustomerTestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group4Package.Literals.HOTEL_CUSTOMER_TEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelCustomerProvides getIhotelcustomerprovides() {
		if (ihotelcustomerprovides != null && ihotelcustomerprovides.eIsProxy()) {
			InternalEObject oldIhotelcustomerprovides = (InternalEObject)ihotelcustomerprovides;
			ihotelcustomerprovides = (IHotelCustomerProvides)eResolveProxy(oldIhotelcustomerprovides);
			if (ihotelcustomerprovides != oldIhotelcustomerprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_CUSTOMER_TEST__IHOTELCUSTOMERPROVIDES, oldIhotelcustomerprovides, ihotelcustomerprovides));
			}
		}
		return ihotelcustomerprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelCustomerProvides basicGetIhotelcustomerprovides() {
		return ihotelcustomerprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIhotelcustomerprovides(IHotelCustomerProvides newIhotelcustomerprovides) {
		IHotelCustomerProvides oldIhotelcustomerprovides = ihotelcustomerprovides;
		ihotelcustomerprovides = newIhotelcustomerprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_CUSTOMER_TEST__IHOTELCUSTOMERPROVIDES, oldIhotelcustomerprovides, ihotelcustomerprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides getIhotelstartupprovides() {
		if (ihotelstartupprovides != null && ihotelstartupprovides.eIsProxy()) {
			InternalEObject oldIhotelstartupprovides = (InternalEObject)ihotelstartupprovides;
			ihotelstartupprovides = (IHotelStartupProvides)eResolveProxy(oldIhotelstartupprovides);
			if (ihotelstartupprovides != oldIhotelstartupprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_CUSTOMER_TEST__IHOTELSTARTUPPROVIDES, oldIhotelstartupprovides, ihotelstartupprovides));
			}
		}
		return ihotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides basicGetIhotelstartupprovides() {
		return ihotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIhotelstartupprovides(IHotelStartupProvides newIhotelstartupprovides) {
		IHotelStartupProvides oldIhotelstartupprovides = ihotelstartupprovides;
		ihotelstartupprovides = newIhotelstartupprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_CUSTOMER_TEST__IHOTELSTARTUPPROVIDES, oldIhotelstartupprovides, ihotelstartupprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group4Package.HOTEL_CUSTOMER_TEST__IHOTELCUSTOMERPROVIDES:
				if (resolve) return getIhotelcustomerprovides();
				return basicGetIhotelcustomerprovides();
			case Group4Package.HOTEL_CUSTOMER_TEST__IHOTELSTARTUPPROVIDES:
				if (resolve) return getIhotelstartupprovides();
				return basicGetIhotelstartupprovides();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group4Package.HOTEL_CUSTOMER_TEST__IHOTELCUSTOMERPROVIDES:
				setIhotelcustomerprovides((IHotelCustomerProvides)newValue);
				return;
			case Group4Package.HOTEL_CUSTOMER_TEST__IHOTELSTARTUPPROVIDES:
				setIhotelstartupprovides((IHotelStartupProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_CUSTOMER_TEST__IHOTELCUSTOMERPROVIDES:
				setIhotelcustomerprovides((IHotelCustomerProvides)null);
				return;
			case Group4Package.HOTEL_CUSTOMER_TEST__IHOTELSTARTUPPROVIDES:
				setIhotelstartupprovides((IHotelStartupProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_CUSTOMER_TEST__IHOTELCUSTOMERPROVIDES:
				return ihotelcustomerprovides != null;
			case Group4Package.HOTEL_CUSTOMER_TEST__IHOTELSTARTUPPROVIDES:
				return ihotelstartupprovides != null;
		}
		return super.eIsSet(featureID);
	}

} //HotelCustomerTestImpl
