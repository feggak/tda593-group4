package se.chalmers.cse.mdsd1617.group4.test;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import se.chalmers.cse.mdsd1617.group4.Booking.Booking;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingStatus;
import se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler;
import se.chalmers.cse.mdsd1617.group4.Booking.IBookingStartUp;
import se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.Room.RoomDTO;

public class BookingHandlerImplTest {

    private static IBookingStartUp startUp;
    private static BookingFactory factory;
    private static IBookingHandler bookingHandler;

    @BeforeClass
    public static void oneTimeSetUp() {
        if (factory == null) {
            factory = BookingFactoryImpl.init();
        }

        if (bookingHandler == null) {
            bookingHandler = factory.createBookingHandler();
            startUp = (IBookingStartUp) bookingHandler;
        }
    }

    @Before
    public void setUp() {
        startUp.clearBookings();
    }

    @Test
    public void testGetFreeRooms_atLeastOneRoom() {
        int nbrFreeRooms = bookingHandler.getFreeRooms(0, "20150101", "20151231").size();
        assertTrue(nbrFreeRooms > 0);
    }

    @Test
    public void testGetFreeRooms_minimumBeds() {
        int nbrFreeRooms_atLeastOneBed = bookingHandler.getFreeRooms(1, "20100101", "20151231").size();
        int nbrFreeRooms_atLeastTwoBeds = bookingHandler.getFreeRooms(2, "20100101", "20151231").size();

        assertTrue(nbrFreeRooms_atLeastOneBed >= nbrFreeRooms_atLeastTwoBeds);
    }

    @Test
    public void testInitiateBooking() {
        int bookingID = exampleBooking();
        assertTrue(bookingID >= 0);
    }

    @Test
    public void testInitiateBooking_wrongDates() {
        int bookingID = bookingHandler.initiateBooking("Hotel", "20151231", "20151201", "Bookingson");
        assertEquals(-1, bookingID);
    }

    @Test
    public void testAddRoomToBooking() {
        int bookingID = exampleBooking();
        boolean booked = bookingHandler.addRoomToBooking("DEFAULT", bookingID);
        assertTrue(booked);
    }

    @Test
    public void testAddRoomToBooking_lessFreeRooms() {
        int nbrOfRoomsBefore = bookingHandler.getFreeRooms(2, "20151201", "20151231").size();

        int bookingID = exampleBooking();
        boolean booked = bookingHandler.addRoomToBooking("DEFAULT", bookingID);
        assertTrue(booked);

        int nbrOfRoomsAfter = bookingHandler.getFreeRooms(2, "20151201", "20151231").size();
        assertTrue(nbrOfRoomsAfter < nbrOfRoomsBefore);
    }

    @Test
    public void testAddRoomToBooking_invalidBookingID() {
        int bookingID = exampleBooking();
        boolean booked = bookingHandler.addRoomToBooking("DEFAULT", bookingID * -1);
        assertFalse(booked);
    }

    @Test
    public void testAddRoomToBooking_invalidRoomType() {
        int bookingID = exampleBooking();
        boolean booked = bookingHandler.addRoomToBooking("This room type doesn't exist", bookingID);
        assertFalse(booked);
    }

    @Test
    public void testConfirmBooking() {
        int bookingID = exampleBooking();

        boolean booked = bookingHandler.addRoomToBooking("DEFAULT", bookingID);
        assertTrue(booked);

        assertTrue(bookingHandler.confirmBooking(bookingID));
    }

    @Test
    public void testConfirmBooking_noRooms() {
        int bookingID = exampleBooking();

        assertFalse(bookingHandler.confirmBooking(bookingID));
    }
    
    @Test
    public  void testInitiateCheckin() {
    	int bookingID = exampleBooking();
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID);
    	bookingHandler.confirmBooking(bookingID);
    	
    	int nbrOfRooms = bookingHandler.initiateCheckIn(bookingID).size();
    	assertTrue(nbrOfRooms > 0);
    }
    
    @Test
    public void testInitiateCheckout() {
    	int bookingID = exampleBooking();
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID);
    	bookingHandler.confirmBooking(bookingID);
    	List<RoomDTO> rooms = bookingHandler.initiateCheckIn(bookingID);
    	bookingHandler.checkInPhysicalRoom(bookingID, rooms.get(0).getRoomID());
    	
    	double price = bookingHandler.initiateCheckout(bookingID);
    	assertTrue(price > 0);
    }
    
    @Test
    public void testInitiateRoomCheckout() {
    	int bookingID = exampleBooking();
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID);
    	bookingHandler.confirmBooking(bookingID);
    	List<RoomDTO> rooms = bookingHandler.initiateCheckIn(bookingID);
    	bookingHandler.checkInPhysicalRoom(bookingID, rooms.get(0).getRoomID());
    	
    	double price = bookingHandler.initiateRoomCheckout(rooms.get(0).getRoomID(), bookingID);
    	assertTrue(price > 0);
    }
    
    @Test
    public void testAddExtraCostToRoom() {
    	double extraPrice = 100;
    	
    	int bookingID1 = exampleBooking();
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID1);
    	bookingHandler.confirmBooking(bookingID1);
    	List<RoomDTO> rooms = bookingHandler.initiateCheckIn(bookingID1);
    	int room1ID = rooms.get(0).getRoomID();
    	bookingHandler.checkInPhysicalRoom(bookingID1, room1ID);
    	
    	int bookingID2 = exampleBooking();
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID2);
    	bookingHandler.confirmBooking(bookingID2);
    	rooms = bookingHandler.initiateCheckIn(bookingID2);
    	int room2ID = rooms.get(0).getRoomID();
    	bookingHandler.checkInPhysicalRoom(bookingID2, room2ID);
    	bookingHandler.addExtraCostToRoom(bookingID2, room2ID, "Extra roomservice", extraPrice);
    	
    	double price1 = bookingHandler.initiateCheckout(bookingID1);
    	double price2 = bookingHandler.initiateCheckout(bookingID2);
    	assertTrue(price1 < price2);
    	assertEquals(price1, price2 + extraPrice, 0.000001);
    }

    @Test
    public void testListBookings_initiallyEmpty() {
        int nbrOfBookings = bookingHandler.listBookings().size();
        assertEquals(0, nbrOfBookings);
    }

    @Test
    public void testListBookings_oneBooking() {
        int bookingID = exampleBooking();
        boolean booked = bookingHandler.addRoomToBooking("DEFAULT", bookingID);
        assertTrue(booked);

        int nbrOfBookings = bookingHandler.listBookings().size();
        assertEquals(1, nbrOfBookings);
    }
    
    @Test
    public void testListCheckins_invalidOrderOfDates() {
    	Date startDate = new Date(115, 11, 31); // 2015-12-31
    	Date endDate = new Date(115, 11, 11); // 2015-12-01
    	assertNull(bookingHandler.listCheckIns(startDate, endDate));
    }
    
    @Test
    public void testListCheckins_futureStartAndEndDate() {
    	Date startDate = new Date(215, 11, 1); // 2115-12-01
    	Date endDate = new Date(215, 11, 31); // 2115-12-31
    	assertNull(bookingHandler.listCheckIns(startDate, endDate));
    }
    
    @Test
    public void testListCheckins_futureEndDate() {
    	Date startDate = new Date(115, 11, 1); // 2015-12-01
    	Date endDate = new Date(215, 11, 31); // 2115-12-31
    	assertNull(bookingHandler.listCheckIns(startDate, endDate));
    }
    
    @Test
    public void testListCheckins_noCheckins() {
    	Date startDate = new Date(115, 11, 1); // 2015-12-01
    	Date endDate = new Date(115, 11, 31); // 2015-12-31
    	
    	assertTrue(bookingHandler.listCheckIns(startDate, endDate).isEmpty());
    	
    	int bookingID = exampleBooking(); // Booking is from 2015-12-04 to 2015-12-06
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID);
    	bookingHandler.confirmBooking(bookingID);
    	assertTrue(bookingHandler.listCheckIns(startDate, endDate).isEmpty());
    	
    	bookingHandler.cancelBooking(bookingID);
    	assertTrue(bookingHandler.listCheckIns(startDate, endDate).isEmpty());
    }
    
    @Test
    public void testListCheckins_oneCheckin() {
    	Date startDate = new Date(115, 11, 1); // 2015-12-01
    	Date endDate = new Date(115, 11, 31); // 2015-12-31
    	
    	int bookingID = exampleBooking(); // Booking is from 2015-12-04 to 2015-12-06
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID);
    	bookingHandler.confirmBooking(bookingID);
    	List<RoomDTO> rooms = bookingHandler.initiateCheckIn(bookingID);
    	bookingHandler.checkInPhysicalRoom(bookingID, rooms.get(0).getRoomID());
    	
    	assertEquals(1, bookingHandler.listCheckIns(startDate, endDate).size());
    }
    
    @Test
    public void testListCheckouts_invalidOrderOfDates() {
    	Date startDate = new Date(115, 11, 31); // 2015-12-31
    	Date endDate = new Date(115, 11, 11); // 2015-12-01
    	assertNull(bookingHandler.listCheckOuts(startDate, endDate));
    }
    
    @Test
    public void testListCheckouts_futureStartAndEndDate() {
    	Date startDate = new Date(215, 11, 1); // 2115-12-01
    	Date endDate = new Date(215, 11, 31); // 2115-12-31
    	assertNull(bookingHandler.listCheckOuts(startDate, endDate));
    }
    
    @Test
    public void testListCheckouts_futureEndDate() {
    	Date startDate = new Date(115, 11, 1); // 2015-12-01
    	Date endDate = new Date(215, 11, 31); // 2115-12-31
    	assertNull(bookingHandler.listCheckOuts(startDate, endDate));
    }
    
    @Test
    public void testListCheckouts_noCheckouts_oneCheckin() {
    	Date startDate = new Date(115, 11, 1); // 2015-12-01
    	Date endDate = new Date(115, 11, 31); // 2015-12-31
    	
    	assertTrue(bookingHandler.listCheckOuts(startDate, endDate).isEmpty());
    	
    	int bookingID = exampleBooking(); // Booking is from 2015-12-04 to 2015-12-06
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID);
    	bookingHandler.confirmBooking(bookingID);
    	List<RoomDTO> rooms = bookingHandler.initiateCheckIn(bookingID);
    	bookingHandler.checkInPhysicalRoom(bookingID, rooms.get(0).getRoomID());
    	double price = bookingHandler.initiateCheckout(bookingID);
    	assertTrue(price > 0);
    	
    	assertTrue(bookingHandler.listCheckOuts(startDate, endDate).isEmpty());
    }
    
    @Test
    public void testListCheckouts_oneCheckout() {
    	Date startDate = new Date(115, 11, 1); // 2015-12-01
    	Date endDate = new Date(115, 11, 31); // 2015-12-31
    	
    	int bookingID = exampleBooking(); // Booking is from 2015-12-04 to 2015-12-06
    	bookingHandler.addRoomToBooking("DEFAULT", bookingID);
    	bookingHandler.confirmBooking(bookingID);
    	List<RoomDTO> rooms = bookingHandler.initiateCheckIn(bookingID);
    	bookingHandler.checkInPhysicalRoom(bookingID, rooms.get(0).getRoomID());
    	bookingHandler.initiateCheckout(bookingID);
    	
    	assertEquals(1, bookingHandler.listCheckOuts(startDate, endDate).size());
    }

    @Test
    public void bookingStatus_progress() {
        int bookingID = exampleBooking();
        Booking booking = bookingHandler.getBookingByID(bookingID);
        assertEquals(BookingStatus.INITIATED, booking.getBookingstatus());
        
        bookingHandler.addRoomToBooking("DEFAULT", bookingID);
        bookingHandler.confirmBooking(bookingID);
        assertEquals(BookingStatus.CONFIRMED, booking.getBookingstatus());
        
        List<RoomDTO> rooms = bookingHandler.initiateCheckIn(bookingID);
        bookingHandler.checkInPhysicalRoom(bookingID, rooms.get(0).getRoomID());
        assertEquals(BookingStatus.CHECKEDIN, booking.getBookingstatus());
        
        double price = bookingHandler.initiateCheckout(bookingID);
        assertTrue(price > 0);
        assertEquals(BookingStatus.CHECKEDOUT, booking.getBookingstatus());
    }
    
    @Test
    public void bookingStatus_cancelled() {
        int bookingID = exampleBooking();
        Booking booking = bookingHandler.getBookingByID(bookingID);
        
        bookingHandler.cancelBooking(bookingID);
        assertEquals(BookingStatus.CANCELLED, booking.getBookingstatus());
    }
    
    private int exampleBooking() {
    	return bookingHandler.initiateBooking("Hotel", "20151204", "20151206", "Bookingson");
    }

}
