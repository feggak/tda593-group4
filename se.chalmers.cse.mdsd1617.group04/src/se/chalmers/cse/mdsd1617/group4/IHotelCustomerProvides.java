/**
 */
package se.chalmers.cse.mdsd1617.group4;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Customer Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getBookingPrice <em>Booking Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getRoomPrice <em>Room Price</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getIHotelCustomerProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IHotelCustomerProvides extends EObject {
	/**
	 * Returns the value of the '<em><b>Booking Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking Price</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking Price</em>' attribute.
	 * @see #setBookingPrice(double)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getIHotelCustomerProvides_BookingPrice()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getBookingPrice();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getBookingPrice <em>Booking Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking Price</em>' attribute.
	 * @see #getBookingPrice()
	 * @generated
	 */
	void setBookingPrice(double value);

	/**
	 * Returns the value of the '<em><b>Room Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Price</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Price</em>' attribute.
	 * @see #setRoomPrice(double)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getIHotelCustomerProvides_RoomPrice()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getRoomPrice();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getRoomPrice <em>Room Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Price</em>' attribute.
	 * @see #getRoomPrice()
	 * @generated
	 */
	void setRoomPrice(double value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" numBedsRequired="true" numBedsOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" firstNameRequired="true" firstNameOrdered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	int initiateBooking(String firstName, String startDate, String endDate, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeDescriptionRequired="true" roomTypeDescriptionOrdered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean addRoomToBooking(String roomTypeDescription, int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	boolean confirmBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	double initiateCheckout(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" ccNumberRequired="true" ccNumberOrdered="false" ccvRequired="true" ccvOrdered="false" expiryMonthRequired="true" expiryMonthOrdered="false" expiryYearRequired="true" expiryYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNrRequired="true" roomNrOrdered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	double initiateRoomCheckout(int roomNr, int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNrRequired="true" roomNrOrdered="false" ccNrRequired="true" ccNrOrdered="false" ccvRequired="true" ccvOrdered="false" expiryMonthRequired="true" expiryMonthOrdered="false" expiryYearRequired="true" expiryYearOrdered="false" firstNameRequired="true" firstNameOrdered="false" lastNameRequired="true" lastNameOrdered="false"
	 * @generated
	 */
	boolean payRoomDuringCheckout(int roomNr, String ccNr, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	int checkInRoom(String roomType, int bookingID);

} // IHotelCustomerProvides
