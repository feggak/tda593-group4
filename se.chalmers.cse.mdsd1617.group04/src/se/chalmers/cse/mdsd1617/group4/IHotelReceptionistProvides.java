/**
 */
package se.chalmers.cse.mdsd1617.group4;

import java.util.Date;
import org.eclipse.emf.common.util.EList;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO;
import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.Room.RoomDTO;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getIHotelReceptionistProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IHotelReceptionistProvides extends IHotelCustomerProvides {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	EList<RoomDTO> initiateCheckIn(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" roomNrRequired="true" roomNrOrdered="false"
	 * @generated
	 */
	void checkInPhysicalRooms(int bookingID, int roomNr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingDataRequired="true" bookingDataOrdered="false"
	 * @generated
	 */
	void editBooking(BookingDTO bookingData);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false"
	 * @generated
	 */
	void cancelBooking(int bookingID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false"
	 * @generated
	 */
	EList<BookingDTO> listBookings();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" dateRequired="true" dateOrdered="false"
	 * @generated
	 */
	EList<IRoom> listOccupied(Date date);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<BookingDTO> listCheckIns(Date startDate, Date endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startDateRequired="true" startDateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<BookingDTO> listCheckOuts(Date startDate, Date endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model bookingIDRequired="true" bookingIDOrdered="false" roomNrRequired="true" roomNrOrdered="false" extraDescriptionRequired="true" extraDescriptionOrdered="false" priceRequired="true" priceOrdered="false"
	 *        priceAnnotation="http://www.eclipse.org/uml2/2.0.0/UML originalName='price '"
	 * @generated
	 */
	void addExtraCostToRoom(int bookingID, int roomNr, String extraDescription, double price);
} // IHotelReceptionistProvides
