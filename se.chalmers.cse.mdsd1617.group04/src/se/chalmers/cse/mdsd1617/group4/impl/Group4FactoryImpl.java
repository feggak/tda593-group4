/**
 */
package se.chalmers.cse.mdsd1617.group4.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import se.chalmers.cse.mdsd1617.group4.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Group4FactoryImpl extends EFactoryImpl implements Group4Factory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Group4Factory init() {
		try {
			Group4Factory theGroup4Factory = (Group4Factory)EPackage.Registry.INSTANCE.getEFactory(Group4Package.eNS_URI);
			if (theGroup4Factory != null) {
				return theGroup4Factory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Group4FactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group4FactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Group4Package.FREE_ROOM_TYPES_DTO: return createFreeRoomTypesDTO();
			case Group4Package.HOTEL_ADMIN_PROVIDES: return createHotelAdminProvides();
			case Group4Package.HOTEL_CUSTOMER_PROVIDES: return createHotelCustomerProvides();
			case Group4Package.RECEPTIONIST_PROVIDES: return createReceptionistProvides();
			case Group4Package.HOTEL_ADMIN_TEST: return createHotelAdminTest();
			case Group4Package.HOTEL_CUSTOMER_TEST: return createHotelCustomerTest();
			case Group4Package.HOTEL_RECEPTIONIST_TEST: return createHotelReceptionistTest();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FreeRoomTypesDTO createFreeRoomTypesDTO() {
		FreeRoomTypesDTOImpl freeRoomTypesDTO = new FreeRoomTypesDTOImpl();
		return freeRoomTypesDTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelAdminProvides createHotelAdminProvides() {
		HotelAdminProvidesImpl hotelAdminProvides = new HotelAdminProvidesImpl();
		return hotelAdminProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelCustomerProvides createHotelCustomerProvides() {
		HotelCustomerProvidesImpl hotelCustomerProvides = new HotelCustomerProvidesImpl();
		return hotelCustomerProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReceptionistProvides createReceptionistProvides() {
		ReceptionistProvidesImpl receptionistProvides = new ReceptionistProvidesImpl();
		return receptionistProvides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelAdminTest createHotelAdminTest() {
		HotelAdminTestImpl hotelAdminTest = new HotelAdminTestImpl();
		return hotelAdminTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelCustomerTest createHotelCustomerTest() {
		HotelCustomerTestImpl hotelCustomerTest = new HotelCustomerTestImpl();
		return hotelCustomerTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HotelReceptionistTest createHotelReceptionistTest() {
		HotelReceptionistTestImpl hotelReceptionistTest = new HotelReceptionistTestImpl();
		return hotelReceptionistTest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group4Package getGroup4Package() {
		return (Group4Package)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Group4Package getPackage() {
		return Group4Package.eINSTANCE;
	}

} //Group4FactoryImpl
