/**
 */
package se.chalmers.cse.mdsd1617.group4.impl;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group4.Group4Package;
import se.chalmers.cse.mdsd1617.group4.HotelAdminProvides;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler;
import se.chalmers.cse.mdsd1617.group4.Room.RoomFactory;
import se.chalmers.cse.mdsd1617.group4.Room.impl.RoomFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeFactory;
import se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Admin Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.HotelAdminProvidesImpl#getIroomtypehandler <em>Iroomtypehandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.HotelAdminProvidesImpl#getIroomhandler <em>Iroomhandler</em>}</li>
 * </ul>
 *
 * @generated NOT
 */
public class HotelAdminProvidesImpl extends MinimalEObjectImpl.Container implements HotelAdminProvides {
	
	/**
	 * The cached value of the '{@link #getIroomtypehandler() <em>Iroomtypehandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomtypehandler()
	 * @generated
	 * @ordered
	 */
	protected IRoomTypeHandler iroomtypehandler;
	/**
	 * The cached value of the '{@link #getIroomhandler() <em>Iroomhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomhandler()
	 * @generated
	 * @ordered
	 */
	protected IRoomHandler iroomhandler;
	
	protected RoomTypeFactory roomtypefactory;
	protected RoomFactory roomfactory;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HotelAdminProvidesImpl() {
		super();
		roomtypefactory = RoomTypeFactoryImpl.init();
		roomfactory = RoomFactoryImpl.init();
		
		iroomtypehandler = roomtypefactory.createRoomTypeHandler();
		iroomhandler = roomfactory.createRoomHandler();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group4Package.Literals.HOTEL_ADMIN_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeHandler getIroomtypehandler() {
		if (iroomtypehandler != null && iroomtypehandler.eIsProxy()) {
			InternalEObject oldIroomtypehandler = (InternalEObject)iroomtypehandler;
			iroomtypehandler = (IRoomTypeHandler)eResolveProxy(oldIroomtypehandler);
			if (iroomtypehandler != oldIroomtypehandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_ADMIN_PROVIDES__IROOMTYPEHANDLER, oldIroomtypehandler, iroomtypehandler));
			}
		}
		return iroomtypehandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomTypeHandler basicGetIroomtypehandler() {
		return iroomtypehandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomtypehandler(IRoomTypeHandler newIroomtypehandler) {
		IRoomTypeHandler oldIroomtypehandler = iroomtypehandler;
		iroomtypehandler = newIroomtypehandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_ADMIN_PROVIDES__IROOMTYPEHANDLER, oldIroomtypehandler, iroomtypehandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomHandler getIroomhandler() {
		if (iroomhandler != null && iroomhandler.eIsProxy()) {
			InternalEObject oldIroomhandler = (InternalEObject)iroomhandler;
			iroomhandler = (IRoomHandler)eResolveProxy(oldIroomhandler);
			if (iroomhandler != oldIroomhandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_ADMIN_PROVIDES__IROOMHANDLER, oldIroomhandler, iroomhandler));
			}
		}
		return iroomhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomHandler basicGetIroomhandler() {
		return iroomhandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomhandler(IRoomHandler newIroomhandler) {
		IRoomHandler oldIroomhandler = iroomhandler;
		iroomhandler = newIroomhandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_ADMIN_PROVIDES__IROOMHANDLER, oldIroomhandler, iroomhandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void derp(int herp) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addRoomType(String name, double price, int nbrOfBeds, String description) {
		iroomtypehandler.addRoomType(name, price, nbrOfBeds, description);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void updateRoomType(String name, double price, int nbrOfBeds, String description) {
		iroomtypehandler.updateRoomType(name, price, nbrOfBeds, description);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeRoomType(String name) {
		iroomtypehandler.removeRoomType(name);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void addRoom(int roomID, IRoomType roomType) {
		iroomhandler.addRoom(roomID, roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void removeRoom(int name) {
		iroomhandler.removeRoom(name);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unblockRoom(int roomID) {
		iroomhandler.unblockRoom(roomID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void blockRoom(int roomID) {
		iroomhandler.blockRoom(roomID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group4Package.HOTEL_ADMIN_PROVIDES__IROOMTYPEHANDLER:
				if (resolve) return getIroomtypehandler();
				return basicGetIroomtypehandler();
			case Group4Package.HOTEL_ADMIN_PROVIDES__IROOMHANDLER:
				if (resolve) return getIroomhandler();
				return basicGetIroomhandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group4Package.HOTEL_ADMIN_PROVIDES__IROOMTYPEHANDLER:
				setIroomtypehandler((IRoomTypeHandler)newValue);
				return;
			case Group4Package.HOTEL_ADMIN_PROVIDES__IROOMHANDLER:
				setIroomhandler((IRoomHandler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_ADMIN_PROVIDES__IROOMTYPEHANDLER:
				setIroomtypehandler((IRoomTypeHandler)null);
				return;
			case Group4Package.HOTEL_ADMIN_PROVIDES__IROOMHANDLER:
				setIroomhandler((IRoomHandler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_ADMIN_PROVIDES__IROOMTYPEHANDLER:
				return iroomtypehandler != null;
			case Group4Package.HOTEL_ADMIN_PROVIDES__IROOMHANDLER:
				return iroomhandler != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group4Package.HOTEL_ADMIN_PROVIDES___DERP__INT:
				derp((Integer)arguments.get(0));
				return null;
			case Group4Package.HOTEL_ADMIN_PROVIDES___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
			case Group4Package.HOTEL_ADMIN_PROVIDES___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				updateRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
			case Group4Package.HOTEL_ADMIN_PROVIDES___REMOVE_ROOM_TYPE__STRING:
				removeRoomType((String)arguments.get(0));
				return null;
			case Group4Package.HOTEL_ADMIN_PROVIDES___ADD_ROOM__INT_IROOMTYPE:
				addRoom((Integer)arguments.get(0), (IRoomType)arguments.get(1));
				return null;
			case Group4Package.HOTEL_ADMIN_PROVIDES___REMOVE_ROOM__INT:
				removeRoom((Integer)arguments.get(0));
				return null;
			case Group4Package.HOTEL_ADMIN_PROVIDES___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case Group4Package.HOTEL_ADMIN_PROVIDES___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //HotelAdminProvidesImpl
