/**
 */
package se.chalmers.cse.mdsd1617.group4.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO;
import se.chalmers.cse.mdsd1617.group4.Booking.BookingFactory;
import se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler;
import se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.Room.RoomDTO;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeFactory;
import se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeFactoryImpl;
import se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group4.Group4Package;
import se.chalmers.cse.mdsd1617.group4.ReceptionistProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.ReceptionistProvidesImpl#getBookingPrice <em>Booking Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.ReceptionistProvidesImpl#getRoomPrice <em>Room Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.ReceptionistProvidesImpl#getIbookinghandler <em>Ibookinghandler</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReceptionistProvidesImpl extends MinimalEObjectImpl.Container implements ReceptionistProvides {
	
	/**
	 * The default value of the '{@link #getBookingPrice() <em>Booking Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingPrice()
	 * @generated NOT
	 * @ordered
	 */
	protected static final double BOOKING_PRICE_EDEFAULT = -1.0;
	/**
	 * The cached value of the '{@link #getBookingPrice() <em>Booking Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingPrice()
	 * @generated
	 * @ordered
	 */
	protected double bookingPrice = BOOKING_PRICE_EDEFAULT;
	/**
	 * The default value of the '{@link #getRoomPrice() <em>Room Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomPrice()
	 * @generated NOT
	 * @ordered
	 */
	protected static final double ROOM_PRICE_EDEFAULT = -1.0;
	/**
	 * The cached value of the '{@link #getRoomPrice() <em>Room Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomPrice()
	 * @generated
	 * @ordered
	 */
	protected double roomPrice = ROOM_PRICE_EDEFAULT;
	/**
	 * The cached value of the '{@link #getIbookinghandler() <em>Ibookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIbookinghandler()
	 * @generated NOT
	 * @ordered
	 */
	protected IBookingHandler ibookinghandler;
	protected BookingFactory bookingfactory;
	protected IRoomTypeHandler iroomtypehandler;
	protected RoomTypeFactory roomtypefactory;
	protected Map<Integer, Double> roomNrToPrice;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected ReceptionistProvidesImpl() {
		super();
		bookingfactory = BookingFactoryImpl.init();
		ibookinghandler = bookingfactory.createBookingHandler();
		roomtypefactory = RoomTypeFactoryImpl.init();
		iroomtypehandler = roomtypefactory.createRoomTypeHandler();
		//map for price information about checkout initiated rooms
		roomNrToPrice = new HashMap<Integer, Double>(); 
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	protected EClass eStaticClass() {
		return Group4Package.Literals.RECEPTIONIST_PROVIDES;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getBookingPrice() {
		return bookingPrice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingPrice(double newBookingPrice) {
		double oldBookingPrice = bookingPrice;
		bookingPrice = newBookingPrice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.RECEPTIONIST_PROVIDES__BOOKING_PRICE, oldBookingPrice, bookingPrice));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getRoomPrice() {
		return roomPrice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomPrice(double newRoomPrice) {
		double oldRoomPrice = roomPrice;
		roomPrice = newRoomPrice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.RECEPTIONIST_PROVIDES__ROOM_PRICE, oldRoomPrice, roomPrice));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IBookingHandler getIbookinghandler() {
		if (ibookinghandler != null && ibookinghandler.eIsProxy()) {
			InternalEObject oldIbookinghandler = (InternalEObject)ibookinghandler;
			ibookinghandler = (IBookingHandler)eResolveProxy(oldIbookinghandler);
			if (ibookinghandler != oldIbookinghandler) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.RECEPTIONIST_PROVIDES__IBOOKINGHANDLER, oldIbookinghandler, ibookinghandler));
			}
		}
		return ibookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IBookingHandler basicGetIbookinghandler() {
		return ibookinghandler;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setIbookinghandler(IBookingHandler newIbookinghandler) {
		IBookingHandler oldIbookinghandler = ibookinghandler;
		ibookinghandler = newIbookinghandler;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.RECEPTIONIST_PROVIDES__IBOOKINGHANDLER, oldIbookinghandler, ibookinghandler));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		return ibookinghandler.getFreeRooms(numBeds, startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		return ibookinghandler.initiateBooking(firstName, startDate, endDate, lastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		return ibookinghandler.addRoomToBooking(roomTypeDescription, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		return ibookinghandler.confirmBooking(bookingID);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingID) {
		IRoomType roomType = iroomtypehandler.getRoomTypeByDescription(roomTypeDescription);
		return ibookinghandler.checkInRoom(roomType, bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		this.bookingPrice = this.ibookinghandler.initiateCheckout(bookingID);
		return this.bookingPrice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		try {
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bank = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
			if(bank.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName) && this.bookingPrice > 0){
				if(bank.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, this.bookingPrice)){
					this.bookingPrice = -1;
					return true;					
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch (SOAPException e) {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNr, int bookingID) {
		double price = this.ibookinghandler.initiateRoomCheckout(roomNr, bookingID);
		this.roomNrToPrice.put(roomNr, price);
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNr, String ccNr, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		try {
			se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires bank = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
			double price = this.roomNrToPrice.get(roomNr);
			if(bank.isCreditCardValid(ccNr, ccv, expiryMonth, expiryYear, firstName, lastName) && price > 0){
				if(bank.makePayment(ccNr, ccv, expiryMonth, expiryYear, firstName, lastName, price)){
					this.roomNrToPrice.remove(roomNr);
					return true;					
				}else{
					return false;
				}
			}else{
				return false;
			}
		} catch (SOAPException e) {
			return false;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomDTO> initiateCheckIn(int bookingID) {
		return ibookinghandler.initiateCheckIn(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkInPhysicalRooms(int bookingID, int roomNr) {
		ibookinghandler.checkInPhysicalRoom(bookingID, roomNr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void editBooking(BookingDTO bookingData) {
		ibookinghandler.editBooking(bookingData);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void cancelBooking(int bookingID) {
		ibookinghandler.cancelBooking(bookingID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<BookingDTO> listBookings() {
		return ibookinghandler.listBookings();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoom> listOccupied(Date date) {
		return ibookinghandler.listOccupied(date);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<BookingDTO> listCheckIns(Date startDate, Date endDate) {
		return ibookinghandler.listCheckIns(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<BookingDTO> listCheckOuts(Date startDate, Date endDate) {
		return ibookinghandler.listCheckOuts(startDate, endDate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtraCostToRoom(int bookingID, int roomNr, String extraDescription, double price) {
		ibookinghandler.addExtraCostToRoom(bookingID, roomNr, extraDescription, price);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group4Package.RECEPTIONIST_PROVIDES__IBOOKINGHANDLER:
				if (resolve) return getIbookinghandler();
				return basicGetIbookinghandler();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group4Package.RECEPTIONIST_PROVIDES__BOOKING_PRICE:
				setBookingPrice((Double)newValue);
				return;
			case Group4Package.RECEPTIONIST_PROVIDES__ROOM_PRICE:
				setRoomPrice((Double)newValue);
				return;
			case Group4Package.RECEPTIONIST_PROVIDES__IBOOKINGHANDLER:
				setIbookinghandler((IBookingHandler)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group4Package.RECEPTIONIST_PROVIDES__BOOKING_PRICE:
				setBookingPrice(BOOKING_PRICE_EDEFAULT);
				return;
			case Group4Package.RECEPTIONIST_PROVIDES__ROOM_PRICE:
				setRoomPrice(ROOM_PRICE_EDEFAULT);
				return;
			case Group4Package.RECEPTIONIST_PROVIDES__IBOOKINGHANDLER:
				setIbookinghandler((IBookingHandler)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group4Package.RECEPTIONIST_PROVIDES__BOOKING_PRICE:
				return bookingPrice != BOOKING_PRICE_EDEFAULT;
			case Group4Package.RECEPTIONIST_PROVIDES__ROOM_PRICE:
				return roomPrice != ROOM_PRICE_EDEFAULT;
			case Group4Package.RECEPTIONIST_PROVIDES__IBOOKINGHANDLER:
				return ibookinghandler != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group4Package.RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group4Package.RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case Group4Package.RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case Group4Package.RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case Group4Package.RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case Group4Package.RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case Group4Package.RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case Group4Package.RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case Group4Package.RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case Group4Package.RECEPTIONIST_PROVIDES___INITIATE_CHECK_IN__INT:
				return initiateCheckIn((Integer)arguments.get(0));
			case Group4Package.RECEPTIONIST_PROVIDES___CHECK_IN_PHYSICAL_ROOMS__INT_INT:
				checkInPhysicalRooms((Integer)arguments.get(0), (Integer)arguments.get(1));
				return null;
			case Group4Package.RECEPTIONIST_PROVIDES___EDIT_BOOKING__BOOKINGDTO:
				editBooking((BookingDTO)arguments.get(0));
				return null;
			case Group4Package.RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT:
				cancelBooking((Integer)arguments.get(0));
				return null;
			case Group4Package.RECEPTIONIST_PROVIDES___LIST_BOOKINGS:
				return listBookings();
			case Group4Package.RECEPTIONIST_PROVIDES___LIST_OCCUPIED__DATE:
				return listOccupied((Date)arguments.get(0));
			case Group4Package.RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE_DATE:
				return listCheckIns((Date)arguments.get(0), (Date)arguments.get(1));
			case Group4Package.RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE_DATE:
				return listCheckOuts((Date)arguments.get(0), (Date)arguments.get(1));
			case Group4Package.RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE:
				addExtraCostToRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (bookingPrice: ");
		result.append(bookingPrice);
		result.append(", roomPrice: ");
		result.append(roomPrice);
		result.append(')');
		return result.toString();
	}

} //ReceptionistProvidesImpl
