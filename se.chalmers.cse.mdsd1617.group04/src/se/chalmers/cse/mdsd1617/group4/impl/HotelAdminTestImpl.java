/**
 */
package se.chalmers.cse.mdsd1617.group4.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group4.Group4Package;
import se.chalmers.cse.mdsd1617.group4.HotelAdminTest;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.IHotelStartupProvides;

import se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Admin Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.HotelAdminTestImpl#getIhoteladminprovides <em>Ihoteladminprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.HotelAdminTestImpl#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelAdminTestImpl extends MinimalEObjectImpl.Container implements HotelAdminTest {
	/**
	 * The cached value of the '{@link #getIhoteladminprovides() <em>Ihoteladminprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIhoteladminprovides()
	 * @generated
	 * @ordered
	 */
	protected IHotelAdminProvides ihoteladminprovides;

	/**
	 * The cached value of the '{@link #getIhotelstartupprovides() <em>Ihotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIhotelstartupprovides()
	 * @generated
	 * @ordered
	 */
	protected IHotelStartupProvides ihotelstartupprovides;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HotelAdminTestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group4Package.Literals.HOTEL_ADMIN_TEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelAdminProvides getIhoteladminprovides() {
		if (ihoteladminprovides != null && ihoteladminprovides.eIsProxy()) {
			InternalEObject oldIhoteladminprovides = (InternalEObject)ihoteladminprovides;
			ihoteladminprovides = (IHotelAdminProvides)eResolveProxy(oldIhoteladminprovides);
			if (ihoteladminprovides != oldIhoteladminprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_ADMIN_TEST__IHOTELADMINPROVIDES, oldIhoteladminprovides, ihoteladminprovides));
			}
		}
		return ihoteladminprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelAdminProvides basicGetIhoteladminprovides() {
		return ihoteladminprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIhoteladminprovides(IHotelAdminProvides newIhoteladminprovides) {
		IHotelAdminProvides oldIhoteladminprovides = ihoteladminprovides;
		ihoteladminprovides = newIhoteladminprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_ADMIN_TEST__IHOTELADMINPROVIDES, oldIhoteladminprovides, ihoteladminprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides getIhotelstartupprovides() {
		if (ihotelstartupprovides != null && ihotelstartupprovides.eIsProxy()) {
			InternalEObject oldIhotelstartupprovides = (InternalEObject)ihotelstartupprovides;
			ihotelstartupprovides = (IHotelStartupProvides)eResolveProxy(oldIhotelstartupprovides);
			if (ihotelstartupprovides != oldIhotelstartupprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_ADMIN_TEST__IHOTELSTARTUPPROVIDES, oldIhotelstartupprovides, ihotelstartupprovides));
			}
		}
		return ihotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides basicGetIhotelstartupprovides() {
		return ihotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIhotelstartupprovides(IHotelStartupProvides newIhotelstartupprovides) {
		IHotelStartupProvides oldIhotelstartupprovides = ihotelstartupprovides;
		ihotelstartupprovides = newIhotelstartupprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_ADMIN_TEST__IHOTELSTARTUPPROVIDES, oldIhotelstartupprovides, ihotelstartupprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group4Package.HOTEL_ADMIN_TEST__IHOTELADMINPROVIDES:
				if (resolve) return getIhoteladminprovides();
				return basicGetIhoteladminprovides();
			case Group4Package.HOTEL_ADMIN_TEST__IHOTELSTARTUPPROVIDES:
				if (resolve) return getIhotelstartupprovides();
				return basicGetIhotelstartupprovides();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group4Package.HOTEL_ADMIN_TEST__IHOTELADMINPROVIDES:
				setIhoteladminprovides((IHotelAdminProvides)newValue);
				return;
			case Group4Package.HOTEL_ADMIN_TEST__IHOTELSTARTUPPROVIDES:
				setIhotelstartupprovides((IHotelStartupProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_ADMIN_TEST__IHOTELADMINPROVIDES:
				setIhoteladminprovides((IHotelAdminProvides)null);
				return;
			case Group4Package.HOTEL_ADMIN_TEST__IHOTELSTARTUPPROVIDES:
				setIhotelstartupprovides((IHotelStartupProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_ADMIN_TEST__IHOTELADMINPROVIDES:
				return ihoteladminprovides != null;
			case Group4Package.HOTEL_ADMIN_TEST__IHOTELSTARTUPPROVIDES:
				return ihotelstartupprovides != null;
		}
		return super.eIsSet(featureID);
	}

} //HotelAdminTestImpl
