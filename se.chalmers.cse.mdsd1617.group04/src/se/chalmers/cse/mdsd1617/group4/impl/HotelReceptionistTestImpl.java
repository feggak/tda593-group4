/**
 */
package se.chalmers.cse.mdsd1617.group4.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group4.Group4Package;
import se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.IHotelStartupProvides;

import se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hotel Receptionist Test</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.HotelReceptionistTestImpl#getIhotelreceptionistprovides <em>Ihotelreceptionistprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.impl.HotelReceptionistTestImpl#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HotelReceptionistTestImpl extends MinimalEObjectImpl.Container implements HotelReceptionistTest {
	/**
	 * The cached value of the '{@link #getIhotelreceptionistprovides() <em>Ihotelreceptionistprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIhotelreceptionistprovides()
	 * @generated
	 * @ordered
	 */
	protected IHotelReceptionistProvides ihotelreceptionistprovides;

	/**
	 * The cached value of the '{@link #getIhotelstartupprovides() <em>Ihotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIhotelstartupprovides()
	 * @generated
	 * @ordered
	 */
	protected IHotelStartupProvides ihotelstartupprovides;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HotelReceptionistTestImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group4Package.Literals.HOTEL_RECEPTIONIST_TEST;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelReceptionistProvides getIhotelreceptionistprovides() {
		if (ihotelreceptionistprovides != null && ihotelreceptionistprovides.eIsProxy()) {
			InternalEObject oldIhotelreceptionistprovides = (InternalEObject)ihotelreceptionistprovides;
			ihotelreceptionistprovides = (IHotelReceptionistProvides)eResolveProxy(oldIhotelreceptionistprovides);
			if (ihotelreceptionistprovides != oldIhotelreceptionistprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES, oldIhotelreceptionistprovides, ihotelreceptionistprovides));
			}
		}
		return ihotelreceptionistprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelReceptionistProvides basicGetIhotelreceptionistprovides() {
		return ihotelreceptionistprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIhotelreceptionistprovides(IHotelReceptionistProvides newIhotelreceptionistprovides) {
		IHotelReceptionistProvides oldIhotelreceptionistprovides = ihotelreceptionistprovides;
		ihotelreceptionistprovides = newIhotelreceptionistprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES, oldIhotelreceptionistprovides, ihotelreceptionistprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides getIhotelstartupprovides() {
		if (ihotelstartupprovides != null && ihotelstartupprovides.eIsProxy()) {
			InternalEObject oldIhotelstartupprovides = (InternalEObject)ihotelstartupprovides;
			ihotelstartupprovides = (IHotelStartupProvides)eResolveProxy(oldIhotelstartupprovides);
			if (ihotelstartupprovides != oldIhotelstartupprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES, oldIhotelstartupprovides, ihotelstartupprovides));
			}
		}
		return ihotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IHotelStartupProvides basicGetIhotelstartupprovides() {
		return ihotelstartupprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIhotelstartupprovides(IHotelStartupProvides newIhotelstartupprovides) {
		IHotelStartupProvides oldIhotelstartupprovides = ihotelstartupprovides;
		ihotelstartupprovides = newIhotelstartupprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES, oldIhotelstartupprovides, ihotelstartupprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES:
				if (resolve) return getIhotelreceptionistprovides();
				return basicGetIhotelreceptionistprovides();
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES:
				if (resolve) return getIhotelstartupprovides();
				return basicGetIhotelstartupprovides();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES:
				setIhotelreceptionistprovides((IHotelReceptionistProvides)newValue);
				return;
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES:
				setIhotelstartupprovides((IHotelStartupProvides)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES:
				setIhotelreceptionistprovides((IHotelReceptionistProvides)null);
				return;
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES:
				setIhotelstartupprovides((IHotelStartupProvides)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES:
				return ihotelreceptionistprovides != null;
			case Group4Package.HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES:
				return ihotelstartupprovides != null;
		}
		return super.eIsSet(featureID);
	}

} //HotelReceptionistTestImpl
