/**
 */
package se.chalmers.cse.mdsd1617.group4;

import org.eclipse.emf.ecore.EObject;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IHotel Admin Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getIHotelAdminProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IHotelAdminProvides extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model herpRequired="true" herpOrdered="false"
	 * @generated
	 */
	void derp(int herp);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" nbrOfBedsRequired="true" nbrOfBedsOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	void addRoomType(String name, double price, int nbrOfBeds, String description);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" nbrOfBedsRequired="true" nbrOfBedsOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	void updateRoomType(String name, double price, int nbrOfBeds, String description);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	void removeRoomType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	void addRoom(int roomID, IRoomType roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	void removeRoom(int name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	void unblockRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	void blockRoom(int roomID);
} // IHotelAdminProvides
