/**
 */
package se.chalmers.cse.mdsd1617.group4.Room.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group4.Room.Room;
import se.chalmers.cse.mdsd1617.group4.Room.RoomPackage;
import se.chalmers.cse.mdsd1617.group4.Room.RoomStatus;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomImpl#getRoomID <em>Room ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomImpl#getRoomType <em>Room Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomImpl extends MinimalEObjectImpl.Container implements Room {
	/**
	 * The default value of the '{@link #getRoomID() <em>Room ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomID()
	 * @generated
	 * @ordered
	 */
	protected static final int ROOM_ID_EDEFAULT = 0;
	/**
	 * The cached value of the '{@link #getRoomID() <em>Room ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomID()
	 * @generated
	 * @ordered
	 */
	protected int roomID = ROOM_ID_EDEFAULT;
	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final RoomStatus STATUS_EDEFAULT = RoomStatus.FREE;
	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected RoomStatus status = STATUS_EDEFAULT;
	/**
	 * The cached value of the '{@link #getRoomType() <em>Room Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomType()
	 * @generated
	 * @ordered
	 */
	protected IRoomType roomType;
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomImpl() {
		super();
	}
	protected RoomImpl(int roomID, IRoomType roomType, RoomStatus status){
		this.roomID = roomID;
		this.roomType = roomType;
		this.status = status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRoomID() {
		return roomID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomID(int newRoomID) {
		int oldRoomID = roomID;
		roomID = newRoomID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__ROOM_ID, oldRoomID, roomID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomType getRoomType() {
		if (roomType != null && roomType.eIsProxy()) {
			InternalEObject oldRoomType = (InternalEObject)roomType;
			roomType = (IRoomType)eResolveProxy(oldRoomType);
			if (roomType != oldRoomType) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, RoomPackage.ROOM__ROOM_TYPE, oldRoomType, roomType));
			}
		}
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomType basicGetRoomType() {
		return roomType;
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomType(IRoomType newRoomType) {
		IRoomType oldRoomType = roomType;
		roomType = newRoomType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__ROOM_TYPE, oldRoomType, roomType));
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(RoomStatus newStatus) {
		RoomStatus oldStatus = status;
		status = newStatus == null ? STATUS_EDEFAULT : newStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM__STATUS, oldStatus, status));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM__ROOM_ID:
				return getRoomID();
			case RoomPackage.ROOM__STATUS:
				return getStatus();
			case RoomPackage.ROOM__ROOM_TYPE:
				if (resolve) return getRoomType();
				return basicGetRoomType();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM__ROOM_ID:
				setRoomID((Integer)newValue);
				return;
			case RoomPackage.ROOM__STATUS:
				setStatus((RoomStatus)newValue);
				return;
			case RoomPackage.ROOM__ROOM_TYPE:
				setRoomType((IRoomType)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM__ROOM_ID:
				setRoomID(ROOM_ID_EDEFAULT);
				return;
			case RoomPackage.ROOM__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
			case RoomPackage.ROOM__ROOM_TYPE:
				setRoomType((IRoomType)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM__ROOM_ID:
				return roomID != ROOM_ID_EDEFAULT;
			case RoomPackage.ROOM__STATUS:
				return status != STATUS_EDEFAULT;
			case RoomPackage.ROOM__ROOM_TYPE:
				return roomType != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (roomID: ");
		result.append(roomID);
		result.append(", status: ");
		result.append(status);
		result.append(')');
		return result.toString();
	}

} //RoomImpl
