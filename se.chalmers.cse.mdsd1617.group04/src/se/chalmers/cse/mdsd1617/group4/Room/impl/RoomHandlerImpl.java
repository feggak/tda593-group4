/**
 */
package se.chalmers.cse.mdsd1617.group4.Room.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp;
import se.chalmers.cse.mdsd1617.group4.Room.RoomDTO;
import se.chalmers.cse.mdsd1617.group4.Room.RoomHandler;
import se.chalmers.cse.mdsd1617.group4.Room.RoomPackage;
import se.chalmers.cse.mdsd1617.group4.Room.RoomStatus;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomType;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeFactory;
import se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomHandlerImpl#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomHandlerImpl extends MinimalEObjectImpl.Container implements RoomHandler {
	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<IRoom> rooms;
	
	private static RoomHandlerImpl instance = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomHandlerImpl() {
		super();
		rooms = new BasicEList<IRoom>();
	}
	
	public static synchronized RoomHandler getInstance(){
		if(instance == null){
			instance = new RoomHandlerImpl();
		}
		
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<IRoom> getRooms() {
		if (rooms == null) {
			rooms = new EObjectResolvingEList<IRoom>(IRoom.class, this, RoomPackage.ROOM_HANDLER__ROOMS);
		}
		return rooms;
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IRoom getRoomByID(int roomID){
		for (IRoom r : rooms){
			if (r.getRoomID() == roomID) {
				return r;
			}
		}
		return null;
	}
	
	public void addRoom(int roomID, IRoomType roomType) {
		if (!rooms.contains(getRoomByID(roomID))){
			rooms.add(new RoomImpl(roomID, roomType, RoomStatus.FREE));
		}
		//some Exception?
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeRoom(int roomID) {
		IRoom r = getRoomByID(roomID);
		if (r != null) {
			rooms.remove(r);
		}
		//some Exception?
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unblockRoom(int roomID) {
		IRoom r = getRoomByID(roomID);
		if (r.getStatus() == RoomStatus.BLOCKED){
			r.setStatus(RoomStatus.FREE);
		}
		//some Exception?
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockRoom(int roomID) {
		IRoom r = getRoomByID(roomID);
		if (r.getStatus() == RoomStatus.FREE){
			r.setStatus(RoomStatus.BLOCKED);
		}
		//some Exception?
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void changeRoomType(int roomID, IRoomType roomType) {
		IRoom r = getRoomByID(roomID);
		if (r != null) {
			r.setRoomType(roomType);
		}
		//some Exception?
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int checkInRoom(IRoomType roomType) {
		for (IRoom r : rooms){
			if (r.getRoomType().equals(roomType) && r.getStatus().equals(RoomStatus.FREE)) {
				r.setStatus(RoomStatus.OCCUPIED);
				return r.getRoomID();
			}
		}
		//some Exception?
		return -1;
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<IRoom> getAllRooms() {
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkInPsysicalRoom(int roomID) {
		for (IRoom room : rooms) {
			if (room.getRoomID() == roomID && room.getStatus() == RoomStatus.FREE) {
				room.setStatus(RoomStatus.OCCUPIED);
				return true;
			}
		}
		return false;
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean checkOutPsysicalRoom(int roomID) {
		for (IRoom room : rooms) {
			if (room.getRoomID() == roomID && room.getStatus() == RoomStatus.OCCUPIED) {
				room.setStatus(RoomStatus.FREE);
				return true;
			}
		}
		return false;
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IRoom getRoomByNr(int roomNr) {
		return getRoomByID(roomNr);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomDTO> getAllRoomDTOs() {
		
		EList<RoomDTO> out = new BasicEList<RoomDTO>();
		
		for (int i = 0; i < rooms.size(); i++) {
			out.add(new RoomDTOImpl(rooms.get(i)));
		}
		
		return out;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startUpRooms(int nbrOfRooms) {
		
		rooms = new BasicEList<IRoom>();
		
		RoomTypeFactory rf = RoomTypeFactoryImpl.init();
		RoomType rt = rf.createRoomType("DEFAULT", 100.0, 2, "This is a default startup room");
		
		for (int i = 0; i < nbrOfRooms; i++) {
			rooms.add(new RoomImpl(i, rt, RoomStatus.FREE));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_HANDLER__ROOMS:
				return getRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_HANDLER__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends IRoom>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_HANDLER__ROOMS:
				getRooms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_HANDLER__ROOMS:
				return rooms != null && !rooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IRoomBookingHandler.class) {
			switch (baseOperationID) {
				case RoomPackage.IROOM_BOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE: return RoomPackage.ROOM_HANDLER___CHECK_IN_ROOM__IROOMTYPE;
				case RoomPackage.IROOM_BOOKING_HANDLER___GET_ALL_ROOMS: return RoomPackage.ROOM_HANDLER___GET_ALL_ROOMS;
				case RoomPackage.IROOM_BOOKING_HANDLER___CHECK_IN_PSYSICAL_ROOM__INT: return RoomPackage.ROOM_HANDLER___CHECK_IN_PSYSICAL_ROOM__INT;
				case RoomPackage.IROOM_BOOKING_HANDLER___CHECK_OUT_PSYSICAL_ROOM__INT: return RoomPackage.ROOM_HANDLER___CHECK_OUT_PSYSICAL_ROOM__INT;
				case RoomPackage.IROOM_BOOKING_HANDLER___GET_ROOM_BY_NR__INT: return RoomPackage.ROOM_HANDLER___GET_ROOM_BY_NR__INT;
				case RoomPackage.IROOM_BOOKING_HANDLER___GET_ALL_ROOM_DT_OS: return RoomPackage.ROOM_HANDLER___GET_ALL_ROOM_DT_OS;
				default: return -1;
			}
		}
		if (baseClass == IRoomStartUp.class) {
			switch (baseOperationID) {
				case RoomPackage.IROOM_START_UP___START_UP_ROOMS__INT: return RoomPackage.ROOM_HANDLER___START_UP_ROOMS__INT;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomPackage.ROOM_HANDLER___ADD_ROOM__INT_IROOMTYPE:
				addRoom((Integer)arguments.get(0), (IRoomType)arguments.get(1));
				return null;
			case RoomPackage.ROOM_HANDLER___REMOVE_ROOM__INT:
				removeRoom((Integer)arguments.get(0));
				return null;
			case RoomPackage.ROOM_HANDLER___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case RoomPackage.ROOM_HANDLER___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case RoomPackage.ROOM_HANDLER___CHANGE_ROOM_TYPE__INT_IROOMTYPE:
				changeRoomType((Integer)arguments.get(0), (IRoomType)arguments.get(1));
				return null;
			case RoomPackage.ROOM_HANDLER___GET_ROOM_BY_ID__INT:
				return getRoomByID((Integer)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___CHECK_IN_ROOM__IROOMTYPE:
				return checkInRoom((IRoomType)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___GET_ALL_ROOMS:
				return getAllRooms();
			case RoomPackage.ROOM_HANDLER___CHECK_IN_PSYSICAL_ROOM__INT:
				return checkInPsysicalRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___CHECK_OUT_PSYSICAL_ROOM__INT:
				return checkOutPsysicalRoom((Integer)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___GET_ROOM_BY_NR__INT:
				return getRoomByNr((Integer)arguments.get(0));
			case RoomPackage.ROOM_HANDLER___GET_ALL_ROOM_DT_OS:
				return getAllRoomDTOs();
			case RoomPackage.ROOM_HANDLER___START_UP_ROOMS__INT:
				startUpRooms((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomHandlerImpl
