/**
 */
package se.chalmers.cse.mdsd1617.group4.Room;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomFactory
 * @model kind="package"
 * @generated
 */
public interface RoomPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "Room";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group4/Room.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group4.Room";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomPackage eINSTANCE = se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomHandlerImpl <em>Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomHandlerImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getRoomHandler()
	 * @generated
	 */
	int ROOM_HANDLER = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler <em>IRoom Booking Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getIRoomBookingHandler()
	 * @generated
	 */
	int IROOM_BOOKING_HANDLER = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler <em>IRoom Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getIRoomHandler()
	 * @generated
	 */
	int IROOM_HANDLER = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp <em>IRoom Start Up</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getIRoomStartUp()
	 * @generated
	 */
	int IROOM_START_UP = 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoom <em>IRoom</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoom
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getIRoom()
	 * @generated
	 */
	int IROOM = 6;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 0;

	/**
	 * The number of structural features of the '<em>IRoom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Room ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_ROOM_ID = 0;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_ROOM_TYPE = 1;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___GET_STATUS = 2;

	/**
	 * The operation id for the '<em>Set Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___SET_STATUS__ROOMSTATUS = 3;

	/**
	 * The operation id for the '<em>Set Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM___SET_ROOM_TYPE__IROOMTYPE = 4;

	/**
	 * The number of operations of the '<em>IRoom</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_OPERATION_COUNT = 5;

	/**
	 * The feature id for the '<em><b>Room ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_ID = IROOM_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__STATUS = IROOM_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_TYPE = IROOM_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = IROOM_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Room ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_ROOM_ID = IROOM___GET_ROOM_ID;

	/**
	 * The operation id for the '<em>Get Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_ROOM_TYPE = IROOM___GET_ROOM_TYPE;

	/**
	 * The operation id for the '<em>Get Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___GET_STATUS = IROOM___GET_STATUS;

	/**
	 * The operation id for the '<em>Set Status</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___SET_STATUS__ROOMSTATUS = IROOM___SET_STATUS__ROOMSTATUS;

	/**
	 * The operation id for the '<em>Set Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___SET_ROOM_TYPE__IROOMTYPE = IROOM___SET_ROOM_TYPE__IROOMTYPE;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = IROOM_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IRoom Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HANDLER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HANDLER___ADD_ROOM__INT_IROOMTYPE = 0;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HANDLER___REMOVE_ROOM__INT = 1;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HANDLER___UNBLOCK_ROOM__INT = 2;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HANDLER___BLOCK_ROOM__INT = 3;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HANDLER___CHANGE_ROOM_TYPE__INT_IROOMTYPE = 4;

	/**
	 * The operation id for the '<em>Get Room By ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HANDLER___GET_ROOM_BY_ID__INT = 5;

	/**
	 * The number of operations of the '<em>IRoom Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_HANDLER_OPERATION_COUNT = 6;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER__ROOMS = IROOM_HANDLER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER_FEATURE_COUNT = IROOM_HANDLER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___ADD_ROOM__INT_IROOMTYPE = IROOM_HANDLER___ADD_ROOM__INT_IROOMTYPE;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___REMOVE_ROOM__INT = IROOM_HANDLER___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___UNBLOCK_ROOM__INT = IROOM_HANDLER___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___BLOCK_ROOM__INT = IROOM_HANDLER___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Change Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___CHANGE_ROOM_TYPE__INT_IROOMTYPE = IROOM_HANDLER___CHANGE_ROOM_TYPE__INT_IROOMTYPE;

	/**
	 * The operation id for the '<em>Get Room By ID</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_ROOM_BY_ID__INT = IROOM_HANDLER___GET_ROOM_BY_ID__INT;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___CHECK_IN_ROOM__IROOMTYPE = IROOM_HANDLER_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Get All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_ALL_ROOMS = IROOM_HANDLER_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Check In Psysical Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___CHECK_IN_PSYSICAL_ROOM__INT = IROOM_HANDLER_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Check Out Psysical Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___CHECK_OUT_PSYSICAL_ROOM__INT = IROOM_HANDLER_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Get Room By Nr</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_ROOM_BY_NR__INT = IROOM_HANDLER_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Get All Room DT Os</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___GET_ALL_ROOM_DT_OS = IROOM_HANDLER_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Start Up Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER___START_UP_ROOMS__INT = IROOM_HANDLER_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_HANDLER_OPERATION_COUNT = IROOM_HANDLER_OPERATION_COUNT + 7;

	/**
	 * The number of structural features of the '<em>IRoom Booking Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_HANDLER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE = 0;

	/**
	 * The operation id for the '<em>Get All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_HANDLER___GET_ALL_ROOMS = 1;

	/**
	 * The operation id for the '<em>Check In Psysical Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_HANDLER___CHECK_IN_PSYSICAL_ROOM__INT = 2;

	/**
	 * The operation id for the '<em>Check Out Psysical Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_HANDLER___CHECK_OUT_PSYSICAL_ROOM__INT = 3;

	/**
	 * The operation id for the '<em>Get Room By Nr</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_HANDLER___GET_ROOM_BY_NR__INT = 4;

	/**
	 * The operation id for the '<em>Get All Room DT Os</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_HANDLER___GET_ALL_ROOM_DT_OS = 5;

	/**
	 * The number of operations of the '<em>IRoom Booking Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_BOOKING_HANDLER_OPERATION_COUNT = 6;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl <em>DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getRoomDTO()
	 * @generated
	 */
	int ROOM_DTO = 4;

	/**
	 * The feature id for the '<em><b>Nr Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_DTO__NR_OF_BEDS = 0;

	/**
	 * The feature id for the '<em><b>Room ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_DTO__ROOM_ID = 1;

	/**
	 * The feature id for the '<em><b>Room Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_DTO__ROOM_STATUS = 2;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_DTO__PRICE = 3;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_DTO__DESCRIPTION = 4;

	/**
	 * The feature id for the '<em><b>Room Type Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_DTO__ROOM_TYPE_NAME = 5;

	/**
	 * The number of structural features of the '<em>DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_DTO_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_DTO_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>IRoom Start Up</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_START_UP_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Start Up Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_START_UP___START_UP_ROOMS__INT = 0;

	/**
	 * The number of operations of the '<em>IRoom Start Up</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_START_UP_OPERATION_COUNT = 1;


	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomStatus <em>Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomStatus
	 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getRoomStatus()
	 * @generated
	 */
	int ROOM_STATUS = 7;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomHandler
	 * @generated
	 */
	EClass getRoomHandler();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomHandler#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomHandler#getRooms()
	 * @see #getRoomHandler()
	 * @generated
	 */
	EReference getRoomHandler_Rooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler <em>IRoom Booking Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Booking Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler
	 * @generated
	 */
	EClass getIRoomBookingHandler();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#checkInRoom(se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#checkInRoom(se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType)
	 * @generated
	 */
	EOperation getIRoomBookingHandler__CheckInRoom__IRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#getAllRooms() <em>Get All Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#getAllRooms()
	 * @generated
	 */
	EOperation getIRoomBookingHandler__GetAllRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#checkInPsysicalRoom(int) <em>Check In Psysical Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Psysical Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#checkInPsysicalRoom(int)
	 * @generated
	 */
	EOperation getIRoomBookingHandler__CheckInPsysicalRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#checkOutPsysicalRoom(int) <em>Check Out Psysical Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Out Psysical Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#checkOutPsysicalRoom(int)
	 * @generated
	 */
	EOperation getIRoomBookingHandler__CheckOutPsysicalRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#getRoomByNr(int) <em>Get Room By Nr</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room By Nr</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#getRoomByNr(int)
	 * @generated
	 */
	EOperation getIRoomBookingHandler__GetRoomByNr__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#getAllRoomDTOs() <em>Get All Room DT Os</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Room DT Os</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler#getAllRoomDTOs()
	 * @generated
	 */
	EOperation getIRoomBookingHandler__GetAllRoomDTOs();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO <em>DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomDTO
	 * @generated
	 */
	EClass getRoomDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getNrOfBeds <em>Nr Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Nr Of Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getNrOfBeds()
	 * @see #getRoomDTO()
	 * @generated
	 */
	EAttribute getRoomDTO_NrOfBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomID <em>Room ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room ID</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomID()
	 * @see #getRoomDTO()
	 * @generated
	 */
	EAttribute getRoomDTO_RoomID();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomStatus <em>Room Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomStatus()
	 * @see #getRoomDTO()
	 * @generated
	 */
	EAttribute getRoomDTO_RoomStatus();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getPrice()
	 * @see #getRoomDTO()
	 * @generated
	 */
	EAttribute getRoomDTO_Price();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getDescription()
	 * @see #getRoomDTO()
	 * @generated
	 */
	EAttribute getRoomDTO_Description();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomTypeName <em>Room Type Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Type Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomTypeName()
	 * @see #getRoomDTO()
	 * @generated
	 */
	EAttribute getRoomDTO_RoomTypeName();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler <em>IRoom Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler
	 * @generated
	 */
	EClass getIRoomHandler();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#addRoom(int, se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#addRoom(int, se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType)
	 * @generated
	 */
	EOperation getIRoomHandler__AddRoom__int_IRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#removeRoom(int)
	 * @generated
	 */
	EOperation getIRoomHandler__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#unblockRoom(int)
	 * @generated
	 */
	EOperation getIRoomHandler__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#blockRoom(int)
	 * @generated
	 */
	EOperation getIRoomHandler__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#changeRoomType(int, se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType) <em>Change Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#changeRoomType(int, se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType)
	 * @generated
	 */
	EOperation getIRoomHandler__ChangeRoomType__int_IRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#getRoomByID(int) <em>Get Room By ID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room By ID</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler#getRoomByID(int)
	 * @generated
	 */
	EOperation getIRoomHandler__GetRoomByID__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp <em>IRoom Start Up</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Start Up</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp
	 * @generated
	 */
	EClass getIRoomStartUp();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp#startUpRooms(int) <em>Start Up Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Start Up Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp#startUpRooms(int)
	 * @generated
	 */
	EOperation getIRoomStartUp__StartUpRooms__int();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomStatus
	 * @generated
	 */
	EEnum getRoomStatus();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoom <em>IRoom</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoom
	 * @generated
	 */
	EClass getIRoom();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoom#getRoomID() <em>Get Room ID</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room ID</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoom#getRoomID()
	 * @generated
	 */
	EOperation getIRoom__GetRoomID();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoom#getRoomType() <em>Get Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoom#getRoomType()
	 * @generated
	 */
	EOperation getIRoom__GetRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoom#getStatus() <em>Get Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Status</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoom#getStatus()
	 * @generated
	 */
	EOperation getIRoom__GetStatus();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoom#setStatus(se.chalmers.cse.mdsd1617.group4.Room.RoomStatus) <em>Set Status</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Status</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoom#setStatus(se.chalmers.cse.mdsd1617.group4.Room.RoomStatus)
	 * @generated
	 */
	EOperation getIRoom__SetStatus__RoomStatus();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoom#setRoomType(se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType) <em>Set Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoom#setRoomType(se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType)
	 * @generated
	 */
	EOperation getIRoom__SetRoomType__IRoomType();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.Room.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Room.Room#getRoomID <em>Room ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room ID</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.Room#getRoomID()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomID();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.Room.Room#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.Room#getStatus()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Status();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.Room.Room#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.Room#getRoomType()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_RoomType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomFactory getRoomFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomHandlerImpl <em>Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomHandlerImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getRoomHandler()
		 * @generated
		 */
		EClass ROOM_HANDLER = eINSTANCE.getRoomHandler();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_HANDLER__ROOMS = eINSTANCE.getRoomHandler_Rooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler <em>IRoom Booking Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getIRoomBookingHandler()
		 * @generated
		 */
		EClass IROOM_BOOKING_HANDLER = eINSTANCE.getIRoomBookingHandler();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE = eINSTANCE.getIRoomBookingHandler__CheckInRoom__IRoomType();

		/**
		 * The meta object literal for the '<em><b>Get All Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_HANDLER___GET_ALL_ROOMS = eINSTANCE.getIRoomBookingHandler__GetAllRooms();

		/**
		 * The meta object literal for the '<em><b>Check In Psysical Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_HANDLER___CHECK_IN_PSYSICAL_ROOM__INT = eINSTANCE.getIRoomBookingHandler__CheckInPsysicalRoom__int();

		/**
		 * The meta object literal for the '<em><b>Check Out Psysical Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_HANDLER___CHECK_OUT_PSYSICAL_ROOM__INT = eINSTANCE.getIRoomBookingHandler__CheckOutPsysicalRoom__int();

		/**
		 * The meta object literal for the '<em><b>Get Room By Nr</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_HANDLER___GET_ROOM_BY_NR__INT = eINSTANCE.getIRoomBookingHandler__GetRoomByNr__int();

		/**
		 * The meta object literal for the '<em><b>Get All Room DT Os</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_BOOKING_HANDLER___GET_ALL_ROOM_DT_OS = eINSTANCE.getIRoomBookingHandler__GetAllRoomDTOs();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl <em>DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getRoomDTO()
		 * @generated
		 */
		EClass ROOM_DTO = eINSTANCE.getRoomDTO();

		/**
		 * The meta object literal for the '<em><b>Nr Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_DTO__NR_OF_BEDS = eINSTANCE.getRoomDTO_NrOfBeds();

		/**
		 * The meta object literal for the '<em><b>Room ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_DTO__ROOM_ID = eINSTANCE.getRoomDTO_RoomID();

		/**
		 * The meta object literal for the '<em><b>Room Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_DTO__ROOM_STATUS = eINSTANCE.getRoomDTO_RoomStatus();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_DTO__PRICE = eINSTANCE.getRoomDTO_Price();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_DTO__DESCRIPTION = eINSTANCE.getRoomDTO_Description();

		/**
		 * The meta object literal for the '<em><b>Room Type Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_DTO__ROOM_TYPE_NAME = eINSTANCE.getRoomDTO_RoomTypeName();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler <em>IRoom Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getIRoomHandler()
		 * @generated
		 */
		EClass IROOM_HANDLER = eINSTANCE.getIRoomHandler();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_HANDLER___ADD_ROOM__INT_IROOMTYPE = eINSTANCE.getIRoomHandler__AddRoom__int_IRoomType();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_HANDLER___REMOVE_ROOM__INT = eINSTANCE.getIRoomHandler__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_HANDLER___UNBLOCK_ROOM__INT = eINSTANCE.getIRoomHandler__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_HANDLER___BLOCK_ROOM__INT = eINSTANCE.getIRoomHandler__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Change Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_HANDLER___CHANGE_ROOM_TYPE__INT_IROOMTYPE = eINSTANCE.getIRoomHandler__ChangeRoomType__int_IRoomType();

		/**
		 * The meta object literal for the '<em><b>Get Room By ID</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_HANDLER___GET_ROOM_BY_ID__INT = eINSTANCE.getIRoomHandler__GetRoomByID__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp <em>IRoom Start Up</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getIRoomStartUp()
		 * @generated
		 */
		EClass IROOM_START_UP = eINSTANCE.getIRoomStartUp();

		/**
		 * The meta object literal for the '<em><b>Start Up Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_START_UP___START_UP_ROOMS__INT = eINSTANCE.getIRoomStartUp__StartUpRooms__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomStatus <em>Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomStatus
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getRoomStatus()
		 * @generated
		 */
		EEnum ROOM_STATUS = eINSTANCE.getRoomStatus();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Room.IRoom <em>IRoom</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Room.IRoom
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getIRoom()
		 * @generated
		 */
		EClass IROOM = eINSTANCE.getIRoom();

		/**
		 * The meta object literal for the '<em><b>Get Room ID</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_ROOM_ID = eINSTANCE.getIRoom__GetRoomID();

		/**
		 * The meta object literal for the '<em><b>Get Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_ROOM_TYPE = eINSTANCE.getIRoom__GetRoomType();

		/**
		 * The meta object literal for the '<em><b>Get Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___GET_STATUS = eINSTANCE.getIRoom__GetStatus();

		/**
		 * The meta object literal for the '<em><b>Set Status</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___SET_STATUS__ROOMSTATUS = eINSTANCE.getIRoom__SetStatus__RoomStatus();

		/**
		 * The meta object literal for the '<em><b>Set Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM___SET_ROOM_TYPE__IROOMTYPE = eINSTANCE.getIRoom__SetRoomType__IRoomType();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomImpl
		 * @see se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Room ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_ID = eINSTANCE.getRoom_RoomID();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__STATUS = eINSTANCE.getRoom_Status();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ROOM_TYPE = eINSTANCE.getRoom_RoomType();

	}

} //RoomPackage
