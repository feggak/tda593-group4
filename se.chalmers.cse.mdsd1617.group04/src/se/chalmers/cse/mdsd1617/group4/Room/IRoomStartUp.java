/**
 */
package se.chalmers.cse.mdsd1617.group4.Room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Start Up</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getIRoomStartUp()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomStartUp extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nbrOfRoomsRequired="true" nbrOfRoomsOrdered="false"
	 * @generated
	 */
	void startUpRooms(int nbrOfRooms);
} // IRoomStartUp
