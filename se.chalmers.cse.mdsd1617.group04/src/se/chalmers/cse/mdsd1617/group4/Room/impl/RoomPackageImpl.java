/**
 */
package se.chalmers.cse.mdsd1617.group4.Room.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage;

import se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl;

import se.chalmers.cse.mdsd1617.group4.Group4Package;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl.HotelStartUpPackageImpl;

import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomBookingHandler;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler;
import se.chalmers.cse.mdsd1617.group4.Room.IRoomStartUp;
import se.chalmers.cse.mdsd1617.group4.Room.Room;
import se.chalmers.cse.mdsd1617.group4.Room.RoomDTO;
import se.chalmers.cse.mdsd1617.group4.Room.RoomFactory;
import se.chalmers.cse.mdsd1617.group4.Room.RoomHandler;
import se.chalmers.cse.mdsd1617.group4.Room.RoomPackage;

import se.chalmers.cse.mdsd1617.group4.Room.RoomStatus;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage;

import se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl;

import se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomPackageImpl extends EPackageImpl implements RoomPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomBookingHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomDTOEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomStartUpEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum roomStatusEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RoomPackageImpl() {
		super(eNS_URI, RoomFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RoomPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RoomPackage init() {
		if (isInited) return (RoomPackage)EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI);

		// Obtain or create and register package
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RoomPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group4PackageImpl theGroup4Package = (Group4PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group4Package.eNS_URI) instanceof Group4PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group4Package.eNS_URI) : Group4Package.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI) : RoomTypePackage.eINSTANCE);
		HotelStartUpPackageImpl theHotelStartUpPackage = (HotelStartUpPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelStartUpPackage.eNS_URI) instanceof HotelStartUpPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelStartUpPackage.eNS_URI) : HotelStartUpPackage.eINSTANCE);

		// Create package meta-data objects
		theRoomPackage.createPackageContents();
		theGroup4Package.createPackageContents();
		theBookingPackage.createPackageContents();
		theRoomTypePackage.createPackageContents();
		theHotelStartUpPackage.createPackageContents();

		// Initialize created meta-data
		theRoomPackage.initializePackageContents();
		theGroup4Package.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theRoomTypePackage.initializePackageContents();
		theHotelStartUpPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoomPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RoomPackage.eNS_URI, theRoomPackage);
		return theRoomPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomHandler() {
		return roomHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomHandler_Rooms() {
		return (EReference)roomHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomBookingHandler() {
		return iRoomBookingHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingHandler__CheckInRoom__IRoomType() {
		return iRoomBookingHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingHandler__GetAllRooms() {
		return iRoomBookingHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingHandler__CheckInPsysicalRoom__int() {
		return iRoomBookingHandlerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingHandler__CheckOutPsysicalRoom__int() {
		return iRoomBookingHandlerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingHandler__GetRoomByNr__int() {
		return iRoomBookingHandlerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomBookingHandler__GetAllRoomDTOs() {
		return iRoomBookingHandlerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomDTO() {
		return roomDTOEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomDTO_NrOfBeds() {
		return (EAttribute)roomDTOEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomDTO_RoomID() {
		return (EAttribute)roomDTOEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomDTO_RoomStatus() {
		return (EAttribute)roomDTOEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomDTO_Price() {
		return (EAttribute)roomDTOEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomDTO_Description() {
		return (EAttribute)roomDTOEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomDTO_RoomTypeName() {
		return (EAttribute)roomDTOEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomHandler() {
		return iRoomHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomHandler__AddRoom__int_IRoomType() {
		return iRoomHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomHandler__RemoveRoom__int() {
		return iRoomHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomHandler__UnblockRoom__int() {
		return iRoomHandlerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomHandler__BlockRoom__int() {
		return iRoomHandlerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomHandler__ChangeRoomType__int_IRoomType() {
		return iRoomHandlerEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomHandler__GetRoomByID__int() {
		return iRoomHandlerEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomStartUp() {
		return iRoomStartUpEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomStartUp__StartUpRooms__int() {
		return iRoomStartUpEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getRoomStatus() {
		return roomStatusEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoom() {
		return iRoomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetRoomID() {
		return iRoomEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetRoomType() {
		return iRoomEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__GetStatus() {
		return iRoomEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__SetStatus__RoomStatus() {
		return iRoomEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoom__SetRoomType__IRoomType() {
		return iRoomEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoom() {
		return roomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_RoomID() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_Status() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_RoomType() {
		return (EReference)roomEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomFactory getRoomFactory() {
		return (RoomFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roomEClass = createEClass(ROOM);
		createEAttribute(roomEClass, ROOM__ROOM_ID);
		createEAttribute(roomEClass, ROOM__STATUS);
		createEReference(roomEClass, ROOM__ROOM_TYPE);

		roomHandlerEClass = createEClass(ROOM_HANDLER);
		createEReference(roomHandlerEClass, ROOM_HANDLER__ROOMS);

		iRoomHandlerEClass = createEClass(IROOM_HANDLER);
		createEOperation(iRoomHandlerEClass, IROOM_HANDLER___ADD_ROOM__INT_IROOMTYPE);
		createEOperation(iRoomHandlerEClass, IROOM_HANDLER___REMOVE_ROOM__INT);
		createEOperation(iRoomHandlerEClass, IROOM_HANDLER___UNBLOCK_ROOM__INT);
		createEOperation(iRoomHandlerEClass, IROOM_HANDLER___BLOCK_ROOM__INT);
		createEOperation(iRoomHandlerEClass, IROOM_HANDLER___CHANGE_ROOM_TYPE__INT_IROOMTYPE);
		createEOperation(iRoomHandlerEClass, IROOM_HANDLER___GET_ROOM_BY_ID__INT);

		iRoomBookingHandlerEClass = createEClass(IROOM_BOOKING_HANDLER);
		createEOperation(iRoomBookingHandlerEClass, IROOM_BOOKING_HANDLER___CHECK_IN_ROOM__IROOMTYPE);
		createEOperation(iRoomBookingHandlerEClass, IROOM_BOOKING_HANDLER___GET_ALL_ROOMS);
		createEOperation(iRoomBookingHandlerEClass, IROOM_BOOKING_HANDLER___CHECK_IN_PSYSICAL_ROOM__INT);
		createEOperation(iRoomBookingHandlerEClass, IROOM_BOOKING_HANDLER___CHECK_OUT_PSYSICAL_ROOM__INT);
		createEOperation(iRoomBookingHandlerEClass, IROOM_BOOKING_HANDLER___GET_ROOM_BY_NR__INT);
		createEOperation(iRoomBookingHandlerEClass, IROOM_BOOKING_HANDLER___GET_ALL_ROOM_DT_OS);

		roomDTOEClass = createEClass(ROOM_DTO);
		createEAttribute(roomDTOEClass, ROOM_DTO__NR_OF_BEDS);
		createEAttribute(roomDTOEClass, ROOM_DTO__ROOM_ID);
		createEAttribute(roomDTOEClass, ROOM_DTO__ROOM_STATUS);
		createEAttribute(roomDTOEClass, ROOM_DTO__PRICE);
		createEAttribute(roomDTOEClass, ROOM_DTO__DESCRIPTION);
		createEAttribute(roomDTOEClass, ROOM_DTO__ROOM_TYPE_NAME);

		iRoomStartUpEClass = createEClass(IROOM_START_UP);
		createEOperation(iRoomStartUpEClass, IROOM_START_UP___START_UP_ROOMS__INT);

		iRoomEClass = createEClass(IROOM);
		createEOperation(iRoomEClass, IROOM___GET_ROOM_ID);
		createEOperation(iRoomEClass, IROOM___GET_ROOM_TYPE);
		createEOperation(iRoomEClass, IROOM___GET_STATUS);
		createEOperation(iRoomEClass, IROOM___SET_STATUS__ROOMSTATUS);
		createEOperation(iRoomEClass, IROOM___SET_ROOM_TYPE__IROOMTYPE);

		// Create enums
		roomStatusEEnum = createEEnum(ROOM_STATUS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		RoomTypePackage theRoomTypePackage = (RoomTypePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		roomEClass.getESuperTypes().add(this.getIRoom());
		roomHandlerEClass.getESuperTypes().add(this.getIRoomHandler());
		roomHandlerEClass.getESuperTypes().add(this.getIRoomBookingHandler());
		roomHandlerEClass.getESuperTypes().add(this.getIRoomStartUp());

		// Initialize classes, features, and operations; add parameters
		initEClass(roomEClass, Room.class, "Room", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoom_RoomID(), ecorePackage.getEInt(), "roomID", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoom_Status(), this.getRoomStatus(), "status", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getRoom_RoomType(), theRoomTypePackage.getIRoomType(), null, "roomType", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomHandlerEClass, RoomHandler.class, "RoomHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomHandler_Rooms(), this.getIRoom(), null, "rooms", null, 0, -1, RoomHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomHandlerEClass, IRoomHandler.class, "IRoomHandler", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIRoomHandler__AddRoom__int_IRoomType(), null, "addRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomTypePackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomHandler__RemoveRoom__int(), null, "removeRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomHandler__UnblockRoom__int(), null, "unblockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomHandler__BlockRoom__int(), null, "blockRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomHandler__ChangeRoomType__int_IRoomType(), null, "changeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomTypePackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomHandler__GetRoomByID__int(), this.getIRoom(), "getRoomByID", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomBookingHandlerEClass, IRoomBookingHandler.class, "IRoomBookingHandler", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomBookingHandler__CheckInRoom__IRoomType(), ecorePackage.getEInt(), "checkInRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomTypePackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomBookingHandler__GetAllRooms(), this.getIRoom(), "getAllRooms", 0, -1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomBookingHandler__CheckInPsysicalRoom__int(), ecorePackage.getEBoolean(), "checkInPsysicalRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomBookingHandler__CheckOutPsysicalRoom__int(), ecorePackage.getEBoolean(), "checkOutPsysicalRoom", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomBookingHandler__GetRoomByNr__int(), this.getIRoom(), "getRoomByNr", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "roomNr", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomBookingHandler__GetAllRoomDTOs(), this.getRoomDTO(), "getAllRoomDTOs", 0, -1, IS_UNIQUE, !IS_ORDERED);

		initEClass(roomDTOEClass, RoomDTO.class, "RoomDTO", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoomDTO_NrOfBeds(), ecorePackage.getEInt(), "nrOfBeds", null, 1, 1, RoomDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomDTO_RoomID(), ecorePackage.getEInt(), "roomID", null, 1, 1, RoomDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomDTO_RoomStatus(), this.getRoomStatus(), "roomStatus", null, 1, 1, RoomDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomDTO_Price(), ecorePackage.getEDouble(), "price", null, 1, 1, RoomDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomDTO_Description(), ecorePackage.getEString(), "description", null, 1, 1, RoomDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomDTO_RoomTypeName(), ecorePackage.getEString(), "roomTypeName", null, 1, 1, RoomDTO.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomStartUpEClass, IRoomStartUp.class, "IRoomStartUp", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		op = initEOperation(getIRoomStartUp__StartUpRooms__int(), null, "startUpRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "nbrOfRooms", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomEClass, IRoom.class, "IRoom", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoom__GetRoomID(), ecorePackage.getEInt(), "getRoomID", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__GetRoomType(), theRoomTypePackage.getIRoomType(), "getRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoom__GetStatus(), this.getRoomStatus(), "getStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoom__SetStatus__RoomStatus(), null, "setStatus", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, this.getRoomStatus(), "status", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoom__SetRoomType__IRoomType(), null, "setRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, theRoomTypePackage.getIRoomType(), "roomType", 1, 1, IS_UNIQUE, !IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(roomStatusEEnum, RoomStatus.class, "RoomStatus");
		addEEnumLiteral(roomStatusEEnum, RoomStatus.FREE);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.OCCUPIED);
		addEEnumLiteral(roomStatusEEnum, RoomStatus.BLOCKED);
	}

} //RoomPackageImpl
