/**
 */
package se.chalmers.cse.mdsd1617.group4.Room;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.RoomHandler#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomHandler()
 * @model
 * @generated
 */
public interface RoomHandler extends IRoomHandler, IRoomBookingHandler, IRoomStartUp {
	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group4.Room.IRoom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomHandler_Rooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<IRoom> getRooms();

} // RoomHandler
