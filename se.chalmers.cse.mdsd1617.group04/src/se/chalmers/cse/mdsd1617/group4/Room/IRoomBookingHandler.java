/**
 */
package se.chalmers.cse.mdsd1617.group4.Room;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Booking Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getIRoomBookingHandler()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomBookingHandler extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomTypeRequired="true" roomTypeOrdered="false"
	 * @generated
	 */
	int checkInRoom(IRoomType roomType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<IRoom> getAllRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean checkInPsysicalRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomIDRequired="true" roomIDOrdered="false"
	 * @generated
	 */
	boolean checkOutPsysicalRoom(int roomID);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNrRequired="true" roomNrOrdered="false"
	 * @generated
	 */
	IRoom getRoomByNr(int roomNr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<RoomDTO> getAllRoomDTOs();
} // IRoomBookingHandler
