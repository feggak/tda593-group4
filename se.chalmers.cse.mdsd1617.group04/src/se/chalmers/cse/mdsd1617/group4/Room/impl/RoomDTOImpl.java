/**
 */
package se.chalmers.cse.mdsd1617.group4.Room.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group4.Room.IRoom;
import se.chalmers.cse.mdsd1617.group4.Room.RoomDTO;
import se.chalmers.cse.mdsd1617.group4.Room.RoomPackage;
import se.chalmers.cse.mdsd1617.group4.Room.RoomStatus;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>DTO</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl#getNrOfBeds <em>Nr Of Beds</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl#getRoomID <em>Room ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl#getRoomStatus <em>Room Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl#getPrice <em>Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.impl.RoomDTOImpl#getRoomTypeName <em>Room Type Name</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomDTOImpl extends MinimalEObjectImpl.Container implements RoomDTO {
	
	public RoomDTOImpl(IRoom iRoom){
		
		nrOfBeds = iRoom.getRoomType().getNumberOfBeds();
		roomID = iRoom.getRoomID();
		price = iRoom.getRoomType().getPrice();
		description = iRoom.getRoomType().getDescription();
		roomTypeName = iRoom.getRoomType().getName();
		roomStatus = iRoom.getStatus();
	}
	
	/**
	 * The default value of the '{@link #getNrOfBeds() <em>Nr Of Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNrOfBeds()
	 * @generated
	 * @ordered
	 */
	protected static final int NR_OF_BEDS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNrOfBeds() <em>Nr Of Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNrOfBeds()
	 * @generated
	 * @ordered
	 */
	protected int nrOfBeds = NR_OF_BEDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoomID() <em>Room ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomID()
	 * @generated
	 * @ordered
	 */
	protected static final int ROOM_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getRoomID() <em>Room ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomID()
	 * @generated
	 * @ordered
	 */
	protected int roomID = ROOM_ID_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoomStatus() <em>Room Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomStatus()
	 * @generated
	 * @ordered
	 */
	protected static final RoomStatus ROOM_STATUS_EDEFAULT = RoomStatus.FREE;

	/**
	 * The cached value of the '{@link #getRoomStatus() <em>Room Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomStatus()
	 * @generated
	 * @ordered
	 */
	protected RoomStatus roomStatus = ROOM_STATUS_EDEFAULT;

	/**
	 * The default value of the '{@link #getPrice() <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrice()
	 * @generated
	 * @ordered
	 */
	protected static final double PRICE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPrice() <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrice()
	 * @generated
	 * @ordered
	 */
	protected double price = PRICE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getRoomTypeName() <em>Room Type Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeName()
	 * @generated
	 * @ordered
	 */
	protected static final String ROOM_TYPE_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getRoomTypeName() <em>Room Type Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypeName()
	 * @generated
	 * @ordered
	 */
	protected String roomTypeName = ROOM_TYPE_NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomDTOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomPackage.Literals.ROOM_DTO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNrOfBeds() {
		return nrOfBeds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setNrOfBeds(int newNrOfBeds) {
		int oldNrOfBeds = nrOfBeds;
		nrOfBeds = newNrOfBeds;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_DTO__NR_OF_BEDS, oldNrOfBeds, nrOfBeds));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRoomID() {
		return roomID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomID(int newRoomID) {
		int oldRoomID = roomID;
		roomID = newRoomID;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_DTO__ROOM_ID, oldRoomID, roomID));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomStatus getRoomStatus() {
		return roomStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomStatus(RoomStatus newRoomStatus) {
		RoomStatus oldRoomStatus = roomStatus;
		roomStatus = newRoomStatus == null ? ROOM_STATUS_EDEFAULT : newRoomStatus;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_DTO__ROOM_STATUS, oldRoomStatus, roomStatus));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPrice(double newPrice) {
		double oldPrice = price;
		price = newPrice;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_DTO__PRICE, oldPrice, price));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_DTO__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRoomTypeName() {
		return roomTypeName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoomTypeName(String newRoomTypeName) {
		String oldRoomTypeName = roomTypeName;
		roomTypeName = newRoomTypeName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, RoomPackage.ROOM_DTO__ROOM_TYPE_NAME, oldRoomTypeName, roomTypeName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomPackage.ROOM_DTO__NR_OF_BEDS:
				return getNrOfBeds();
			case RoomPackage.ROOM_DTO__ROOM_ID:
				return getRoomID();
			case RoomPackage.ROOM_DTO__ROOM_STATUS:
				return getRoomStatus();
			case RoomPackage.ROOM_DTO__PRICE:
				return getPrice();
			case RoomPackage.ROOM_DTO__DESCRIPTION:
				return getDescription();
			case RoomPackage.ROOM_DTO__ROOM_TYPE_NAME:
				return getRoomTypeName();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomPackage.ROOM_DTO__NR_OF_BEDS:
				setNrOfBeds((Integer)newValue);
				return;
			case RoomPackage.ROOM_DTO__ROOM_ID:
				setRoomID((Integer)newValue);
				return;
			case RoomPackage.ROOM_DTO__ROOM_STATUS:
				setRoomStatus((RoomStatus)newValue);
				return;
			case RoomPackage.ROOM_DTO__PRICE:
				setPrice((Double)newValue);
				return;
			case RoomPackage.ROOM_DTO__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case RoomPackage.ROOM_DTO__ROOM_TYPE_NAME:
				setRoomTypeName((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_DTO__NR_OF_BEDS:
				setNrOfBeds(NR_OF_BEDS_EDEFAULT);
				return;
			case RoomPackage.ROOM_DTO__ROOM_ID:
				setRoomID(ROOM_ID_EDEFAULT);
				return;
			case RoomPackage.ROOM_DTO__ROOM_STATUS:
				setRoomStatus(ROOM_STATUS_EDEFAULT);
				return;
			case RoomPackage.ROOM_DTO__PRICE:
				setPrice(PRICE_EDEFAULT);
				return;
			case RoomPackage.ROOM_DTO__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case RoomPackage.ROOM_DTO__ROOM_TYPE_NAME:
				setRoomTypeName(ROOM_TYPE_NAME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomPackage.ROOM_DTO__NR_OF_BEDS:
				return nrOfBeds != NR_OF_BEDS_EDEFAULT;
			case RoomPackage.ROOM_DTO__ROOM_ID:
				return roomID != ROOM_ID_EDEFAULT;
			case RoomPackage.ROOM_DTO__ROOM_STATUS:
				return roomStatus != ROOM_STATUS_EDEFAULT;
			case RoomPackage.ROOM_DTO__PRICE:
				return price != PRICE_EDEFAULT;
			case RoomPackage.ROOM_DTO__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case RoomPackage.ROOM_DTO__ROOM_TYPE_NAME:
				return ROOM_TYPE_NAME_EDEFAULT == null ? roomTypeName != null : !ROOM_TYPE_NAME_EDEFAULT.equals(roomTypeName);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (nrOfBeds: ");
		result.append(nrOfBeds);
		result.append(", roomID: ");
		result.append(roomID);
		result.append(", roomStatus: ");
		result.append(roomStatus);
		result.append(", price: ");
		result.append(price);
		result.append(", description: ");
		result.append(description);
		result.append(", roomTypeName: ");
		result.append(roomTypeName);
		result.append(')');
		return result.toString();
	}

} //RoomDTOImpl
