/**
 */
package se.chalmers.cse.mdsd1617.group4.Room;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>DTO</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getNrOfBeds <em>Nr Of Beds</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomID <em>Room ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomStatus <em>Room Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getPrice <em>Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getDescription <em>Description</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomTypeName <em>Room Type Name</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomDTO()
 * @model
 * @generated
 */
public interface RoomDTO extends EObject {
	/**
	 * Returns the value of the '<em><b>Nr Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Nr Of Beds</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Nr Of Beds</em>' attribute.
	 * @see #setNrOfBeds(int)
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomDTO_NrOfBeds()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getNrOfBeds();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getNrOfBeds <em>Nr Of Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Nr Of Beds</em>' attribute.
	 * @see #getNrOfBeds()
	 * @generated
	 */
	void setNrOfBeds(int value);

	/**
	 * Returns the value of the '<em><b>Room ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room ID</em>' attribute.
	 * @see #setRoomID(int)
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomDTO_RoomID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getRoomID();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomID <em>Room ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room ID</em>' attribute.
	 * @see #getRoomID()
	 * @generated
	 */
	void setRoomID(int value);

	/**
	 * Returns the value of the '<em><b>Room Status</b></em>' attribute.
	 * The literals are from the enumeration {@link se.chalmers.cse.mdsd1617.group4.Room.RoomStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomStatus
	 * @see #setRoomStatus(RoomStatus)
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomDTO_RoomStatus()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	RoomStatus getRoomStatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomStatus <em>Room Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomStatus
	 * @see #getRoomStatus()
	 * @generated
	 */
	void setRoomStatus(RoomStatus value);

	/**
	 * Returns the value of the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Price</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Price</em>' attribute.
	 * @see #setPrice(double)
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomDTO_Price()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getPrice();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getPrice <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Price</em>' attribute.
	 * @see #getPrice()
	 * @generated
	 */
	void setPrice(double value);

	/**
	 * Returns the value of the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description</em>' attribute.
	 * @see #setDescription(String)
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomDTO_Description()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * Returns the value of the '<em><b>Room Type Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type Name</em>' attribute.
	 * @see #setRoomTypeName(String)
	 * @see se.chalmers.cse.mdsd1617.group4.Room.RoomPackage#getRoomDTO_RoomTypeName()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	String getRoomTypeName();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.Room.RoomDTO#getRoomTypeName <em>Room Type Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type Name</em>' attribute.
	 * @see #getRoomTypeName()
	 * @generated
	 */
	void setRoomTypeName(String value);

} // RoomDTO
