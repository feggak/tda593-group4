/**
 */
package se.chalmers.cse.mdsd1617.group4.RoomType.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import se.chalmers.cse.mdsd1617.group4.Booking.BookingPackage;

import se.chalmers.cse.mdsd1617.group4.Booking.impl.BookingPackageImpl;

import se.chalmers.cse.mdsd1617.group4.Group4Package;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.HotelStartUpPackage;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.impl.HotelStartUpPackageImpl;

import se.chalmers.cse.mdsd1617.group4.Room.RoomPackage;

import se.chalmers.cse.mdsd1617.group4.Room.impl.RoomPackageImpl;

import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomType;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeFactory;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeHandler;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage;

import se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RoomTypePackageImpl extends EPackageImpl implements RoomTypePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeHandlerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeStartUpEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass iRoomTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomTypeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private RoomTypePackageImpl() {
		super(eNS_URI, RoomTypeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link RoomTypePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static RoomTypePackage init() {
		if (isInited) return (RoomTypePackage)EPackage.Registry.INSTANCE.getEPackage(RoomTypePackage.eNS_URI);

		// Obtain or create and register package
		RoomTypePackageImpl theRoomTypePackage = (RoomTypePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof RoomTypePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new RoomTypePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		Group4PackageImpl theGroup4Package = (Group4PackageImpl)(EPackage.Registry.INSTANCE.getEPackage(Group4Package.eNS_URI) instanceof Group4PackageImpl ? EPackage.Registry.INSTANCE.getEPackage(Group4Package.eNS_URI) : Group4Package.eINSTANCE);
		BookingPackageImpl theBookingPackage = (BookingPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) instanceof BookingPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(BookingPackage.eNS_URI) : BookingPackage.eINSTANCE);
		RoomPackageImpl theRoomPackage = (RoomPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) instanceof RoomPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RoomPackage.eNS_URI) : RoomPackage.eINSTANCE);
		HotelStartUpPackageImpl theHotelStartUpPackage = (HotelStartUpPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(HotelStartUpPackage.eNS_URI) instanceof HotelStartUpPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(HotelStartUpPackage.eNS_URI) : HotelStartUpPackage.eINSTANCE);

		// Create package meta-data objects
		theRoomTypePackage.createPackageContents();
		theGroup4Package.createPackageContents();
		theBookingPackage.createPackageContents();
		theRoomPackage.createPackageContents();
		theHotelStartUpPackage.createPackageContents();

		// Initialize created meta-data
		theRoomTypePackage.initializePackageContents();
		theGroup4Package.initializePackageContents();
		theBookingPackage.initializePackageContents();
		theRoomPackage.initializePackageContents();
		theHotelStartUpPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theRoomTypePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(RoomTypePackage.eNS_URI, theRoomTypePackage);
		return theRoomTypePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomTypeHandler() {
		return roomTypeHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoomTypeHandler_RoomTypes() {
		return (EReference)roomTypeHandlerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTypeHandler() {
		return iRoomTypeHandlerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeHandler__AddRoomType__String_double_int_String() {
		return iRoomTypeHandlerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeHandler__UpdateRoomType__String_double_int_String() {
		return iRoomTypeHandlerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeHandler__RemoveRoomType__String() {
		return iRoomTypeHandlerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeHandler__GetRoomTypeByName__String() {
		return iRoomTypeHandlerEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomTypeStartUp() {
		return iRoomTypeStartUpEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomTypeStartUp__ResetRoomTypes() {
		return iRoomTypeStartUpEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIRoomType() {
		return iRoomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetName() {
		return iRoomTypeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetPrice() {
		return iRoomTypeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetDescription() {
		return iRoomTypeEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__GetNumberOfBeds() {
		return iRoomTypeEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__SetName__String() {
		return iRoomTypeEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__SetPrice__double() {
		return iRoomTypeEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__SetDescription__String() {
		return iRoomTypeEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getIRoomType__SetNumberOfBeds__int() {
		return iRoomTypeEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoomType() {
		return roomTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Name() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Price() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_Description() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoomType_NumberOfBeds() {
		return (EAttribute)roomTypeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeFactory getRoomTypeFactory() {
		return (RoomTypeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		roomTypeHandlerEClass = createEClass(ROOM_TYPE_HANDLER);
		createEReference(roomTypeHandlerEClass, ROOM_TYPE_HANDLER__ROOM_TYPES);

		roomTypeEClass = createEClass(ROOM_TYPE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__NAME);
		createEAttribute(roomTypeEClass, ROOM_TYPE__PRICE);
		createEAttribute(roomTypeEClass, ROOM_TYPE__DESCRIPTION);
		createEAttribute(roomTypeEClass, ROOM_TYPE__NUMBER_OF_BEDS);

		iRoomTypeHandlerEClass = createEClass(IROOM_TYPE_HANDLER);
		createEOperation(iRoomTypeHandlerEClass, IROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING);
		createEOperation(iRoomTypeHandlerEClass, IROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING);
		createEOperation(iRoomTypeHandlerEClass, IROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING);
		createEOperation(iRoomTypeHandlerEClass, IROOM_TYPE_HANDLER___GET_ROOM_TYPE_BY_NAME__STRING);

		iRoomTypeStartUpEClass = createEClass(IROOM_TYPE_START_UP);
		createEOperation(iRoomTypeStartUpEClass, IROOM_TYPE_START_UP___RESET_ROOM_TYPES);

		iRoomTypeEClass = createEClass(IROOM_TYPE);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_NAME);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_PRICE);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_DESCRIPTION);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___GET_NUMBER_OF_BEDS);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___SET_NAME__STRING);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___SET_PRICE__DOUBLE);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___SET_DESCRIPTION__STRING);
		createEOperation(iRoomTypeEClass, IROOM_TYPE___SET_NUMBER_OF_BEDS__INT);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		roomTypeHandlerEClass.getESuperTypes().add(this.getIRoomTypeHandler());
		roomTypeHandlerEClass.getESuperTypes().add(this.getIRoomTypeStartUp());
		roomTypeEClass.getESuperTypes().add(this.getIRoomType());

		// Initialize classes, features, and operations; add parameters
		initEClass(roomTypeHandlerEClass, RoomTypeHandler.class, "RoomTypeHandler", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoomTypeHandler_RoomTypes(), this.getRoomType(), null, "roomTypes", null, 1, -1, RoomTypeHandler.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(roomTypeEClass, RoomType.class, "RoomType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoomType_Name(), ecorePackage.getEString(), "name", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_Price(), ecorePackage.getEDouble(), "price", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_Description(), ecorePackage.getEString(), "description", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEAttribute(getRoomType_NumberOfBeds(), ecorePackage.getEInt(), "numberOfBeds", null, 1, 1, RoomType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(iRoomTypeHandlerEClass, IRoomTypeHandler.class, "IRoomTypeHandler", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getIRoomTypeHandler__AddRoomType__String_double_int_String(), null, "addRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "nbrOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeHandler__UpdateRoomType__String_double_int_String(), null, "updateRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "nbrOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeHandler__RemoveRoomType__String(), null, "removeRoomType", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomTypeHandler__GetRoomTypeByName__String(), this.getIRoomType(), "getRoomTypeByName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomTypeStartUpEClass, IRoomTypeStartUp.class, "IRoomTypeStartUp", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoomTypeStartUp__ResetRoomTypes(), null, "resetRoomTypes", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEClass(iRoomTypeEClass, IRoomType.class, "IRoomType", IS_ABSTRACT, IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEOperation(getIRoomType__GetName(), ecorePackage.getEString(), "getName", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetPrice(), ecorePackage.getEDouble(), "getPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetDescription(), ecorePackage.getEString(), "getDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);

		initEOperation(getIRoomType__GetNumberOfBeds(), ecorePackage.getEInt(), "getNumberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomType__SetName__String(), null, "setName", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "name", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomType__SetPrice__double(), null, "setPrice", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "price", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomType__SetDescription__String(), null, "setDescription", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEString(), "description", 1, 1, IS_UNIQUE, !IS_ORDERED);

		op = initEOperation(getIRoomType__SetNumberOfBeds__int(), null, "setNumberOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
		addEParameter(op, ecorePackage.getEInt(), "nbrOfBeds", 1, 1, IS_UNIQUE, !IS_ORDERED);
	}

} //RoomTypePackageImpl
