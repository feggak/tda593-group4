/**
 */
package se.chalmers.cse.mdsd1617.group4.RoomType.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomType;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeHandler;
import se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeHandlerImpl#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTypeHandlerImpl extends MinimalEObjectImpl.Container implements RoomTypeHandler {
	/**
	 * The cached value of the '{@link #getRoomTypes() <em>Room Types</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomType> roomTypes;
	
	private static RoomTypeHandler instance = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected RoomTypeHandlerImpl() {
		super();
		roomTypes = new BasicEList<>();
	}

	public static synchronized RoomTypeHandler getInstance(){
		if(instance == null){
			instance = new RoomTypeHandlerImpl();
		}
		return instance;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RoomTypePackage.Literals.ROOM_TYPE_HANDLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomType> getRoomTypes() {
		if (roomTypes == null) {
			roomTypes = new EObjectResolvingEList<RoomType>(RoomType.class, this, RoomTypePackage.ROOM_TYPE_HANDLER__ROOM_TYPES);
		}
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addRoomType(String name, double price, int nbrOfBeds, String description) {
		for (RoomType rt : roomTypes) {
			if (rt.getName() == name) { //Assumes that you identify a roomtype by its name
				return;
			}
		}
		roomTypes.add(new RoomTypeImpl(name, price, nbrOfBeds, description));

	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateRoomType(String name, double price, int nbrOfBeds, String description) {
		for (RoomType a : roomTypes) {
			if (a.getName() == name) { //Assumes that you identify a roomtype by its name
				a.setName(name);
				a.setPrice(price);
				a.setNumberOfBeds(nbrOfBeds);
				a.setDescription(description);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeRoomType(String name) {
		for(int i = 0; i < roomTypes.size(); i++){ //Assumes that you identify a roomtype by its name
			if(roomTypes.get(i).getName() == name){
				roomTypes.remove(i);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public IRoomType getRoomTypeByName(String name) {
		for (IRoomType roomType : roomTypes) {
			if (roomType.getName().equals(name)) {
				return roomType;
			}
		}
		return null;
	}
	
	public IRoomType getRoomTypeByDescription(String description) {
		for (IRoomType roomType : roomTypes) {
			if (roomType.getDescription().equals(description) || roomType.getName().equals(description)) {
				return roomType;
			}
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void resetRoomTypes() {
		roomTypes.clear();
		roomTypes.add(new RoomTypeImpl("DEFAULT", 100.0, 2, "This is a default startup room"));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_HANDLER__ROOM_TYPES:
				return getRoomTypes();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_HANDLER__ROOM_TYPES:
				getRoomTypes().clear();
				getRoomTypes().addAll((Collection<? extends RoomType>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_HANDLER__ROOM_TYPES:
				getRoomTypes().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case RoomTypePackage.ROOM_TYPE_HANDLER__ROOM_TYPES:
				return roomTypes != null && !roomTypes.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IRoomTypeStartUp.class) {
			switch (baseOperationID) {
				case RoomTypePackage.IROOM_TYPE_START_UP___RESET_ROOM_TYPES: return RoomTypePackage.ROOM_TYPE_HANDLER___RESET_ROOM_TYPES;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case RoomTypePackage.ROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
			case RoomTypePackage.ROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				updateRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
			case RoomTypePackage.ROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING:
				removeRoomType((String)arguments.get(0));
				return null;
			case RoomTypePackage.ROOM_TYPE_HANDLER___GET_ROOM_TYPE_BY_NAME__STRING:
				return getRoomTypeByName((String)arguments.get(0));
			case RoomTypePackage.ROOM_TYPE_HANDLER___RESET_ROOM_TYPES:
				resetRoomTypes();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomTypeHandlerImpl
