/**
 */
package se.chalmers.cse.mdsd1617.group4.RoomType;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Type Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage#getIRoomTypeHandler()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomTypeHandler extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" nbrOfBedsRequired="true" nbrOfBedsOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	void addRoomType(String name, double price, int nbrOfBeds, String description);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameRequired="true" nameOrdered="false" priceRequired="true" priceOrdered="false" nbrOfBedsRequired="true" nbrOfBedsOrdered="false" descriptionRequired="true" descriptionOrdered="false"
	 * @generated
	 */
	void updateRoomType(String name, double price, int nbrOfBeds, String description);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	void removeRoomType(String name);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" nameRequired="true" nameOrdered="false"
	 * @generated
	 */
	IRoomType getRoomTypeByName(String name);
	
	IRoomType getRoomTypeByDescription(String description);
	
} // IRoomTypeHandler
