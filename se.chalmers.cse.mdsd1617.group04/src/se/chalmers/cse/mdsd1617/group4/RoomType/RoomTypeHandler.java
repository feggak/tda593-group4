/**
 */
package se.chalmers.cse.mdsd1617.group4.RoomType;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Handler</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeHandler#getRoomTypes <em>Room Types</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage#getRoomTypeHandler()
 * @model
 * @generated
 */
public interface RoomTypeHandler extends IRoomTypeHandler, IRoomTypeStartUp {
	/**
	 * Returns the value of the '<em><b>Room Types</b></em>' reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Types</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Types</em>' reference list.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage#getRoomTypeHandler_RoomTypes()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

} // RoomTypeHandler
