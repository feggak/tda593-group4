/**
 */
package se.chalmers.cse.mdsd1617.group4.RoomType;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Type Start Up</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypePackage#getIRoomTypeStartUp()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomTypeStartUp extends EObject {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void resetRoomTypes();
} // IRoomTypeStartUp
