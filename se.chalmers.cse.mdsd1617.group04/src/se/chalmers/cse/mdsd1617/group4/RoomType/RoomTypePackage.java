/**
 */
package se.chalmers.cse.mdsd1617.group4.RoomType;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeFactory
 * @model kind="package"
 * @generated
 */
public interface RoomTypePackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "RoomType";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group4/RoomType.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group4.RoomType";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RoomTypePackage eINSTANCE = se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeHandlerImpl <em>Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeHandlerImpl
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getRoomTypeHandler()
	 * @generated
	 */
	int ROOM_TYPE_HANDLER = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler <em>IRoom Type Handler</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getIRoomTypeHandler()
	 * @generated
	 */
	int IROOM_TYPE_HANDLER = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp <em>IRoom Type Start Up</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getIRoomTypeStartUp()
	 * @generated
	 */
	int IROOM_TYPE_START_UP = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType <em>IRoom Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getIRoomType()
	 * @generated
	 */
	int IROOM_TYPE = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeImpl <em>Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeImpl
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 1;

	/**
	 * The number of structural features of the '<em>IRoom Type Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_HANDLER_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 1;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING = 2;

	/**
	 * The operation id for the '<em>Get Room Type By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_HANDLER___GET_ROOM_TYPE_BY_NAME__STRING = 3;

	/**
	 * The number of operations of the '<em>IRoom Type Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_HANDLER_OPERATION_COUNT = 4;

	/**
	 * The feature id for the '<em><b>Room Types</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER__ROOM_TYPES = IROOM_TYPE_HANDLER_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER_FEATURE_COUNT = IROOM_TYPE_HANDLER_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING = IROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Get Room Type By Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___GET_ROOM_TYPE_BY_NAME__STRING = IROOM_TYPE_HANDLER___GET_ROOM_TYPE_BY_NAME__STRING;

	/**
	 * The operation id for the '<em>Reset Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER___RESET_ROOM_TYPES = IROOM_TYPE_HANDLER_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Handler</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_HANDLER_OPERATION_COUNT = IROOM_TYPE_HANDLER_OPERATION_COUNT + 1;

	/**
	 * The number of structural features of the '<em>IRoom Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_NAME = 0;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_PRICE = 1;

	/**
	 * The operation id for the '<em>Get Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_DESCRIPTION = 2;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___GET_NUMBER_OF_BEDS = 3;

	/**
	 * The operation id for the '<em>Set Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___SET_NAME__STRING = 4;

	/**
	 * The operation id for the '<em>Set Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___SET_PRICE__DOUBLE = 5;

	/**
	 * The operation id for the '<em>Set Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___SET_DESCRIPTION__STRING = 6;

	/**
	 * The operation id for the '<em>Set Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE___SET_NUMBER_OF_BEDS__INT = 7;

	/**
	 * The number of operations of the '<em>IRoom Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_OPERATION_COUNT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NAME = IROOM_TYPE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__PRICE = IROOM_TYPE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__DESCRIPTION = IROOM_TYPE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Number Of Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NUMBER_OF_BEDS = IROOM_TYPE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = IROOM_TYPE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Get Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_NAME = IROOM_TYPE___GET_NAME;

	/**
	 * The operation id for the '<em>Get Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_PRICE = IROOM_TYPE___GET_PRICE;

	/**
	 * The operation id for the '<em>Get Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_DESCRIPTION = IROOM_TYPE___GET_DESCRIPTION;

	/**
	 * The operation id for the '<em>Get Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___GET_NUMBER_OF_BEDS = IROOM_TYPE___GET_NUMBER_OF_BEDS;

	/**
	 * The operation id for the '<em>Set Name</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___SET_NAME__STRING = IROOM_TYPE___SET_NAME__STRING;

	/**
	 * The operation id for the '<em>Set Price</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___SET_PRICE__DOUBLE = IROOM_TYPE___SET_PRICE__DOUBLE;

	/**
	 * The operation id for the '<em>Set Description</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___SET_DESCRIPTION__STRING = IROOM_TYPE___SET_DESCRIPTION__STRING;

	/**
	 * The operation id for the '<em>Set Number Of Beds</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE___SET_NUMBER_OF_BEDS__INT = IROOM_TYPE___SET_NUMBER_OF_BEDS__INT;

	/**
	 * The number of operations of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = IROOM_TYPE_OPERATION_COUNT + 0;

	/**
	 * The number of structural features of the '<em>IRoom Type Start Up</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_START_UP_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Reset Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_START_UP___RESET_ROOM_TYPES = 0;

	/**
	 * The number of operations of the '<em>IRoom Type Start Up</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_TYPE_START_UP_OPERATION_COUNT = 1;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeHandler <em>Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeHandler
	 * @generated
	 */
	EClass getRoomTypeHandler();

	/**
	 * Returns the meta object for the reference list '{@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeHandler#getRoomTypes <em>Room Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Room Types</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomTypeHandler#getRoomTypes()
	 * @see #getRoomTypeHandler()
	 * @generated
	 */
	EReference getRoomTypeHandler_RoomTypes();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler <em>IRoom Type Handler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Handler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler
	 * @generated
	 */
	EClass getIRoomTypeHandler();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler#addRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeHandler__AddRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler#updateRoomType(java.lang.String, double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler#updateRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeHandler__UpdateRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeHandler__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler#getRoomTypeByName(java.lang.String) <em>Get Room Type By Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Type By Name</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler#getRoomTypeByName(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomTypeHandler__GetRoomTypeByName__String();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp <em>IRoom Type Start Up</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type Start Up</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp
	 * @generated
	 */
	EClass getIRoomTypeStartUp();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp#resetRoomTypes() <em>Reset Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp#resetRoomTypes()
	 * @generated
	 */
	EOperation getIRoomTypeStartUp__ResetRoomTypes();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType <em>IRoom Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType
	 * @generated
	 */
	EClass getIRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#getName() <em>Get Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Name</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#getName()
	 * @generated
	 */
	EOperation getIRoomType__GetName();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#getPrice() <em>Get Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Price</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#getPrice()
	 * @generated
	 */
	EOperation getIRoomType__GetPrice();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#getDescription() <em>Get Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Description</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#getDescription()
	 * @generated
	 */
	EOperation getIRoomType__GetDescription();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#getNumberOfBeds() <em>Get Number Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Number Of Beds</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#getNumberOfBeds()
	 * @generated
	 */
	EOperation getIRoomType__GetNumberOfBeds();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#setName(java.lang.String) <em>Set Name</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Name</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#setName(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomType__SetName__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#setPrice(double) <em>Set Price</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Price</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#setPrice(double)
	 * @generated
	 */
	EOperation getIRoomType__SetPrice__double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#setDescription(java.lang.String) <em>Set Description</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Description</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#setDescription(java.lang.String)
	 * @generated
	 */
	EOperation getIRoomType__SetDescription__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#setNumberOfBeds(int) <em>Set Number Of Beds</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Set Number Of Beds</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType#setNumberOfBeds(int)
	 * @generated
	 */
	EOperation getIRoomType__SetNumberOfBeds__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomType#getName()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Name();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomType#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomType#getPrice()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Price();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomType#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomType#getDescription()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Description();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.RoomType.RoomType#getNumberOfBeds <em>Number Of Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Number Of Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.RoomType.RoomType#getNumberOfBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NumberOfBeds();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RoomTypeFactory getRoomTypeFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeHandlerImpl <em>Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeHandlerImpl
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getRoomTypeHandler()
		 * @generated
		 */
		EClass ROOM_TYPE_HANDLER = eINSTANCE.getRoomTypeHandler();

		/**
		 * The meta object literal for the '<em><b>Room Types</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_TYPE_HANDLER__ROOM_TYPES = eINSTANCE.getRoomTypeHandler_RoomTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler <em>IRoom Type Handler</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getIRoomTypeHandler()
		 * @generated
		 */
		EClass IROOM_TYPE_HANDLER = eINSTANCE.getIRoomTypeHandler();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_HANDLER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIRoomTypeHandler__AddRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_HANDLER___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIRoomTypeHandler__UpdateRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_HANDLER___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIRoomTypeHandler__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Get Room Type By Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_HANDLER___GET_ROOM_TYPE_BY_NAME__STRING = eINSTANCE.getIRoomTypeHandler__GetRoomTypeByName__String();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp <em>IRoom Type Start Up</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeStartUp
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getIRoomTypeStartUp()
		 * @generated
		 */
		EClass IROOM_TYPE_START_UP = eINSTANCE.getIRoomTypeStartUp();

		/**
		 * The meta object literal for the '<em><b>Reset Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE_START_UP___RESET_ROOM_TYPES = eINSTANCE.getIRoomTypeStartUp__ResetRoomTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType <em>IRoom Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getIRoomType()
		 * @generated
		 */
		EClass IROOM_TYPE = eINSTANCE.getIRoomType();

		/**
		 * The meta object literal for the '<em><b>Get Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_NAME = eINSTANCE.getIRoomType__GetName();

		/**
		 * The meta object literal for the '<em><b>Get Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_PRICE = eINSTANCE.getIRoomType__GetPrice();

		/**
		 * The meta object literal for the '<em><b>Get Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_DESCRIPTION = eINSTANCE.getIRoomType__GetDescription();

		/**
		 * The meta object literal for the '<em><b>Get Number Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___GET_NUMBER_OF_BEDS = eINSTANCE.getIRoomType__GetNumberOfBeds();

		/**
		 * The meta object literal for the '<em><b>Set Name</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___SET_NAME__STRING = eINSTANCE.getIRoomType__SetName__String();

		/**
		 * The meta object literal for the '<em><b>Set Price</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___SET_PRICE__DOUBLE = eINSTANCE.getIRoomType__SetPrice__double();

		/**
		 * The meta object literal for the '<em><b>Set Description</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___SET_DESCRIPTION__STRING = eINSTANCE.getIRoomType__SetDescription__String();

		/**
		 * The meta object literal for the '<em><b>Set Number Of Beds</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_TYPE___SET_NUMBER_OF_BEDS__INT = eINSTANCE.getIRoomType__SetNumberOfBeds__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeImpl <em>Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypeImpl
		 * @see se.chalmers.cse.mdsd1617.group4.RoomType.impl.RoomTypePackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NAME = eINSTANCE.getRoomType_Name();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__PRICE = eINSTANCE.getRoomType_Price();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__DESCRIPTION = eINSTANCE.getRoomType_Description();

		/**
		 * The meta object literal for the '<em><b>Number Of Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NUMBER_OF_BEDS = eINSTANCE.getRoomType_NumberOfBeds();

	}

} //RoomTypePackage
