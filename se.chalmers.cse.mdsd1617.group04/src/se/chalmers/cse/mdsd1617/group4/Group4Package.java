/**
 */
package se.chalmers.cse.mdsd1617.group4;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group4.Group4Factory
 * @model kind="package"
 * @generated
 */
public interface Group4Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "group4";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group4.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group4";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Group4Package eINSTANCE = se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.impl.FreeRoomTypesDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getFreeRoomTypesDTO()
	 * @generated
	 */
	int FREE_ROOM_TYPES_DTO = 0;

	/**
	 * The feature id for the '<em><b>Room Type Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Num Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_BEDS = 1;

	/**
	 * The feature id for the '<em><b>Price Per Night</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = 2;

	/**
	 * The feature id for the '<em><b>Num Free Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = 3;

	/**
	 * The number of structural features of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelAdminProvidesImpl <em>Hotel Admin Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelAdminProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelAdminProvides()
	 * @generated
	 */
	int HOTEL_ADMIN_PROVIDES = 1;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getIHotelCustomerProvides()
	 * @generated
	 */
	int IHOTEL_CUSTOMER_PROVIDES = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelCustomerProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelCustomerProvides()
	 * @generated
	 */
	int HOTEL_CUSTOMER_PROVIDES = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides <em>IHotel Admin Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getIHotelAdminProvides()
	 * @generated
	 */
	int IHOTEL_ADMIN_PROVIDES = 2;

	/**
	 * The number of structural features of the '<em>IHotel Admin Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Derp</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES___DERP__INT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 1;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 2;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES___REMOVE_ROOM_TYPE__STRING = 3;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES___ADD_ROOM__INT_IROOMTYPE = 4;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES___REMOVE_ROOM__INT = 5;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES___UNBLOCK_ROOM__INT = 6;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES___BLOCK_ROOM__INT = 7;

	/**
	 * The number of operations of the '<em>IHotel Admin Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_ADMIN_PROVIDES_OPERATION_COUNT = 8;

	/**
	 * The feature id for the '<em><b>Iroomtypehandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES__IROOMTYPEHANDLER = IHOTEL_ADMIN_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Iroomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES__IROOMHANDLER = IHOTEL_ADMIN_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Hotel Admin Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES_FEATURE_COUNT = IHOTEL_ADMIN_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Derp</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES___DERP__INT = IHOTEL_ADMIN_PROVIDES___DERP__INT;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IHOTEL_ADMIN_PROVIDES___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IHOTEL_ADMIN_PROVIDES___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES___REMOVE_ROOM_TYPE__STRING = IHOTEL_ADMIN_PROVIDES___REMOVE_ROOM_TYPE__STRING;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES___ADD_ROOM__INT_IROOMTYPE = IHOTEL_ADMIN_PROVIDES___ADD_ROOM__INT_IROOMTYPE;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES___REMOVE_ROOM__INT = IHOTEL_ADMIN_PROVIDES___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES___UNBLOCK_ROOM__INT = IHOTEL_ADMIN_PROVIDES___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES___BLOCK_ROOM__INT = IHOTEL_ADMIN_PROVIDES___BLOCK_ROOM__INT;

	/**
	 * The number of operations of the '<em>Hotel Admin Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_PROVIDES_OPERATION_COUNT = IHOTEL_ADMIN_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Booking Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES__BOOKING_PRICE = 0;

	/**
	 * The feature id for the '<em><b>Room Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES__ROOM_PRICE = 1;

	/**
	 * The number of structural features of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = 2;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = 4;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 5;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = 6;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 7;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = 8;

	/**
	 * The number of operations of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = 9;

	/**
	 * The feature id for the '<em><b>Booking Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__BOOKING_PRICE = IHOTEL_CUSTOMER_PROVIDES__BOOKING_PRICE;

	/**
	 * The feature id for the '<em><b>Room Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__ROOM_PRICE = IHOTEL_CUSTOMER_PROVIDES__ROOM_PRICE;

	/**
	 * The feature id for the '<em><b>Ibookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES__IBOOKINGHANDLER = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The number of operations of the '<em>Hotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides <em>IHotel Receptionist Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getIHotelReceptionistProvides()
	 * @generated
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES = 5;

	/**
	 * The feature id for the '<em><b>Booking Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES__BOOKING_PRICE = IHOTEL_CUSTOMER_PROVIDES__BOOKING_PRICE;

	/**
	 * The feature id for the '<em><b>Room Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES__ROOM_PRICE = IHOTEL_CUSTOMER_PROVIDES__ROOM_PRICE;

	/**
	 * The number of structural features of the '<em>IHotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES_FEATURE_COUNT = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECK_IN__INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Check In Physical Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_PHYSICAL_ROOMS__INT_INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__BOOKINGDTO = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>List Occupied</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED__DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE_DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE_DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The number of operations of the '<em>IHotel Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_RECEPTIONIST_PROVIDES_OPERATION_COUNT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.impl.ReceptionistProvidesImpl <em>Receptionist Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.impl.ReceptionistProvidesImpl
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getReceptionistProvides()
	 * @generated
	 */
	int RECEPTIONIST_PROVIDES = 6;

	/**
	 * The feature id for the '<em><b>Booking Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES__BOOKING_PRICE = IHOTEL_RECEPTIONIST_PROVIDES__BOOKING_PRICE;

	/**
	 * The feature id for the '<em><b>Room Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES__ROOM_PRICE = IHOTEL_RECEPTIONIST_PROVIDES__ROOM_PRICE;

	/**
	 * The feature id for the '<em><b>Ibookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES__IBOOKINGHANDLER = IHOTEL_RECEPTIONIST_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES_FEATURE_COUNT = IHOTEL_RECEPTIONIST_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_RECEPTIONIST_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_RECEPTIONIST_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_RECEPTIONIST_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Initiate Check In</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___INITIATE_CHECK_IN__INT = IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECK_IN__INT;

	/**
	 * The operation id for the '<em>Check In Physical Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___CHECK_IN_PHYSICAL_ROOMS__INT_INT = IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_PHYSICAL_ROOMS__INT_INT;

	/**
	 * The operation id for the '<em>Edit Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___EDIT_BOOKING__BOOKINGDTO = IHOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__BOOKINGDTO;

	/**
	 * The operation id for the '<em>Cancel Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = IHOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___LIST_BOOKINGS = IHOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS;

	/**
	 * The operation id for the '<em>List Occupied</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___LIST_OCCUPIED__DATE = IHOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED__DATE;

	/**
	 * The operation id for the '<em>List Check Ins</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE_DATE = IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE_DATE;

	/**
	 * The operation id for the '<em>List Check Outs</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE_DATE = IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE_DATE;

	/**
	 * The operation id for the '<em>Add Extra Cost To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE = IHOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE;

	/**
	 * The number of operations of the '<em>Receptionist Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECEPTIONIST_PROVIDES_OPERATION_COUNT = IHOTEL_RECEPTIONIST_PROVIDES_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelAdminTestImpl <em>Hotel Admin Test</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelAdminTestImpl
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelAdminTest()
	 * @generated
	 */
	int HOTEL_ADMIN_TEST = 7;

	/**
	 * The feature id for the '<em><b>Ihoteladminprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_TEST__IHOTELADMINPROVIDES = 0;

	/**
	 * The feature id for the '<em><b>Ihotelstartupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_TEST__IHOTELSTARTUPPROVIDES = 1;

	/**
	 * The number of structural features of the '<em>Hotel Admin Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_TEST_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Hotel Admin Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_ADMIN_TEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelCustomerTestImpl <em>Hotel Customer Test</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelCustomerTestImpl
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelCustomerTest()
	 * @generated
	 */
	int HOTEL_CUSTOMER_TEST = 8;

	/**
	 * The feature id for the '<em><b>Ihotelcustomerprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_TEST__IHOTELCUSTOMERPROVIDES = 0;

	/**
	 * The feature id for the '<em><b>Ihotelstartupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_TEST__IHOTELSTARTUPPROVIDES = 1;

	/**
	 * The number of structural features of the '<em>Hotel Customer Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_TEST_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Hotel Customer Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_CUSTOMER_TEST_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelReceptionistTestImpl <em>Hotel Receptionist Test</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelReceptionistTestImpl
	 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelReceptionistTest()
	 * @generated
	 */
	int HOTEL_RECEPTIONIST_TEST = 9;

	/**
	 * The feature id for the '<em><b>Ihotelreceptionistprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES = 0;

	/**
	 * The feature id for the '<em><b>Ihotelstartupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES = 1;

	/**
	 * The number of structural features of the '<em>Hotel Receptionist Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_TEST_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Hotel Receptionist Test</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOTEL_RECEPTIONIST_TEST_OPERATION_COUNT = 0;

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO <em>Free Room Types DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Free Room Types DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO
	 * @generated
	 */
	EClass getFreeRoomTypesDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO#getRoomTypeDescription <em>Room Type Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Type Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO#getRoomTypeDescription()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_RoomTypeDescription();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO#getNumBeds <em>Num Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO#getNumBeds()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO#getPricePerNight <em>Price Per Night</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price Per Night</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO#getPricePerNight()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_PricePerNight();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO#getNumFreeRooms <em>Num Free Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Free Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.FreeRoomTypesDTO#getNumFreeRooms()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumFreeRooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminProvides <em>Hotel Admin Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Admin Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelAdminProvides
	 * @generated
	 */
	EClass getHotelAdminProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminProvides#getIroomtypehandler <em>Iroomtypehandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomtypehandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelAdminProvides#getIroomtypehandler()
	 * @see #getHotelAdminProvides()
	 * @generated
	 */
	EReference getHotelAdminProvides_Iroomtypehandler();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminProvides#getIroomhandler <em>Iroomhandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomhandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelAdminProvides#getIroomhandler()
	 * @see #getHotelAdminProvides()
	 * @generated
	 */
	EReference getHotelAdminProvides_Iroomhandler();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerProvides <em>Hotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelCustomerProvides
	 * @generated
	 */
	EClass getHotelCustomerProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerProvides#getIbookinghandler <em>Ibookinghandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ibookinghandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelCustomerProvides#getIbookinghandler()
	 * @see #getHotelCustomerProvides()
	 * @generated
	 */
	EReference getHotelCustomerProvides_Ibookinghandler();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides <em>IHotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides
	 * @generated
	 */
	EClass getIHotelCustomerProvides();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getBookingPrice <em>Booking Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getBookingPrice()
	 * @see #getIHotelCustomerProvides()
	 * @generated
	 */
	EAttribute getIHotelCustomerProvides_BookingPrice();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getRoomPrice <em>Room Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getRoomPrice()
	 * @see #getIHotelCustomerProvides()
	 * @generated
	 */
	EAttribute getIHotelCustomerProvides_RoomPrice();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#confirmBooking(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__CheckInRoom__String_int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides <em>IHotel Admin Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Admin Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides
	 * @generated
	 */
	EClass getIHotelAdminProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#derp(int) <em>Derp</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Derp</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#derp(int)
	 * @generated
	 */
	EOperation getIHotelAdminProvides__Derp__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#addRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelAdminProvides__AddRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#updateRoomType(java.lang.String, double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#updateRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelAdminProvides__UpdateRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#removeRoomType(java.lang.String) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#removeRoomType(java.lang.String)
	 * @generated
	 */
	EOperation getIHotelAdminProvides__RemoveRoomType__String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#addRoom(int, se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#addRoom(int, se.chalmers.cse.mdsd1617.group4.RoomType.IRoomType)
	 * @generated
	 */
	EOperation getIHotelAdminProvides__AddRoom__int_IRoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#removeRoom(int)
	 * @generated
	 */
	EOperation getIHotelAdminProvides__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#unblockRoom(int)
	 * @generated
	 */
	EOperation getIHotelAdminProvides__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides#blockRoom(int)
	 * @generated
	 */
	EOperation getIHotelAdminProvides__BlockRoom__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides <em>IHotel Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Receptionist Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides
	 * @generated
	 */
	EClass getIHotelReceptionistProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#initiateCheckIn(int) <em>Initiate Check In</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Check In</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#initiateCheckIn(int)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__InitiateCheckIn__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#checkInPhysicalRooms(int, int) <em>Check In Physical Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Physical Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#checkInPhysicalRooms(int, int)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__CheckInPhysicalRooms__int_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#editBooking(se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO) <em>Edit Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#editBooking(se.chalmers.cse.mdsd1617.group4.Booking.BookingDTO)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__EditBooking__BookingDTO();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#cancelBooking(int) <em>Cancel Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#cancelBooking(int)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__CancelBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#listBookings()
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#listOccupied(java.util.Date) <em>List Occupied</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Occupied</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#listOccupied(java.util.Date)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListOccupied__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#listCheckIns(java.util.Date, java.util.Date) <em>List Check Ins</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#listCheckIns(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListCheckIns__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#listCheckOuts(java.util.Date, java.util.Date) <em>List Check Outs</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#listCheckOuts(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__ListCheckOuts__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#addExtraCostToRoom(int, int, java.lang.String, double) <em>Add Extra Cost To Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Cost To Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides#addExtraCostToRoom(int, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getIHotelReceptionistProvides__AddExtraCostToRoom__int_int_String_double();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.ReceptionistProvides <em>Receptionist Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Receptionist Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.ReceptionistProvides
	 * @generated
	 */
	EClass getReceptionistProvides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.ReceptionistProvides#getIbookinghandler <em>Ibookinghandler</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ibookinghandler</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.ReceptionistProvides#getIbookinghandler()
	 * @see #getReceptionistProvides()
	 * @generated
	 */
	EReference getReceptionistProvides_Ibookinghandler();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminTest <em>Hotel Admin Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Admin Test</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelAdminTest
	 * @generated
	 */
	EClass getHotelAdminTest();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminTest#getIhoteladminprovides <em>Ihoteladminprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ihoteladminprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelAdminTest#getIhoteladminprovides()
	 * @see #getHotelAdminTest()
	 * @generated
	 */
	EReference getHotelAdminTest_Ihoteladminprovides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ihotelstartupprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelAdminTest#getIhotelstartupprovides()
	 * @see #getHotelAdminTest()
	 * @generated
	 */
	EReference getHotelAdminTest_Ihotelstartupprovides();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerTest <em>Hotel Customer Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Customer Test</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelCustomerTest
	 * @generated
	 */
	EClass getHotelCustomerTest();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerTest#getIhotelcustomerprovides <em>Ihotelcustomerprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ihotelcustomerprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelCustomerTest#getIhotelcustomerprovides()
	 * @see #getHotelCustomerTest()
	 * @generated
	 */
	EReference getHotelCustomerTest_Ihotelcustomerprovides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ihotelstartupprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelCustomerTest#getIhotelstartupprovides()
	 * @see #getHotelCustomerTest()
	 * @generated
	 */
	EReference getHotelCustomerTest_Ihotelstartupprovides();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest <em>Hotel Receptionist Test</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hotel Receptionist Test</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest
	 * @generated
	 */
	EClass getHotelReceptionistTest();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest#getIhotelreceptionistprovides <em>Ihotelreceptionistprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ihotelreceptionistprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest#getIhotelreceptionistprovides()
	 * @see #getHotelReceptionistTest()
	 * @generated
	 */
	EReference getHotelReceptionistTest_Ihotelreceptionistprovides();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Ihotelstartupprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest#getIhotelstartupprovides()
	 * @see #getHotelReceptionistTest()
	 * @generated
	 */
	EReference getHotelReceptionistTest_Ihotelstartupprovides();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Group4Factory getGroup4Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.impl.FreeRoomTypesDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getFreeRoomTypesDTO()
		 * @generated
		 */
		EClass FREE_ROOM_TYPES_DTO = eINSTANCE.getFreeRoomTypesDTO();

		/**
		 * The meta object literal for the '<em><b>Room Type Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = eINSTANCE.getFreeRoomTypesDTO_RoomTypeDescription();

		/**
		 * The meta object literal for the '<em><b>Num Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_BEDS = eINSTANCE.getFreeRoomTypesDTO_NumBeds();

		/**
		 * The meta object literal for the '<em><b>Price Per Night</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = eINSTANCE.getFreeRoomTypesDTO_PricePerNight();

		/**
		 * The meta object literal for the '<em><b>Num Free Rooms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = eINSTANCE.getFreeRoomTypesDTO_NumFreeRooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelAdminProvidesImpl <em>Hotel Admin Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelAdminProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelAdminProvides()
		 * @generated
		 */
		EClass HOTEL_ADMIN_PROVIDES = eINSTANCE.getHotelAdminProvides();

		/**
		 * The meta object literal for the '<em><b>Iroomtypehandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMIN_PROVIDES__IROOMTYPEHANDLER = eINSTANCE.getHotelAdminProvides_Iroomtypehandler();

		/**
		 * The meta object literal for the '<em><b>Iroomhandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMIN_PROVIDES__IROOMHANDLER = eINSTANCE.getHotelAdminProvides_Iroomhandler();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelCustomerProvidesImpl <em>Hotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelCustomerProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelCustomerProvides()
		 * @generated
		 */
		EClass HOTEL_CUSTOMER_PROVIDES = eINSTANCE.getHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Ibookinghandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_PROVIDES__IBOOKINGHANDLER = eINSTANCE.getHotelCustomerProvides_Ibookinghandler();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.IHotelCustomerProvides
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getIHotelCustomerProvides()
		 * @generated
		 */
		EClass IHOTEL_CUSTOMER_PROVIDES = eINSTANCE.getIHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Booking Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IHOTEL_CUSTOMER_PROVIDES__BOOKING_PRICE = eINSTANCE.getIHotelCustomerProvides_BookingPrice();

		/**
		 * The meta object literal for the '<em><b>Room Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IHOTEL_CUSTOMER_PROVIDES__ROOM_PRICE = eINSTANCE.getIHotelCustomerProvides_RoomPrice();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIHotelCustomerProvides__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = eINSTANCE.getIHotelCustomerProvides__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = eINSTANCE.getIHotelCustomerProvides__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getIHotelCustomerProvides__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides <em>IHotel Admin Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.IHotelAdminProvides
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getIHotelAdminProvides()
		 * @generated
		 */
		EClass IHOTEL_ADMIN_PROVIDES = eINSTANCE.getIHotelAdminProvides();

		/**
		 * The meta object literal for the '<em><b>Derp</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMIN_PROVIDES___DERP__INT = eINSTANCE.getIHotelAdminProvides__Derp__int();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMIN_PROVIDES___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIHotelAdminProvides__AddRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMIN_PROVIDES___UPDATE_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIHotelAdminProvides__UpdateRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMIN_PROVIDES___REMOVE_ROOM_TYPE__STRING = eINSTANCE.getIHotelAdminProvides__RemoveRoomType__String();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMIN_PROVIDES___ADD_ROOM__INT_IROOMTYPE = eINSTANCE.getIHotelAdminProvides__AddRoom__int_IRoomType();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMIN_PROVIDES___REMOVE_ROOM__INT = eINSTANCE.getIHotelAdminProvides__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMIN_PROVIDES___UNBLOCK_ROOM__INT = eINSTANCE.getIHotelAdminProvides__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_ADMIN_PROVIDES___BLOCK_ROOM__INT = eINSTANCE.getIHotelAdminProvides__BlockRoom__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides <em>IHotel Receptionist Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.IHotelReceptionistProvides
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getIHotelReceptionistProvides()
		 * @generated
		 */
		EClass IHOTEL_RECEPTIONIST_PROVIDES = eINSTANCE.getIHotelReceptionistProvides();

		/**
		 * The meta object literal for the '<em><b>Initiate Check In</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___INITIATE_CHECK_IN__INT = eINSTANCE.getIHotelReceptionistProvides__InitiateCheckIn__int();

		/**
		 * The meta object literal for the '<em><b>Check In Physical Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___CHECK_IN_PHYSICAL_ROOMS__INT_INT = eINSTANCE.getIHotelReceptionistProvides__CheckInPhysicalRooms__int_int();

		/**
		 * The meta object literal for the '<em><b>Edit Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___EDIT_BOOKING__BOOKINGDTO = eINSTANCE.getIHotelReceptionistProvides__EditBooking__BookingDTO();

		/**
		 * The meta object literal for the '<em><b>Cancel Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___CANCEL_BOOKING__INT = eINSTANCE.getIHotelReceptionistProvides__CancelBooking__int();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_BOOKINGS = eINSTANCE.getIHotelReceptionistProvides__ListBookings();

		/**
		 * The meta object literal for the '<em><b>List Occupied</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_OCCUPIED__DATE = eINSTANCE.getIHotelReceptionistProvides__ListOccupied__Date();

		/**
		 * The meta object literal for the '<em><b>List Check Ins</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_INS__DATE_DATE = eINSTANCE.getIHotelReceptionistProvides__ListCheckIns__Date_Date();

		/**
		 * The meta object literal for the '<em><b>List Check Outs</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___LIST_CHECK_OUTS__DATE_DATE = eINSTANCE.getIHotelReceptionistProvides__ListCheckOuts__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Add Extra Cost To Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_RECEPTIONIST_PROVIDES___ADD_EXTRA_COST_TO_ROOM__INT_INT_STRING_DOUBLE = eINSTANCE.getIHotelReceptionistProvides__AddExtraCostToRoom__int_int_String_double();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.impl.ReceptionistProvidesImpl <em>Receptionist Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.impl.ReceptionistProvidesImpl
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getReceptionistProvides()
		 * @generated
		 */
		EClass RECEPTIONIST_PROVIDES = eINSTANCE.getReceptionistProvides();

		/**
		 * The meta object literal for the '<em><b>Ibookinghandler</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECEPTIONIST_PROVIDES__IBOOKINGHANDLER = eINSTANCE.getReceptionistProvides_Ibookinghandler();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelAdminTestImpl <em>Hotel Admin Test</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelAdminTestImpl
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelAdminTest()
		 * @generated
		 */
		EClass HOTEL_ADMIN_TEST = eINSTANCE.getHotelAdminTest();

		/**
		 * The meta object literal for the '<em><b>Ihoteladminprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMIN_TEST__IHOTELADMINPROVIDES = eINSTANCE.getHotelAdminTest_Ihoteladminprovides();

		/**
		 * The meta object literal for the '<em><b>Ihotelstartupprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_ADMIN_TEST__IHOTELSTARTUPPROVIDES = eINSTANCE.getHotelAdminTest_Ihotelstartupprovides();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelCustomerTestImpl <em>Hotel Customer Test</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelCustomerTestImpl
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelCustomerTest()
		 * @generated
		 */
		EClass HOTEL_CUSTOMER_TEST = eINSTANCE.getHotelCustomerTest();

		/**
		 * The meta object literal for the '<em><b>Ihotelcustomerprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_TEST__IHOTELCUSTOMERPROVIDES = eINSTANCE.getHotelCustomerTest_Ihotelcustomerprovides();

		/**
		 * The meta object literal for the '<em><b>Ihotelstartupprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_CUSTOMER_TEST__IHOTELSTARTUPPROVIDES = eINSTANCE.getHotelCustomerTest_Ihotelstartupprovides();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group4.impl.HotelReceptionistTestImpl <em>Hotel Receptionist Test</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group4.impl.HotelReceptionistTestImpl
		 * @see se.chalmers.cse.mdsd1617.group4.impl.Group4PackageImpl#getHotelReceptionistTest()
		 * @generated
		 */
		EClass HOTEL_RECEPTIONIST_TEST = eINSTANCE.getHotelReceptionistTest();

		/**
		 * The meta object literal for the '<em><b>Ihotelreceptionistprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_RECEPTIONIST_TEST__IHOTELRECEPTIONISTPROVIDES = eINSTANCE.getHotelReceptionistTest_Ihotelreceptionistprovides();

		/**
		 * The meta object literal for the '<em><b>Ihotelstartupprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOTEL_RECEPTIONIST_TEST__IHOTELSTARTUPPROVIDES = eINSTANCE.getHotelReceptionistTest_Ihotelstartupprovides();

	}

} //Group4Package
