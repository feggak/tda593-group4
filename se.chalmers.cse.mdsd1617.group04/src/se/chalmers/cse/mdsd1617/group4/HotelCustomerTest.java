/**
 */
package se.chalmers.cse.mdsd1617.group4;

import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.IHotelStartupProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Customer Test</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerTest#getIhotelcustomerprovides <em>Ihotelcustomerprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelCustomerTest()
 * @model
 * @generated
 */
public interface HotelCustomerTest extends EObject {
	/**
	 * Returns the value of the '<em><b>Ihotelcustomerprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ihotelcustomerprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ihotelcustomerprovides</em>' reference.
	 * @see #setIhotelcustomerprovides(IHotelCustomerProvides)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelCustomerTest_Ihotelcustomerprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IHotelCustomerProvides getIhotelcustomerprovides();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerTest#getIhotelcustomerprovides <em>Ihotelcustomerprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ihotelcustomerprovides</em>' reference.
	 * @see #getIhotelcustomerprovides()
	 * @generated
	 */
	void setIhotelcustomerprovides(IHotelCustomerProvides value);

	/**
	 * Returns the value of the '<em><b>Ihotelstartupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ihotelstartupprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ihotelstartupprovides</em>' reference.
	 * @see #setIhotelstartupprovides(IHotelStartupProvides)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelCustomerTest_Ihotelstartupprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IHotelStartupProvides getIhotelstartupprovides();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelCustomerTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ihotelstartupprovides</em>' reference.
	 * @see #getIhotelstartupprovides()
	 * @generated
	 */
	void setIhotelstartupprovides(IHotelStartupProvides value);

} // HotelCustomerTest
