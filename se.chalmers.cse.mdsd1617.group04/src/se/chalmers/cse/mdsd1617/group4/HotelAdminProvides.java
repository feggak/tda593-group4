/**
 */
package se.chalmers.cse.mdsd1617.group4;

import se.chalmers.cse.mdsd1617.group4.Room.IRoomHandler;
import se.chalmers.cse.mdsd1617.group4.RoomType.IRoomTypeHandler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Admin Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelAdminProvides#getIroomtypehandler <em>Iroomtypehandler</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelAdminProvides#getIroomhandler <em>Iroomhandler</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelAdminProvides()
 * @model
 * @generated
 */
public interface HotelAdminProvides extends IHotelAdminProvides {

	/**
	 * Returns the value of the '<em><b>Iroomtypehandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomtypehandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomtypehandler</em>' reference.
	 * @see #setIroomtypehandler(IRoomTypeHandler)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelAdminProvides_Iroomtypehandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomTypeHandler getIroomtypehandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminProvides#getIroomtypehandler <em>Iroomtypehandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomtypehandler</em>' reference.
	 * @see #getIroomtypehandler()
	 * @generated
	 */
	void setIroomtypehandler(IRoomTypeHandler value);

	/**
	 * Returns the value of the '<em><b>Iroomhandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomhandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomhandler</em>' reference.
	 * @see #setIroomhandler(IRoomHandler)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelAdminProvides_Iroomhandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomHandler getIroomhandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelAdminProvides#getIroomhandler <em>Iroomhandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomhandler</em>' reference.
	 * @see #getIroomhandler()
	 * @generated
	 */
	void setIroomhandler(IRoomHandler value);
} // HotelAdminProvides
