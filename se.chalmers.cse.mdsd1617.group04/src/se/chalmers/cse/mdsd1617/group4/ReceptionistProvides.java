/**
 */
package se.chalmers.cse.mdsd1617.group4;

import se.chalmers.cse.mdsd1617.group4.Booking.IBookingHandler;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Receptionist Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.ReceptionistProvides#getIbookinghandler <em>Ibookinghandler</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getReceptionistProvides()
 * @model
 * @generated
 */
public interface ReceptionistProvides extends IHotelReceptionistProvides {

	/**
	 * Returns the value of the '<em><b>Ibookinghandler</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ibookinghandler</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ibookinghandler</em>' reference.
	 * @see #setIbookinghandler(IBookingHandler)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getReceptionistProvides_Ibookinghandler()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IBookingHandler getIbookinghandler();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.ReceptionistProvides#getIbookinghandler <em>Ibookinghandler</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ibookinghandler</em>' reference.
	 * @see #getIbookinghandler()
	 * @generated
	 */
	void setIbookinghandler(IBookingHandler value);
} // ReceptionistProvides
