/**
 */
package se.chalmers.cse.mdsd1617.group4;

import org.eclipse.emf.ecore.EObject;

import se.chalmers.cse.mdsd1617.group4.HotelStartUp.IHotelStartupProvides;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hotel Receptionist Test</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest#getIhotelreceptionistprovides <em>Ihotelreceptionistprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelReceptionistTest()
 * @model
 * @generated
 */
public interface HotelReceptionistTest extends EObject {
	/**
	 * Returns the value of the '<em><b>Ihotelreceptionistprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ihotelreceptionistprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ihotelreceptionistprovides</em>' reference.
	 * @see #setIhotelreceptionistprovides(IHotelReceptionistProvides)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelReceptionistTest_Ihotelreceptionistprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IHotelReceptionistProvides getIhotelreceptionistprovides();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest#getIhotelreceptionistprovides <em>Ihotelreceptionistprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ihotelreceptionistprovides</em>' reference.
	 * @see #getIhotelreceptionistprovides()
	 * @generated
	 */
	void setIhotelreceptionistprovides(IHotelReceptionistProvides value);

	/**
	 * Returns the value of the '<em><b>Ihotelstartupprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ihotelstartupprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ihotelstartupprovides</em>' reference.
	 * @see #setIhotelstartupprovides(IHotelStartupProvides)
	 * @see se.chalmers.cse.mdsd1617.group4.Group4Package#getHotelReceptionistTest_Ihotelstartupprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IHotelStartupProvides getIhotelstartupprovides();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group4.HotelReceptionistTest#getIhotelstartupprovides <em>Ihotelstartupprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Ihotelstartupprovides</em>' reference.
	 * @see #getIhotelstartupprovides()
	 * @generated
	 */
	void setIhotelstartupprovides(IHotelStartupProvides value);

} // HotelReceptionistTest
