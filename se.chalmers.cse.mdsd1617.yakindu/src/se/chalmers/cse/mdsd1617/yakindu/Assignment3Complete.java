package se.chalmers.cse.mdsd1617.yakindu;

public class Assignment3Complete {
	private final int MAX_ROOMS = 2;
	
	private long currentBookingId = 0;
	private long currentRoomReservations = 0;
	private boolean[] roomStatus = new boolean[MAX_ROOMS];

	public long initiateBooking() {
		return currentBookingId++;
	}

	public boolean addRoomToBooking(long bookingId) {
		if (bookingId < 0 || bookingId >= currentBookingId) {
			return false;
		} else if (currentRoomReservations >= MAX_ROOMS) {
			return false;
		} else {
			currentRoomReservations++;
			return true;
		}
	}
	
	public long getFreeRoom() {
		for (int i = 0; i < roomStatus.length; i++) {
			if (!roomStatus[i]) {
				roomStatus[i] = true;
				return i;
			}
		}
		return -1;
	}

	public long getCRR(){
		return currentRoomReservations;
	}
	
	public boolean isRoomOccupied(long roomId) {
		return roomStatus[(int)roomId];
	}
	public void setRoomStatus(long roomId, boolean status) {
		roomStatus[(int)roomId] = status;
	}
}
